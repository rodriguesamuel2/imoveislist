<?php

namespace Application\Form;

use Zend\Form\Form;

class Cliente extends Form {

    public function __construct() {
        parent::__construct('cliente');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/application/index/cadastro');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
        $this->add(array(
            'name' => 'cpf',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Cpf',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Telefone',
            ),
        ));
        $this->add(array(
            'name' => 'senha',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Senha',
            ),
        ));

        $this->add(array(
            'name' => 'cep',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Cep',
            ),
        ));

        $this->add(array(
            'name' => 'rua',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Rua',
            ),
        ));

        $this->add(array(
            'name' => 'numero',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Número',
            ),
        ));

        $this->add(array(
            'name' => 'cidade',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Cidade',
            ),
        ));
        
        
        
         $this->add(array(
            'name' => 'estado',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Estado',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }

}