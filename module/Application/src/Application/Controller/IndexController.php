<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Session\Container;
use Zend\Mvc\Controller\AbstractActionController;
use Core\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Zend\Db\Sql\Sql;
use Zend\Debug\Debug;
use Admin\Model\Cliente;
//use Admin\Model\Pedido;
use Application\Form\Cliente as ClienteForm;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;
use Admin\Model\Pagina;
use Admin\Form\Pagina as PaginaForm;
use Admin\Model\Pagfoto;
use Admin\Form\Pagfoto as PagfotoForm;
use Zend\Validator\File\Size;
use Admin\Model\Catvagas;
use Admin\Model\Vagas;
use Admin\Form\Vagas as VagasForm;
use Admin\Model\Locacao;
use Admin\Form\Locacao as LocacaoForm;
use Admin\Model\Curriculo;
use Admin\Form\Curriculo as CurriculoForm;

//use Zend\Validator\File\Size;

class IndexController extends ActionController {

    public function __construct() {

//        $carrinho_session = new Container('carrinho');
//
//        $carrinhoprodutos = new Container('carrinhoprodutos');
//
//        if (!isset($carrinho_session->carrinho)) {
//            $carrinho_session->carrinho = array();
//        }
//        if (!isset($carrinhoprodutos->carrinhoprodutos)) {
//            $carrinhoprodutos->carrinhoprodutos = '';
//        }
    }

    /* Funçao da página principal do site */

    public function indexAction() {
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
    }

    public function homeAction() {
//        $cliente_session = new Container('cliente');
        $site_session = new Container('site');
        /* Seleciona banners do site */
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $tipo = "";
//        if (isset($_POST['tipo'])) {
//            $tipo = " and produtos.subcategoria_id='" . $_POST['tipo'] . "'";
//        }
//
//        $cidade = "";
//        if (isset($_POST['cidade'])) {
//            $cidade = " and produtos.caracteristicas LIKE '%" . $_POST['cidade'] . "%'";
//        }
//
//        $bairro = "";
//        if (isset($_POST['bairro'])) {
//            $bairro = " and produtos.nome LIKE '%" . $_POST['bairro'] . "%'";
//        }
//
//        $valor = "";
//        if (isset($_POST['valor1']) && isset($_POST['valor2'])) {
//            $valor = " and preco >= " . $_POST['valor1'] . " and preco <= " . $_POST['valor2'];
//        }



        $select = $sql->select()
                ->from('estados')
                ->order(array("nome asc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $estados = $statement->execute();

        $select = $sql->select()
                ->from('imoveis_tipos')
                ->order(array("tipo asc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $tipos = $statement->execute();

//        $select2 = $sql->select()
//                // ->columns(array('id', 'legenda', 'texto', 'imagem'))
//                ->from('produtos')
//                ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem', 'legenda'))
//                ->join('subcategorias', 'subcategorias.id = produtos.subcategoria_id', array('snome' => 'nome'))
//                ->where(array("profotos.principal=1 and produtos.oferta=1 " . $tipo . $cidade . $bairro . $valor))
//                ->order(array("id desc"))
//                ->limit('6');
////                ->where(array('banners.ativo=1'))
////                ->order(array("ordem asc"));
////        echo $sql->getSqlstringForSqlObject($select2); die;
//        $statement2 = $sql->prepareStatementForSqlObject($select2);
//        $produtos = $statement2->execute();
//
//        $contador = 0;
//
//        foreach ($produtos as $imo) {
//            $contador++;
//        }
//        $select3 = $sql->select()
//                ->from('configuracoes');
//        $statement3 = $sql->prepareStatementForSqlObject($select3);
//        $configuracoes = $statement3->execute();
//        $select4 = $sql->select()
//                ->from('subcategorias')
//                ->order('ordem asc');
//        $statement4 = $sql->prepareStatementForSqlObject($select4);
//        $subcategorias = $statement4->execute();
//         echo $sql->getSqlstringForSqlObject($select4);
//        $adapter2 = $this->getServiceLocator()->get('DbAdapter');
//        $sql2 = new Sql($adapter2);
//
//        $select4 = $sql->select()
//                ->from('marcas')
//                ->order('nome asc');
//        $statement4 = $sql->prepareStatementForSqlObject($select4);
//        $marcas = $statement4->execute();
//         echo $sql->getSqlstringForSqlObject($select4);
//        $adapter2 = $this->getServiceLocator()->get('DbAdapter');
//        $sql2 = new Sql($adapter2);
//        $select5 = $sql2->select()
//                ->quantifier('DISTINCT')
//                ->columns(array('cidades' => 'caracteristicas'))
//                ->from(array('p' => 'produtos'))
//                ->order('cidades asc');
//        $statement5 = $sql->prepareStatementForSqlObject($select5);
//        $cidades = $statement5->execute();
//        echo $sql->getSqlstringForSqlObject($select5);
//        die;
//        $select6 = $sql->select()
//                ->quantifier('DISTINCT')
//                ->columns(array('bairros' => 'nome'))
//                ->from(array('p' => 'produtos'))
//                ->order('bairros asc');
//        $statement6 = $sql->prepareStatementForSqlObject($select6);
//        $bairros = $statement6->execute();




        $layout = $this->layout();
        $layout->setTemplate('layout/app');
        $view = new ViewModel(array(
            'estados' => $estados,
            'tipos' => $tipos,
//            'produtos' => $produtos,
//            'configuracoes' => $configuracoes,
//            'marcas' => $marcas,
//            'cidades' => $cidades,
//            'bairros' => $bairros,
//            'banners' => $banners,
//            'categorias' => $subcategorias,
        ));
        return $view;
    }

    public function carregarcidadesAction() {



        $estado = $this->params()->fromQuery('estado', '');

        $this->layout('layout/ajax-layout');

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('cidades')
                ->where("flg_estado='" . $estado . "'")
                ->order('desc_cidade asc');

        $statement = $sql->prepareStatementForSqlObject($select);
        $dados = $statement->execute();

        $view = new ViewModel(array(
            'dados' => $dados
        ));
        return $view;
    }

    public function carregarbairrosAction() {

        $cidade_id = $this->params()->fromQuery('cidade', 0);

        $this->layout('layout/ajax-layout');

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->quantifier('DISTINCT')
                ->columns(array('bairro'))
                ->from(array('i' => 'imoveis'))
                ->where("cidade_id=" . $cidade_id . "")
                ->order('bairro asc');

        $statement = $sql->prepareStatementForSqlObject($select);
        $dados = $statement->execute();

        $view = new ViewModel(array(
            'dados' => $dados
        ));
        return $view;
    }

    public function imoveisAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $cidades = array();
        $bairros = array();

        $estadosql = "";
        $estado = $this->params()->fromRoute('estado', "");
        if ($estado != "") {
            $estadosql = " and imoveis.estado_id='" . $estado . "'";
            $select = $sql->select()
                    ->from('cidades')
                    ->where(array("cidades.flg_estado='" . $estado ."'"))
                    ->order(array("desc_cidade asc"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $cidades = $statement->execute();
        }

        $cidadesql = "";
        $cidade = $this->params()->fromRoute('c', "");
        if ($cidade != "") {
            $cidadesql = " and imoveis.cidade_id='" . $cidade . "'";
            
            $select = $sql->select()
                ->quantifier('DISTINCT')
                ->columns(array('bairro'))
                ->from(array('i' => 'imoveis'))
                ->where("cidade_id=" . $cidade . "")
                ->order('bairro asc');

        $statement = $sql->prepareStatementForSqlObject($select);
        $bairros = $statement->execute();
        }

        $bairrosql = "";
        $bairro = $this->params()->fromRoute('bairro', "");
        if ($bairro != "") {
            $bairrosql = " and imoveis.bairro like '%" . str_replace("_", " ", $bairro) . "%'";
        }

        $tiposql = "";
        $tipo = $this->params()->fromRoute('t', "");
        if ($tipo != "") {
            $tiposql = " and imoveis.tipo_id = '" . $tipo . "'";
        }

        $finalidadesql = "";
        $finalidade =  $this->params()->fromRoute('f', "");
        if ($finalidade != "") {
            $finalidadesql = " and imoveis.finalidade = '" . $finalidade . "'";
        }
        
        $ordemsql = "criacao DESC";
        $ordem =  $this->params()->fromRoute('ordem', "");
        
        if ($ordem != "") {
            if($ordem=='1'){
                $ordemsql = "preco ASC";
            }
            if($ordem=='2'){
                $ordemsql = "preco DESC";
            }
            if($ordem=='3'){
                $ordemsql = "criacao DESC";
            }
        }

        $select = $sql->select()
                ->columns(array('id','titulo','preco','estado_id','bairro','quartos','area_construida','banheiros','garagem'))
                ->from('imoveis')
                ->join('cidades', 'imoveis.cidade_id = cidades.cidade_id', array('cidade' => 'desc_cidade'))
                ->join('imoveis_fotos', 'imoveis.id = imoveis_fotos.imovel_id', array('imagem'))
                ->where(array("imoveis.status='a' " . $finalidadesql . $tiposql . $bairrosql . $cidadesql . $estadosql))
                ->order(array($ordemsql))
                ->group(array("imoveis.id"));
                
//        echo $sql->getSqlstringForSqlObject($select); die();
        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(15);

        $select = $sql->select()
                ->columns(array('id' => new \Zend\Db\Sql\Expression('COUNT(id)')))
                ->from('imoveis')
                ->where(array("imoveis.status='a' " . $finalidadesql . $tiposql . $bairrosql . $cidadesql . $estadosql));
        $statement = $sql->prepareStatementForSqlObject($select);
        $total = $statement->execute();

        $select = $sql->select()
                ->from('estados')
                ->order(array("nome asc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $estados = $statement->execute();



        $select = $sql->select()
                ->from('imoveis_tipos')
                ->order(array("tipo asc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $tipos = $statement->execute();

        $title = " | Imóveis em ".str_replace("_", " ", $this->params()->fromRoute('cidade', ""))." ".$estado." ".str_replace("_", " ",$this->params()->fromRoute('bairro', ""))." ".$this->params()->fromRoute('tipo', "")." ".$this->params()->fromRoute('finalidade', "");

        $view = new ViewModel(array(
            'ordem' => $ordem,
            'finalidade' => $finalidade,
            'estado' => $estado,
            'estados' => $estados,
            'cidade' => $cidade,
            'cidades' => $cidades,
            'bairro' => $bairro,
            'bairros' => $bairros,
            'tipo' => $tipo,
            'tipos' => $tipos,
            'imoveis' => $paginator,
            'total' => $total,
            'title' => $title
        ));
        return $view;
    }

    public function blogsAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('portifolios');
        $statement = $sql->prepareStatementForSqlObject($select);
        $blogs = $statement->execute();

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $view = new ViewModel(array(
            'blogs' => $blogs,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function eventosAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $ano = "";
        $request = $this->getRequest();
        if ($request->isPost()) {
            $ano = $_POST['ano'];

            $select = $sql->select()
                    ->from('agenda')
                    ->join('agenfoto', 'agenfoto.agenda_id = agenda.id', array('imagem', 'legenda'))
                    ->where(array("academico=1 and agenfoto.principal=1 and ano=" . $ano . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $evento = $statement->execute();

            $view = new ViewModel(array(
                'evento' => $evento,
                'ano' => $ano,
                'configuracoes' => $configuracoes,
            ));
            return $view;
        } else {
            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'ano' => $ano,
            ));
            return $view;
        }
    }

    public function agendaAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $ano = "";
        $request = $this->getRequest();
        if ($request->isPost()) {
            $ano = $_POST['ano'];

            $select = $sql->select()
                    ->from('agenda')
                    ->join('agenfoto', 'agenfoto.agenda_id = agenda.id', array('imagem', 'legenda'))
                    ->where(array("academico=2", 'agenfoto.principal=1', "ano=" . $ano . "", 'ativo=1'))
                    ->order('data desc');

            $statement = $sql->prepareStatementForSqlObject($select);
            $agenda = $statement->execute();

            $view = new ViewModel(array(
                'agenda' => $agenda,
                'ano' => $ano,
                'configuracoes' => $configuracoes,
            ));
            return $view;
        } else {
            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'ano' => $ano,
            ));
            return $view;
        }
    }

    public function formandofotosAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('pagfotos')
                    ->where(array("pagina_id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $galeria = $statement->execute();

            $select = $sql->select()
                    ->columns(array('titulo'))
                    ->from('paginas')
                    ->where(array("id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $nome = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'galeria' => $galeria,
                'nome' => $nome,
            ));
            return $view;
        }
    }

    public function eventofotosAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('agenfoto')
                    ->where(array("agenda_id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $galeria = $statement->execute();

            $select = $sql->select()
                    ->columns(array('nome'))
                    ->from('agenda')
                    ->where(array("id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $nome = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'galeria' => $galeria,
                'nome' => $nome,
            ));
            return $view;
        }
    }

    public function imovelAction() {

        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($_POST['bancos'] == 1) {
                return $this->redirect()->toUrl("http://www8.caixa.gov.br/siopiinternet/simulaOperacaoInternet.do?method=inicializarCasoUso");
            }
            if ($_POST['bancos'] == 2) {
                return $this->redirect()->toUrl("http://www.santander.com.br/portal/wps/script/templates/GCMRequest.do?page=5516&entryID=6531");
            }
            if ($_POST['bancos'] == 3) {
                return $this->redirect()->toUrl("https://www42.bb.com.br/portalbb/creditoImobiliario/Proposta,2,2250,2250.bbx");
            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('produtos')
                    ->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('cnome' => 'nome'))
                    ->where(array("produtos.id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $imoveis = $statement->execute();

            $select = $sql->select()
                    ->from('profotos')
                    ->where(array("produto_id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $galeria = $statement->execute();

            $layout = $this->layout();
            $layout->setTemplate('layout/app2');
            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'galeria' => $galeria,
                'imoveis' => $imoveis,
            ));
//            $view->setTemplate('layout/app2');
            return $view;
        }
    }

    public function institucionalAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas');
//                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
//                ->where(array("pagfotos.principal=1"))
//                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();
//        echo $sql->getSqlstringForSqlObject($select); die;

        $select = $sql->select()
                ->from('subcategorias');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();


//        $layout = $this->layout();
//        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'categorias' => $subcategorias,
            'configuracoes' => $configuracoes,
            'paginas' => $paginas,
        ));
        return $view;
    }

    public function casabonitaAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas')
//                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
                ->where(array("id=41"));
//                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                ->from('pagfotos')
//                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
                ->where(array("pagina_id=41"));
//                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $fotos = $statement->execute();
//        echo $sql->getSqlstringForSqlObject($select); die;

        $select = $sql->select()
                ->from('subcategorias')
                ->order("ordem asc");
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();

        $select4 = $sql->select()
                ->from('marcas')
                ->order('nome asc');
        $statement4 = $sql->prepareStatementForSqlObject($select4);
        $marcas = $statement4->execute();


//        $layout = $this->layout();
//        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'categorias' => $subcategorias,
            'configuracoes' => $configuracoes,
            'paginas' => $paginas,
            'fotos' => $fotos,
            'marcas' => $marcas,
        ));
        return $view;
    }

    public function consumoconscienteAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas')
//                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
                ->where(array("id=51"));
//                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                ->from('pagfotos')
//                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
                ->where(array("pagina_id=51"));
//                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $fotos = $statement->execute();
//        echo $sql->getSqlstringForSqlObject($select); die;

        $select = $sql->select()
                ->from('subcategorias')
                ->order("ordem asc");
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();

        $select4 = $sql->select()
                ->from('marcas')
                ->order('nome asc');
        $statement4 = $sql->prepareStatementForSqlObject($select4);
        $marcas = $statement4->execute();


//        $layout = $this->layout();
//        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'categorias' => $subcategorias,
            'configuracoes' => $configuracoes,
            'paginas' => $paginas,
            'fotos' => $fotos,
            'marcas' => $marcas,
        ));
        return $view;
    }

    public function regulamentosAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas');
//                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
//                ->where(array("pagfotos.principal=1"))
//                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();
//        echo $sql->getSqlstringForSqlObject($select); die;

        $select = $sql->select()
                ->from('subcategorias');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();


//        $layout = $this->layout();
//        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'categorias' => $subcategorias,
            'configuracoes' => $configuracoes,
            'paginas' => $paginas,
        ));
        return $view;
    }

    public function atendimentoonlineAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas');
//                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
//                ->where(array("pagfotos.principal=1"))
//                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();
//        echo $sql->getSqlstringForSqlObject($select); die;

        $select = $sql->select()
                ->from('subcategorias');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();


//        $layout = $this->layout();
//        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'categorias' => $subcategorias,
            'configuracoes' => $configuracoes,
            'paginas' => $paginas,
        ));
        return $view;
    }

    public function trabalheconoscoAction() {
        $site_session = new Container('site');
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $form = new CurriculoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $curriculo = new Curriculo();
            $form->setInputFilter($curriculo->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('arquivo');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('arquivo' => $error));
                } else {


                    $adapter->setDestination('./public_html/data/curriculos');
                    $destination = './public_html/data/curriculos';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();

                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['arquivo'] = $newName;
                    //$data['produto_id'] = $proid;
                    $curriculo->setData($data);

                    $saved = $this->getTable('Admin\Model\Curriculo')->save($curriculo);

                    $adapter->setDestination('./public_html/data/curriculos');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];
//                        var_dump($target);

                        $curriculo->exchangeArray($form->getData());

                        $site_session = new Container('msg');
                        $site_session->msg = 'Currículo enviado com sucesso!';

//                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/curriculo/index');
                        //echo 'Profile Name ' . $curriculo->nome . ' upload ' . $curriculo->file;
                    }
                }
            }

            $select = $sql->select()
                    ->columns(array('parametro'))
                    ->from('configuracoes')
                    ->where(array('configuracoes.id=9'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $email = $statement->execute();

            foreach ($email as $para) {
                $email_destino = $para['parametro'];
            }

            //Pega os dados postados pelo formulário HTML e os coloca em variaveis
            if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
                //substitua na linha acima a aprte locaweb.com.br por seu domínio.
                $email_from = 'contato@' . $_SERVER[HTTP_HOST]; // Substitua essa linha pelo seu e-mail@seudominio
            } else {
                $email_from = "contato@" . $_SERVER[HTTP_HOST];
                //    Na linha acima estamos forçando que o remetente seja 'webmaster@',
                // você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            }


            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }

            //pego os dados enviados pelo formulário 
            $nome_para = $_POST["nome"];
            $email_de = $_POST["email"];
            $objetivo = $_POST["objetivo"];
            $perfil = $_POST["perfil"];
            $email = $email_destino;
            //            $email = "yuri@agenciaw3.com.br,yurimoreira2309@gmail.com";
            $assunto = "Novo Currículo Cadastrado no site! ";
            $mensagem = $_POST["msg"];
            //formato o campo da mensagem 
            $mensagem_cont = "
            <b>Nome: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $nome_para . "</p>
            <b>Email: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $email_de . "</p>
            <b>Objetivo Profissional: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $objetivo . "</p>
            <b>Curriculo: </b><a href='http://imoveisdeniolandim.com.br/data/curriculos/$newName'>Clique aqui para fazer o download</a><br>
            <b>Qualificações: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $perfil . "</p>";






//se nao tiver anexo 


            $headers = "MIME-Version: 1.0" . $quebra_linha . "";
            $headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha . "";
            $headers .= "From: $email_from " . $quebra_linha . "";
            $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
            mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);


//                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
        }







        $layout = $this->layout();
        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function formandosAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $estado = "";
        if (isset($_POST['estado'])) {
            $estado = " and paginas.estado='" . $_POST['estado'] . "'";
        }

        $cidade = "";
        if (isset($_POST['cidade'])) {
            $cidade = " and paginas.cidade LIKE '%" . $_POST['cidade'] . "%'";
        }

        $facul = "";
        if (isset($_POST['facul'])) {
            $facul = " and paginas.facul='" . $_POST['facul'] . "'";
        }

        $curso = "";
        if (isset($_POST['curso'])) {
            $curso = " and paginas.curso='" . $_POST['curso'] . "'";
        }

        $ano = "";
        if (isset($_POST['ano'])) {
            $ano = " and paginas.ano='" . $_POST['ano'] . "'";
        }

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas')
                ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
                ->where(array("pagfotos.principal=1 and ativo = '1' " . $estado . $cidade . $facul . $curso . $ano))
                ->group(array("paginas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $formandos = $statement->execute();

        $select = $sql->select()->from(array('p' => 'paginas'), array('ano' => 'DISTINCT(p.ano)'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $anos = $statement->execute();

        $select = $sql->select()->from(array('p' => 'paginas'), array('ano' => 'DISTINCT(p.facul)'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $faculdades = $statement->execute();

        $select = $sql->select()->from(array('p' => 'paginas'), array('ano' => 'DISTINCT(p.curso)'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $cursos = $statement->execute();

        $select = $sql->select()->from(array('p' => 'paginas'), array('ano' => 'DISTINCT(p.cidade)'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $cidades = $statement->execute();

        $view = new ViewModel(array(
            'configuracoes' => $configuracoes,
            'formandos' => $formandos,
            'anos' => $anos,
            'faculdades' => $faculdades,
            'cursos' => $cursos,
            'cidades' => $cidades,
        ));
        return $view;
    }

    public function tutorialAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('tutoriais')
                    ->join('disciplinas', 'tutoriais.nivel = disciplinas.id', array('cnome' => 'nome'))
                    ->where(array("tutoriais.id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $vagas = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'vagas' => $vagas,
            ));
            return $view;
        }
    }

    public function formandodetalhesAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('paginas')
                    ->where(array("id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $agenda = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'agenda' => $agenda,
            ));
            return $view;
        }
    }

    public function vagaAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('vagas')
                    ->join('catvagas', 'vagas.nivel = catvagas.id', array('cnome' => 'nome'))
                    ->where(array("vagas.id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $vagas = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'vagas' => $vagas,
            ));
            return $view;
        }
    }

    public function eventodetalhesAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('agenda')
                    ->where(array("id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $agenda = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'agenda' => $agenda,
            ));
            return $view;
        }
    }

    public function promocoesAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('produtos')
                ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem'))
                ->where(array("profotos.principal=1 and promocao=1"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $produtos = $statement->execute();

        $select = $sql->select()
                ->from('posts')
                ->where(array('status=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $lojas = $statement->execute();

        $view = new ViewModel(array(
            'produtos' => $produtos,
            'lojas' => $lojas,
        ));
        return $view;
    }

    public function lojasAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('posts')
                ->where(array('status=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $lojas = $statement->execute();

        $view = new ViewModel(array(
            'lojas' => $lojas
        ));
        return $view;
    }

    public function listacompletaAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('noivos')
                    ->where(array("id=$id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $noivos = $statement->execute();

            $select = $sql->select()
                    ->from('produtos')
                    ->join('lista_produtos', 'produtos.id = lista_produtos.produto_id')
                    ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem'))
                    ->where(array("lista_produtos.noivo_id=$id and profotos.principal=1"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produtos = $statement->execute();

            $select = $sql->select()
                    ->from('categorias');
            $statement = $sql->prepareStatementForSqlObject($select);
            $categorias = $statement->execute();

            $select = $sql->select()
                    ->from('subcategorias');
            $statement = $sql->prepareStatementForSqlObject($select);
            $subcategorias = $statement->execute();

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'produtos' => $produtos,
                'configuracoes' => $configuracoes,
                'categorias' => $categorias,
                'subcategorias' => $subcategorias,
                'noivos' => $noivos
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/listas');
        }
    }

    public function login2Action() {

        $request = $this->getRequest();
        if ($request->isPost()) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'email', 'senha', 'nome'))
                    ->from('clientes')
                    ->where(array("clientes.email='" . $_POST["email"] . "'", "clientes.senha='" . $_POST["senha"] . "'"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            $cli;
            foreach ($result as $cliente) {
                $cli['id'] = $cliente['id'];
                $cli['email'] = $cliente['email'];
                $cli['nome'] = $cliente['nome'];
            }
            if (!empty($result)) {
                $cliente_session = new Container('cliente');
                $cliente_session->cliente = $cli;
            }
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhaconta');
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function cadastroAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas')
                ->where(array("id=16"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $view = new ViewModel(array(
            'paginas' => $paginas,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function empresaAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                ->from('posts');
        $statement = $sql->prepareStatementForSqlObject($select);
        $lojas = $statement->execute();

        $view = new ViewModel(array(
            'lojas' => $lojas,
            'paginas' => $paginas,
        ));
        return $view;
    }

    public function simuladorAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('id=18'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                ->from('posts')
                ->where(array('status=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $lojas = $statement->execute();

        $view = new ViewModel(array(
            'lojas' => $lojas,
            'paginas' => $paginas,
        ));
        return $view;
    }

    public function dicasAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('posts')
                ->where(array('status=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $lojas = $statement->execute();

        $select = $sql->select()
                ->from('faqs');
        $statement = $sql->prepareStatementForSqlObject($select);
        $dicas = $statement->execute();

        $view = new ViewModel(array(
            'lojas' => $lojas,
            'dicas' => $dicas,
        ));
        return $view;
    }

    public function dicaAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('posts')
                    ->where(array('status=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $lojas = $statement->execute();

            $select = $sql->select()
                    ->from('faqs')
                    ->where(array('id=' . $id));
            $statement = $sql->prepareStatementForSqlObject($select);
            $dicas = $statement->execute();

            $view = new ViewModel(array(
                'lojas' => $lojas,
                'dicas' => $dicas,
            ));
            return $view;
        }
    }

    public function novidadesAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('posts')
                ->where(array('status=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $lojas = $statement->execute();

        $select = $sql->select()
                ->from('novidades');
        $statement = $sql->prepareStatementForSqlObject($select);
        $novidades = $statement->execute();

        $view = new ViewModel(array(
            'lojas' => $lojas,
            'novidades' => $novidades,
        ));
        return $view;
    }

    public function novidadeAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('posts')
                    ->where(array('status=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $lojas = $statement->execute();

            $select = $sql->select()
                    ->from('novidades')
                    ->where(array('id=' . $id));
            $statement = $sql->prepareStatementForSqlObject($select);
            $novidades = $statement->execute();

            $view = new ViewModel(array(
                'lojas' => $lojas,
                'novidades' => $novidades,
            ));
            return $view;
        }
    }

    public function localizacaoAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('portifolios');
        $statement = $sql->prepareStatementForSqlObject($select);
        $portifolios = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=6'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias1 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=7'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias2 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=8'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias3 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=9'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias4 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=10'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias5 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=11'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias6 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=12'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias7 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=13'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias8 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=14'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias9 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=15'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias10 = $statement->execute();

        $select = $sql->select()
                ->from('subcategorias');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();

        $select = $sql->select()
                ->from('galeria')
                ->where(array("status=1"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $galeria = $statement->execute();

        $select = $sql->select()
                ->from('links');
        $statement = $sql->prepareStatementForSqlObject($select);
        $links = $statement->execute();

        $view = new ViewModel(array(
            'galeria' => $galeria,
            'links' => $links,
            'categorias1' => $categorias1,
            'categorias2' => $categorias2,
            'categorias3' => $categorias3,
            'categorias4' => $categorias4,
            'categorias5' => $categorias5,
            'categorias6' => $categorias6,
            'categorias7' => $categorias7,
            'categorias8' => $categorias8,
            'categorias9' => $categorias9,
            'categorias10' => $categorias10,
            'subcategorias' => $subcategorias,
            'portifolios' => $portifolios,
        ));
        return $view;
    }

    public function noticiasAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
//                ->columns(array('id', 'parametro'))
                ->from('subcategorias');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();

        $select = $sql->select()
                ->from('posts')
                ->where(array('status=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $noticias = $statement->execute();

//        $select = $sql->select()
//                ->columns(array('id', 'titulo', 'texto'))
//                ->from('paginas')
//                ->where(array('id=19'));
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $paginas = $statement->execute();

        $view = new ViewModel(array(
            'noticias' => $noticias,
//            'paginas' => $paginas,
            'categorias' => $subcategorias,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function noticiaAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
//        $subid = (int) $this->params()->fromRoute('subid', 0);
        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

//seleciona os telefone e email
            $select = $sql->select()
                    ->columns(array('id', 'parametro'))
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
//                ->columns(array('id', 'parametro'))
                    ->from('subcategorias');
            $statement = $sql->prepareStatementForSqlObject($select);
            $subcategorias = $statement->execute();

            $select = $sql->select()
                    ->from('posts')
                    ->where(array('status=1', 'id=' . $id));
            $statement = $sql->prepareStatementForSqlObject($select);
            $noticias = $statement->execute();

//        $select = $sql->select()
//                ->columns(array('id', 'titulo', 'texto'))
//                ->from('paginas')
//                ->where(array('id=19'));
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $paginas = $statement->execute();

            $view = new ViewModel(array(
                'noticias' => $noticias,
//            'paginas' => $paginas,
                'categorias' => $subcategorias,
                'configuracoes' => $configuracoes,
            ));
            return $view;
        }
    }

    public function estagiosAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $cod = "";
        if (isset($_POST['codigo'])) {
            $cod = " and locacoes.codigo='" . $_POST['codigo'] . "'";
        }

        $estado = "";
        if (isset($_POST['estado'])) {
            $estado = " and locacoes.estado='" . $_POST['estado'] . "'";
        }

        $nivel_academico = "";
        if (isset($_POST['nivel_academico'])) {
            $nivel_academico = " and locacoes.nivel='" . $_POST['nivel_academico'] . "'";
        }

        $palavra_chave = "";
        if (isset($_POST['palavra_chave'])) {
            $palavra_chave = " and locacoes.texto LIKE '%" . $_POST['palavra_chave'] . "%'";
        }

        $cidade = "";
        if (isset($_POST['cidade'])) {
            $cidade = " and locacoes.cidade LIKE '%" . $_POST['cidade'] . "%'";
        }

        $area_profissional = "";
        if (isset($_POST['area_profissional'])) {
            $area_profissional = " and locacoes.area LIKE '%" . $_POST['area_profissional'] . "%'";
        }

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        if (isset($_POST['codigo'])) {

            $select = $sql->select()
                    ->from('locacoes')
                    ->where(array("locacoes.id=locacoes.id and ativo = '1' " . $estado . $nivel_academico . $palavra_chave . $cidade . $area_profissional))
                    ->group(array("locacoes.id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $estagios = $statement->execute();
        } else {

            $select = $sql->select()
                    ->from('locacoes')
                    ->where(array("locacoes.id=locacoes.id and ativo = '1'" . $cod . $estado . $nivel_academico . $palavra_chave . $cidade . $area_profissional))
                    ->group(array("locacoes.id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $estagios = $statement->execute();
        }

        $view = new ViewModel(array(
            'estagios' => $estagios,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function tutoriaisAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $cat = "";
        if (isset($_POST['disciplina'])) {
            $cat = " and tutoriais.nivel='" . $_POST['disciplina'] . "'";
        }

        $estado = "";
        if (isset($_POST['estado'])) {
            $estado = " and tutoriais.estado='" . $_POST['estado'] . "'";
        }

        $cidade = "";
        if (isset($_POST['cidade'])) {
            $cidade = " and tutoriais.cidade LIKE '%" . $_POST['cidade'] . "%'";
        }

        $turno = "";
        if (isset($_POST['horario'])) {
            $turno = " and tutoriais.area='" . $_POST['horario'] . "'";
        }

        $preco = "";
        if (isset($_POST['faixa_preco_min']) && isset($_POST['faixa_preco_max']) && $_POST['faixa_preco_min'] != 0 && $_POST['faixa_preco_max'] != 0) {
            $preco = " and tutoriais.codigo>=" . $_POST['faixa_preco_min'] . " and tutoriais.codigo<=" . $_POST['faixa_preco_max'];
        }

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('disciplinas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias = $statement->execute();

        $select = $sql->select()
                ->from('tutoriais')
                ->where(array("tutoriais.id=tutoriais.id" . $cat . $estado . $cidade . $turno . $preco))
                ->group(array("tutoriais.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $tutoriais = $statement->execute();

        $select = $sql->select()
                ->from('marcas')
                ->where(array("local=6"))
                ->order(array("id desc"));
        ;
        $statement = $sql->prepareStatementForSqlObject($select);
        $publi6 = $statement->execute();

        $view = new ViewModel(array(
            'publi6' => $publi6,
            'tutoriais' => $tutoriais,
            'configuracoes' => $configuracoes,
            'categorias' => $categorias,
        ));
        return $view;
    }

    public function vagasAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $cat = "";
        if (isset($_POST['area'])) {
            $cat = " and vagas.nivel='" . $_POST['area'] . "'";
        }

        $estado = "";
        if (isset($_POST['estado'])) {
            $estado = " and vagas.estado='" . $_POST['estado'] . "'";
        }

        $cidade = "";
        if (isset($_POST['cidade'])) {
            $cidade = " and vagas.cidade LIKE '%" . $_POST['cidade'] . "%'";
        }

        $turno = "";
        if (isset($_POST['turno'])) {
            $turno = " and vagas.area='" . $_POST['turno'] . "'";
        }

        $preco = "";
        if (isset($_POST['faixa_preco_min']) && isset($_POST['faixa_preco_max']) && $_POST['faixa_preco_min'] != 0 && $_POST['faixa_preco_max'] != 0) {
            $preco = " and vagas.codigo>=" . $_POST['faixa_preco_min'] . " and vagas.codigo<=" . $_POST['faixa_preco_max'];
        }

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('catvagas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias = $statement->execute();

        $select = $sql->select()
                ->from('vagas')
                ->where(array("vagas.id=vagas.id and vagas.ativo='1' " . $cat . $estado . $cidade . $turno . $preco))
                ->group(array("vagas.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $vagas = $statement->execute();

        $select = $sql->select()
                ->from('marcas')
                ->where(array("local=5"))
                ->order(array("id desc"));
        ;
        $statement = $sql->prepareStatementForSqlObject($select);
        $publi5 = $statement->execute();

        $view = new ViewModel(array(
            'publi5' => $publi5,
            'vagas' => $vagas,
            'configuracoes' => $configuracoes,
            'categorias' => $categorias,
        ));
        return $view;
    }

    public function servicosAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $cat = "";
        if (isset($_POST['categoria'])) {
            $cat = " and servicos.area='" . $_POST['categoria'] . "'";
        }

        $tipo = "";
        if (isset($_POST['tipo'])) {
            $tipo = " and servicos.iframe='" . $_POST['tipo'] . "'";
        }

        $preco = "";
        if (isset($_POST['faixa_preco_min']) && isset($_POST['faixa_preco_max']) && $_POST['faixa_preco_min'] != 0 && $_POST['faixa_preco_max'] != 0) {
            $preco = " and servicos.telefone>=" . $_POST['faixa_preco_min'] . " and servicos.telefone<=" . $_POST['faixa_preco_max'];
        }

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('marcas')
                ->where(array("local=3"))
                ->order(array("id desc"));
        ;
        $statement = $sql->prepareStatementForSqlObject($select);
        $publi3 = $statement->execute();

        $select = $sql->select()
                ->from('catservicos');
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias = $statement->execute();

        $select = $sql->select()
                ->from('servicos')
                ->where(array("servicos.id=servicos.id" . $cat . $tipo . $preco))
                ->group(array("servicos.id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $servicos = $statement->execute();

        $view = new ViewModel(array(
            'publi3' => $publi3,
            'servicos' => $servicos,
            'configuracoes' => $configuracoes,
            'categorias' => $categorias,
        ));
        return $view;
    }

    public function eveAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {
            $select = $sql->select()
                    ->from('eventos')
                    ->where(array('id=' . $id));

            $statement = $sql->prepareStatementForSqlObject($select);
            $servico = $statement->execute();

            $view = new ViewModel(array(
                'servico' => $servico,
                'configuracoes' => $configuracoes,
            ));
            return $view;
        }
    }

    public function estagioAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('locacoes')
                    ->where(array("locacoes.id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $estagios = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'estagios' => $estagios,
            ));
            return $view;
        }
    }

    public function servicoAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('servicos')
                    ->join('catservicos', 'servicos.area = catservicos.id', array('cnome' => 'nome'))
                    ->where(array("servicos.id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $servicos = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'servicos' => $servicos,
            ));
            return $view;
        }
    }

    public function blogAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id != 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('portifolios')
                    ->where(array("portifolios.id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $blogs = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'blogs' => $blogs,
            ));
            return $view;
        }
    }

    public function locacaoAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('locacoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $locacoes = $statement->execute();

        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $view = new ViewModel(array(
            'locacoes' => $locacoes,
            'paginas' => $paginas,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function midiaAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);


        $request = $this->getRequest();
        if ($request->isPost()) {

            $adapter1 = $this->getServiceLocator()->get('DbAdapter');
            $sql1 = new Sql($adapter1);

            $sql1 = "INSERT INTO newsletter (nome,email) VALUES ('" . $_POST['nome_news'] . "','" . $_POST['email_news'] . "');";
            $statement = $adapter1->query($sql1);
            $inserir = $statement->execute();
        }

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('videos');
        $statement = $sql->prepareStatementForSqlObject($select);
        $videos = $statement->execute();

        $select = $sql->select()
                // ->columns(array('id', 'titulo', 'texto'))
                ->from('pagfotos')
                ->where(array('pagina_id=25'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $galeria = $statement->execute();

        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $view = new ViewModel(array(
            'videos' => $videos,
            'paginas' => $paginas,
            'galeria' => $galeria,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function conveniosAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);


        $request = $this->getRequest();
        if ($request->isPost()) {

            $adapter1 = $this->getServiceLocator()->get('DbAdapter');
            $sql1 = new Sql($adapter1);

            $sql1 = "INSERT INTO newsletter (nome,email) VALUES ('" . $_POST['nome_news'] . "','" . $_POST['email_news'] . "');";
            $statement = $adapter1->query($sql1);
            $inserir = $statement->execute();
            echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
        }

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('marcas')
                ->where(array('ativo=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $convenios = $statement->execute();

        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $view = new ViewModel(array(
            'convenios' => $convenios,
            'paginas' => $paginas,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function clientesAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);


//seleciona os faqs
        $select = $sql->select()
                ->from('parceiros')
                ->order(array("id desc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $parceiros = $statement->execute();

        $select = $sql->select()
                ->from('paginas')
                ->where(array('paginas.id=10'))
                ->order(array("id desc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $view = new ViewModel(array(
            'paginas' => $paginas,
            'parceiros' => $parceiros,
        ));
        return $view;
    }

    public function orcamentoAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

//seleciona contato
        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('paginas.id=25'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                ->columns(array('parametro'))
                ->from('configuracoes')
                ->where(array('configuracoes.id=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $email = $statement->execute();

        foreach ($email as $para) {
            $email_destino = $para['parametro'];
        }

        /* Enviar Email */
        $request = $this->getRequest();

        if ($request->isPost()) {

//Pega os dados postados pelo formulário HTML e os coloca em variaveis
            if (eregi('tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$', $_SERVER[HTTP_HOST])) {
//substitua na linha acima a aprte locaweb.com.br por seu domínio.
                $email_from = 'orcamento@dentalevolux.com.br'; // Substitua essa linha pelo seu e-mail@seudominio
            } else {
                $email_from = "orcamento@" . $_SERVER[HTTP_HOST];
//    Na linha acima estamos forçando que o remetente seja 'webmaster@',
// você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            }


            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }

//pego os dados enviados pelo formulário 
            $nome_para = $_POST["nome"];
            $email_de = $_POST["email_de"];
            $email = $email_destino;
//            $email = "yuri@agenciaw3.com.br,yurimoreira2309@gmail.com";
            $mensagem = $_POST["obs"];
            $assunto = "Orçamento do site Dental Evolux";
//formato o campo da mensagem 
            $mensagem_cont = "
            <b>Nome: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $nome_para . "</p>
            <b>Email:</b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $email_de . "</p>
            <b>Observações:</b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $mensagem . "</p>";

//valido os emails 
            if (!ereg("^([0-9,a-z,A-Z]+)([.,_]([0-9,a-z,A-Z]+))*[@]([0-9,a-z,A-Z]+)([.,_,-]([0-9,a-z,A-Z]+))*[.]([0-9,a-z,A-Z]){2}([0-9,a-z,A-Z])?$", $email_de)) {

                echo"<center>Digite um email valido</center>";
                echo "<center><a href=\"javascript:history.go(-1)\">Voltar</center></a>";
                exit;
            }

            $arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

            if (file_exists($arquivo["tmp_name"]) and ! empty($arquivo)) {

                $fp = fopen($_FILES["arquivo"]["tmp_name"], "rb");
                $anexo = fread($fp, filesize($_FILES["arquivo"]["tmp_name"]));
                $anexo = base64_encode($anexo);

                fclose($fp);

                $anexo = chunk_split($anexo);


                $boundary = "XYZ-" . date("dmYis") . "-ZYX";

                $mens = "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: 8bits" . $quebra_linha . "";
                $mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $quebra_linha . "" . $quebra_linha . ""; //plain 
                $mens .= "$mensagem_cont" . $quebra_linha . "";
                $mens .= "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Type: " . $arquivo["type"] . "" . $quebra_linha . "";
                $mens .= "Content-Disposition: attachment; filename=\"" . $arquivo["name"] . "\"" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: base64" . $quebra_linha . "" . $quebra_linha . "";
                $mens .= "$anexo" . $quebra_linha . "";
                $mens .= "--$boundary--" . $quebra_linha . "";

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";
                $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"" . $quebra_linha . "";
                $headers .= "$boundary" . $quebra_linha . "";


//envio o email com o anexo 
                mail($email, $assunto, $mens, $headers, "-r" . $email_from);

                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }

//se nao tiver anexo 
            else {

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "Content-type: text/html; charset=iso-8859-1" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
                mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);


                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }
        }

        $view = new ViewModel(array(
            'configuracoes' => $configuracoes,
            'paginas' => $paginas,
        ));
        return $view;
    }

    public function maquinaAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $request = $this->getRequest();
        if ($request->isPost()) {

            $adapter1 = $this->getServiceLocator()->get('DbAdapter');
            $sql1 = new Sql($adapter1);

            $sql1 = "INSERT INTO newsletter (nome,email) VALUES ('" . $_POST['nome_news'] . "','" . $_POST['email_news'] . "');";
            $statement = $adapter1->query($sql1);
            $inserir = $statement->execute();
            echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
        }

        $select = $sql->select()
                ->from('subcategorias')
// ->where(array('subcategorias.id='.$subcat))
                ->order(array("id asc"))
                ->limit('1');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias3 = $statement->execute();

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $id = (int) $this->params()->fromRoute('id', 0);

        if ($id != 0) {
//seleciona paginas
            $select = $sql->select()
//                    ->columns(array('id', 'titulo', 'texto'))
                    ->from('portifolios')
                    ->where(array('id=' . $id));

            $statement = $sql->prepareStatementForSqlObject($select);
            $portifolios = $statement->execute();

            $view = new ViewModel(array(
                'portifolios' => $portifolios,
                'subcategorias3' => $subcategorias3,
                'configuracoes' => $configuracoes,
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl('/application/index');
        }
    }

    public function postAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $id = (int) $this->params()->fromRoute('id', 0);

        if ($id != 0) {
//seleciona paginas
            $select = $sql->select()
//                    ->columns(array('id', 'titulo', 'texto'))
                    ->from('posts')
                    ->where(array('id=' . $id));

            $statement = $sql->prepareStatementForSqlObject($select);
            $posts = $statement->execute();

            $view = new ViewModel(array(
                'posts' => $posts,
                'configuracoes' => $configuracoes,
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function trabalheAction() {
        $site_session = new Container('site');
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->columns(array('parametro'))
                ->from('configuracoes')
                ->where(array('configuracoes.id=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $email = $statement->execute();

        foreach ($email as $para) {
            $email_destino = $para['parametro'];
        }

        /* Enviar Email */
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Pega os dados postados pelo formulário HTML e os coloca em variaveis
            if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
                $email_from = "trabalhe_conosco@docedocemoc.com"; // Substitua essa linha pelo seu e-mail@seudominio
            } else {
                $email_from = "trabalhe_conosco@docedocemoc.com";
//    Na linha acima estamos forçando que o remetente seja 'webmaster@',
// você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            }

            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }

//pego os dados enviados pelo formulário 
            $nome = $_POST["nome"];
            $data_nasc = $_POST["data_nasc"];
            $endereco = $_POST["endereco"];
            $email = $email_destino;
            $bairro = $_POST["bairro"];
            $cidade = $_POST["cidade"];
            $estado = $_POST["estado"];
            $tel = $_POST["tel"];
            $email_trabalhe = $_POST["email_trabalhe"];
            $escolaridade = $_POST["escolaridade"];
            $pretensao = $_POST["pretensao"];
            $area = $_POST["area"];

            $empresa = $_POST["empresa"];
            $funcao = $_POST["funcao"];
            $periodo_inicio = $_POST["periodo_inicio"];
            $periodo_termino = $_POST["periodo_termino"];
            $referencia_nome = $_POST["referencia_nome"];
            $referencia_tel = $_POST["referencia_tel"];

            $assunto = "TRABALHE CONOSCO - DOCE DOCE FESTAS";
//formato o campo da mensagem 
            $mensagem = "
			<center><b style='font-size:22px'>Dados Essenciais</b></center>
            <b style='font-size:17px;'>Nome: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $nome . "</p>
            <b style='font-size:17px;'>Data de Nascimento: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $data_nasc . "</p>
            <b style='font-size:17px;'>Endereço: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $endereco . "</p>
            <b style='font-size:17px;'>Bairro: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $bairro . "</p>
            <b style='font-size:17px;'>Cidade: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $cidade . "</p>
            <b style='font-size:17px;'>Estado: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $estado . "</p>
            <b style='font-size:17px;'>Telefone: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $tel . "</p>
            <b style='font-size:17px;'>Email: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $email_trabalhe . "</p>
            <b style='font-size:17px;'>Escolaridade: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $escolaridade . "</p>
            <b style='font-size:17px;'>Pretensão Salarial: </b><p style='text-align:justify;font-size:14px;line-height:22px'>R$ " . $pretensao . "</p>
            <b style='font-size:17px;'>Área Pretendida: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $area . "</p>
			<br><br><br>
			<center><b style='font-size:22px'>Última Experiência Profissional</b></center>
            <b style='font-size:17px;'>Empresa: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $empresa . "</p>
            <b style='font-size:17px;'>Função: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $funcao . "</p>
            <b style='font-size:17px;'>Período - Início: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $periodo_inicio . "</p>
            <b style='font-size:17px;'>Período - Término: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $periodo_termino . "</p>
            <b style='font-size:17px;'>Referência - Nome: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $referencia_nome . "</p>
            <b style='font-size:17px;'>Referência - Telefone: </b><p style='text-align:justify;font-size:14px;line-height:22px'>" . $referencia_tel . "</p>";


            $arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

            if (file_exists($arquivo["tmp_name"]) and ! empty($arquivo)) {

                $fp = fopen($_FILES["arquivo"]["tmp_name"], "rb");
                $anexo = fread($fp, filesize($_FILES["arquivo"]["tmp_name"]));
                $anexo = base64_encode($anexo);

                fclose($fp);

                $anexo = chunk_split($anexo);


                $boundary = "XYZ-" . date("dmYis") . "-ZYX";

                $mens = "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: 8bits" . $quebra_linha . "";
                $mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $quebra_linha . "" . $quebra_linha . ""; //plain 
                $mens .= "$mensagem" . $quebra_linha . "";
                $mens .= "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Type: " . $arquivo["type"] . "" . $quebra_linha . "";
                $mens .= "Content-Disposition: attachment; filename=\"" . $arquivo["name"] . "\"" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: base64" . $quebra_linha . "" . $quebra_linha . "";
                $mens .= "$anexo" . $quebra_linha . "";
                $mens .= "--$boundary--" . $quebra_linha . "";

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";
                $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"" . $quebra_linha . "";
                $headers .= "$boundary" . $quebra_linha . "";


//envio o email com o anexo 
                mail($email, $assunto, $mens, $headers, "-r" . $email_from);

                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }

//se nao tiver anexo 
            else {

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "Content-type: text/html; charset=iso-8859-1" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
                mail($email, $assunto, $mensagem, $headers, "-r" . $email_from);
                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }
        }

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=6'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias1 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=7'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias2 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=8'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias3 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=9'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias4 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=10'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias5 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=11'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias6 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=12'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias7 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=13'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias8 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=14'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias9 = $statement->execute();

        $select = $sql->select()
                ->from('categorias')
                ->where(array('marca_id=15'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $categorias10 = $statement->execute();

        $select = $sql->select()
                ->from('subcategorias');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();

        $select = $sql->select()
                ->from('galeria')
                ->where(array("status=1"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $galeria = $statement->execute();

        $select = $sql->select()
                ->from('links');
        $statement = $sql->prepareStatementForSqlObject($select);
        $links = $statement->execute();

        $view = new ViewModel(array(
            'galeria' => $galeria,
            'links' => $links,
            'categorias1' => $categorias1,
            'categorias2' => $categorias2,
            'categorias3' => $categorias3,
            'categorias4' => $categorias4,
            'categorias5' => $categorias5,
            'categorias6' => $categorias6,
            'categorias7' => $categorias7,
            'categorias8' => $categorias8,
            'categorias9' => $categorias9,
            'categorias10' => $categorias10,
            'subcategorias' => $subcategorias,
                // 'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function galeriaAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                // ->columns(array('id', 'titulo', 'texto'))
                ->from('pagfotos')
                ->where(array('pagfotos.pagina_id=15'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $galeria = $statement->execute();

        $view = new ViewModel(array(
            'galeria' => $galeria
        ));
        return $view;
    }

    public function videosAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
//       ->columns(array('id', 'titulo', 'texto', 'imagem','data'))
                ->from('videos');
        $statement = $sql->prepareStatementForSqlObject($select);
        $videos = $statement->execute();

        $select = $sql->select()
                ->from('posts')
                ->where(array('status=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $lojas = $statement->execute();

        $view = new ViewModel(array(
            'lojas' => $lojas,
            'videos' => $videos,
        ));
        return $view;
    }

    public function sobreAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona paginas
        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('paginas.id=13'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                // ->columns(array('id', 'titulo', 'texto'))
                ->from('pagfotos')
                ->where(array('pagfotos.pagina_id=13'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $fotos = $statement->execute();

        $select = $sql->select()
//                ->columns(array('id', 'titulo', 'texto'))
                ->from('videos');
        $statement = $sql->prepareStatementForSqlObject($select);
        $videos = $statement->execute();


        $select = $sql->select()
// ->columns(array('id', 'titulo', 'texto'))
                ->from('faqs');
        $statement = $sql->prepareStatementForSqlObject($select);
        $sobre = $statement->execute();

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();


        $view = new ViewModel(array(
            'sobre' => $sobre,
            'paginas' => $paginas,
            'fotos' => $fotos,
            'configuracoes' => $configuracoes,
            'videos' => $videos,
        ));
        return $view;
    }

    public function cadastrardepoimentoAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os links uteis home
        $select2 = $sql->select()
                ->columns(array('id', 'descricao', 'link'))
                ->from('links')
                ->order(array("descricao asc"));

        $statement2 = $sql->prepareStatementForSqlObject($select2);
        $links = $statement2->execute();

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $Depoimento = $this->getTable('Admin\Model\Depoimento');
        $sql = $Depoimento->getSql();
        $select = $sql->select()
                ->where(array('status=1'));

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $view = new ViewModel(array(
            'depoimentos' => $paginator,
            'configuracoes' => $configuracoes,
            'links' => $links,
        ));
        return $view;
    }

    public function assistenciaAction() {

        $request = $this->getRequest();

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->columns(array('parametro'))
                ->from('configuracoes')
                ->where(array('configuracoes.id=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $email = $statement->execute();

        foreach ($email as $para) {
            $email_destino = $para['parametro'];
        }

        if ($request->isPost()) {

            $subject = 'Assistência Técnica pelo site trajetomoveis.com.br';

            $nome = $_POST['nome'];

            $cidade = $_POST['cidade'];

            $nome_comprador = $_POST['nome_comprador'];

            $num_compra = $_POST['num_compra'];

            $data_compra = $_POST['data_compra'];

            $email = $_POST['email'];

            $telefone = $_POST['telefone'];

// $cidade = $_POST['cidade_estado'];

            $msg2 = $_POST['mensagem'];

            $msg = "NOME: $nome \n\n
E-MAIL: $email\n\n
Cidade/Estado: $cidade\n\n 
Nome Comprador: $nome_comprador\n\n 
Número da Compra: $num_compra\n\n 
Data da Compra: $data_compra\n\n 
Telefone: $telefone\n\n 
MENSAGEM: $msg2";

            $mailheaders = "From: https://www.trajetomoveis.com.br";


            if (mail("$email_destino", "$subject", "$msg", "$mailheaders")) {
                echo "<script>alert('Sua Mensagem foi enviada! Obrigado pela visita!');</script>";
                echo "<script>window.location = 'index';</script>";
            } else {
                echo "<script>alert('Tente novamente mais tarde!')</script>";
                echo "<script>window.location = 'index';</script>";
            }
        }

        $view = new ViewModel(array(
//            'clientes' => $paginator,
            'configuracoes' => $configuracoes,
//            'links' => $links,
        ));
        return $view;
    }

    public function sugestoesAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

//seleciona contato
        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas');

        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        if (isset($_POST['nome_news']) && isset($_POST['email_news'])) {
            $request = $this->getRequest();
            if ($request->isPost()) {

                $adapter1 = $this->getServiceLocator()->get('DbAdapter');
                $sql1 = new Sql($adapter1);

                $sql1 = "INSERT INTO newsletter (nome,email) VALUES ('" . $_POST['nome_news'] . "','" . $_POST['email_news'] . "');";
                $statement = $adapter1->query($sql1);
                $inserir = $statement->execute();
                echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }
        }

        $select = $sql->select()
                ->columns(array('parametro'))
                ->from('configuracoes')
                ->where(array('configuracoes.id=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $email = $statement->execute();

        foreach ($email as $para) {
            $email_destino = $para['parametro'];
        }

        /* Enviar Email */
        $request = $this->getRequest();

        if ($request->isPost()) {

//Pega os dados postados pelo formulário HTML e os coloca em variaveis
            if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
//substitua na linha acima a aprte locaweb.com.br por seu domínio.
                $email_from = 'registre_experiencia@' . $_SERVER[HTTP_HOST]; // Substitua essa linha pelo seu e-mail@seudominio
            } else {
                $email_from = "registre_experiencia@" . $_SERVER[HTTP_HOST];
//    Na linha acima estamos forçando que o remetente seja 'webmaster@',
// você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            }


            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }

//pego os dados enviados pelo formulário 
            $nome_para = $_POST["nome_sugestoes"];
            $email_de = $_POST["email_sugestoes"];
            $email = $email_destino;
//            $email = "yuri@agenciaw3.com.br,yurimoreira2309@gmail.com";
            $assunto = "Registre sua  Experiência - " . $_POST["tipo_sugestao"];
            $area = $_POST["area_sugestoes"];
            $mensagem = $_POST["mensagem_sugestoes"];
            //formato o campo da mensagem 
            $mensagem_cont = "
            <b>Nome: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $nome_para . "</p>
            <b>Email: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $email_de . "</p>
            <b>Assunto: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $assunto . "</p>
            <b>Área ou Setor: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $area . "</p>
            <b>Mensagem:</b><br><p style='text-align:justify;font-size:16px;line-height:22px'> " . $mensagem . "</p>";


            $arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

            if (file_exists($arquivo["tmp_name"]) and ! empty($arquivo)) {

                $fp = fopen($_FILES["arquivo"]["tmp_name"], "rb");
                $anexo = fread($fp, filesize($_FILES["arquivo"]["tmp_name"]));
                $anexo = base64_encode($anexo);

                fclose($fp);

                $anexo = chunk_split($anexo);


                $boundary = "XYZ-" . date("dmYis") . "-ZYX";

                $mens = "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: 8bits" . $quebra_linha . "";
                $mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $quebra_linha . "" . $quebra_linha . ""; //plain 
                $mens .= "$mensagem_cont" . $quebra_linha . "";
                $mens .= "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Type: " . $arquivo["type"] . "" . $quebra_linha . "";
                $mens .= "Content-Disposition: attachment; filename=\"" . $arquivo["name"] . "\"" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: base64" . $quebra_linha . "" . $quebra_linha . "";
                $mens .= "$anexo" . $quebra_linha . "";
                $mens .= "--$boundary--" . $quebra_linha . "";

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";
                $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"" . $quebra_linha . "";
                $headers .= "$boundary" . $quebra_linha . "";


//envio o email com o anexo 
                mail($email, $assunto, $mens, $headers, "-r" . $email_from);

                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }

//se nao tiver anexo 
            else {

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "Content-type: text/html; charset=iso-8859-1" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
                mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);


                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }
        }

        $view = new ViewModel(array(
            'configuracoes' => $configuracoes,
            'paginas' => $paginas,
        ));
        return $view;
    }

    public function contatoAction() {

        $site_session = new Container('site');

        $adapter = $this->getServiceLocator()->get('DbAdapter');

        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('marcas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $marcas = $statement->execute();

        $select = $sql->select()
                ->from('subcategorias')
                ->order("ordem asc");
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();

        $select = $sql->select()
                ->from('paginas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                ->columns(array('parametro'))
                ->from('configuracoes')
                ->where(array('configuracoes.id=9'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $email = $statement->execute();

        foreach ($email as $para) {
            $email_destino = $para['parametro'];
        }

        /* Enviar Email */
        $request = $this->getRequest();

        if ($request->isPost()) {

            //Pega os dados postados pelo formulário HTML e os coloca em variaveis
            if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
                //substitua na linha acima a aprte locaweb.com.br por seu domínio.
                $email_from = 'contato@' . $_SERVER[HTTP_HOST]; // Substitua essa linha pelo seu e-mail@seudominio
            } else {
                $email_from = "contato@" . $_SERVER[HTTP_HOST];
                //    Na linha acima estamos forçando que o remetente seja 'webmaster@',
                // você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            }


            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }

            //pego os dados enviados pelo formulário 
            $nome_para = $_POST["nome"];
            $email_de = $_POST["email"];
            $email = $email_destino;
            //            $email = "yuri@agenciaw3.com.br,yurimoreira2309@gmail.com";
            $assunto = "Contato do site - " . $_POST["assunto"];
            $info = $_POST["info"];
            $outros = $_POST["outros"];
            $mensagem = $_POST["mensagem"];
            //formato o campo da mensagem 
            $mensagem_cont = "
            <b>Nome: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $nome_para . "</p>
            <b>Email: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $email_de . "</p>
            <b>Assunto: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $assunto . "</p>
            <b>Como ficou sabendo do site: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $info . $outros . "</p>
            <b>Mensagem:</b><br><p style='text-align:justify;font-size:16px;line-height:22px'> " . $mensagem . "</p>";




            $headers = "MIME-Version: 1.0" . $quebra_linha . "";
            $headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha . "";
            $headers .= "From: $email_from " . $quebra_linha . "";
            $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
            mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);

            $site_session = new Container('msg');
            $site_session->msg = 'Mensagem enviada com sucesso!';

//                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
        }



//        $layout = $this->layout();
//        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'configuracoes' => $configuracoes,
            'categorias' => $subcategorias,
            'paginas' => $paginas,
            'marcas' => $marcas,
        ));
        return $view;
    }

    public function editarcontaAction() {

        $cliente_session = new Container('cliente');

        if (isset($cliente_session->cliente)) {

            $form = new ClienteForm();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $cliente = new Cliente;
                $form->setInputFilter($cliente->getInputFilter());
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $data = $form->getData();
//                $data = $request->getPost();
                    unset($data['submit']);
                    if (isset($data['confirmasenha'])) {
                        unset($data['confirmasenha']);
                    }
                    $data['data'] = date('Y-m-d H:i:s');
//                    echo $data['telefone'];
                    $cliente->setData($data);

                    $saved = $this->getTable('Admin\Model\Cliente')->save($cliente);

                    $cliente_session = new Container('msg');
                    $cliente_session->msg = 'Informações Alteradas com sucesso!';
//$cliente_session = new Container('cliente');
//$cliente_session->cliente = $data;
                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhaconta');
                }
            }

            /* Seleciona produtos em promoção */
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'nome', 'email', 'telefone', 'cpf', 'rua', 'bairro', 'numero', 'cidade', 'estado', 'cep'))
                    ->from('clientes')
                    ->where(array('clientes.id=' . $cliente_session->cliente['id']));

            $statement = $sql->prepareStatementForSqlObject($select);
            $cliente = $statement->execute();

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'cliente' => $cliente
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function mudarsenhaAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {

            $request = $this->getRequest();
            if ($request->isPost()) {
                $cliente = new Cliente;

                $data['id'] = $cliente_session->cliente['id'];
                $data['senha'] = $this->getRequest()->getPost('senha', null);



                $cliente->setData($data);
                if ($this->getRequest()->getPost('senha', null) == $this->getRequest()->getPost('confirmar_senha', null)) {
                    $saved = $this->getTable('Admin\Model\Cliente')->save($cliente);

                    $cliente_session = new Container('msg');
                    $cliente_session->msg = 'Senha alterada com sucesso!';
                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhaconta');
                } else {
                    $cliente_session = new Container('msg');
                    $cliente_session->msg = 'As senhas precisam ser iguais!';

                    $view = new ViewModel();
                    return $view;
                }
            }

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function pedidoscontaAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'data', 'frete', 'valor_produtos', 'valor_frete', 'status', 'pago', 'clientes_id'))
                    ->from('pedidos')
                    ->join('clientes', 'clientes.id = pedidos.clientes_id', array('cnome' => 'nome'))
                    ->where(array('clientes.id=' . $cliente_session->cliente['id']));

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'pedidos' => $paginator,
//'clientes' => $clientes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function pedidocontaAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {

            $id = (int) $this->params()->fromRoute('id', 0);

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'data', 'frete', 'valor_produtos', 'valor_frete', 'status', 'pago', 'clientes_id'))
                    ->from('pedidos')
                    ->join('clientes', 'clientes.id = pedidos.clientes_id', array('cnome' => 'nome'))
                    ->where(array('pedidos.id=' . $id));

            $statement = $sql->prepareStatementForSqlObject($select);
            $pedido = $statement->execute();


//$adapter = $this->getServiceLocator()->get('DbAdapter');
//$sql = new Sql($adapter);
            $select2 = $sql->select()
                    ->columns(array('id', 'produto_id', 'pedido_id', 'quantidade'))
                    ->from('pedido_produtos')
                    ->join('produtos', 'pedido_produtos.produto_id = produtos.id', array('nome', 'preco', 'preco_promocao', 'promocao'))
                    ->where(array('pedido_produtos.pedido_id=' . $id));

            $statement2 = $sql->prepareStatementForSqlObject($select2);
            $produtos = $statement2->execute();


            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'pedidos' => $pedido,
                'produtos' => $produtos
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function enviaremail($assunto, $destinatario, $mensagem) {

        $mailheaders = "From: Antares Motos";

        if (mail($destinatario, "$assunto", "$mensagem", "$mailheaders")) {
            return true;
        } else {
            return false;
        }
    }

    public function sairAction() {
        $cliente_session = new Container('cliente');
        $cliente_session->cliente = '';
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
    }

    public function logincontaAction() {

        $cliente_session = new Container('cliente');
        if (empty($cliente_session->cliente)) {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $data = $request->getPost();
                $data = $data->toArray();
//                $cli = '';
                $adapter = $this->getServiceLocator()->get('DbAdapter');
                $sql = new Sql($adapter);
                $select = $sql->select()
                        ->columns(array('id', 'email', 'nome', 'telefone', 'cpf', 'rua', 'bairro', 'numero', 'cidade', 'estado', 'cep'))
                        ->from('clientes')
                        ->where(array("clientes.email='" . $data['email'] . "'", "clientes.senha='" . $data['senha'] . "'"));

                $statement = $sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();
                foreach ($result as $cliente) {
                    $cli['id'] = $cliente['id'];
                    $cli['email'] = $cliente['email'];
                    $cli['nome'] = $cliente['nome'];
                }
                if (!isset($cli)) {
                    $cliente_session = new Container('msg');
                    $cliente_session->msg = 'Usuario ou Senha incorreta';
                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
                } else {
                    if (empty($cli)) {
                        $cliente_session = new Container('msg');
                        $cliente_session->msg = 'Usuario ou Senha incorreta';
                    }
                    $cliente_session->cliente = $cli;
                }


                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhaconta');
            } else {
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
            }
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhaconta');
//            echo "esta aqui: sessão não esta fazia";
//            $view = new ViewModel();
//            return $view;
        }
    }

    public function minhacontaAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function curriculoAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $curriculo = $this->getTable('Admin\Model\Curriculo');
            $sql = $curriculo->getSql();
            $select = $sql->select()->where(array("cliente_id=" . $cliente_session->cliente['id'] . ""));

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


//        $view = new ViewModel(array(
//            'curriculos' => $paginator
//        ));
//        return $view;

            $view = new ViewModel(array(
                'curriculos' => $paginator,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function excluircurriculoAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $id = (int) $this->params()->fromRoute('id', 0);

            if ($id == 0) {
                throw new \Exception("Código obrigatório");
            }

            $Curriculo = $this->getTable('Admin\Model\Curriculo')->get($id);

            unlink('./public_html/data/curriculos/' . $Curriculo->arquivo);

            $this->getTable('Admin\Model\Curriculo')->delete($id);
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/curriculo');
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function novocurriculoAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $form = new CurriculoForm();
            $request = $this->getRequest();
            if ($request->isPost()) {

                $curriculo = new Curriculo();
                $form->setInputFilter($curriculo->getInputFilter());

                $nonFile = $request->getPost()->toArray();
                $File = $this->params()->fromFiles('arquivo');
                $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
                );


                //set data post and file ...    
                $form->setData($data);

                if ($form->isValid()) {


                    //HEY, FORM IS VALID...
                    $size = new Size(array('min' => 200000)); //minimum bytes filesize

                    $adapter = new \Zend\File\Transfer\Adapter\Http();
                    //validator can be more than one...
                    //$adapter->setValidators(array($size), $File['name']);

                    if (!$adapter->isValid()) {
                        $dataError = $adapter->getMessages();
                        $error = array();
                        foreach ($dataError as $key => $row) {
                            $error[] = $row;
                        } //set formElementErrors
                        $form->setMessages(array('arquivo' => $error));
                    } else {


                        $adapter->setDestination('./public_html/data/curriculo');
                        $destination = './public_html/data/curriculo';

                        $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                        $newName = md5(rand() . $File['name']) . '.' . $ext;

                        $adapter->addFilter('File\Rename', array(
                            'target' => $destination . '/' . $newName,
                        ));

                        $data = $form->getData();

                        unset($data['submit']);
                        //$data['post_date'] = date('Y-m-d H:i:s');
                        $data['arquivo'] = $newName;
                        $data['cliente_id'] = $cliente_session->cliente['id'];
                        //$data['produto_id'] = $proid;
                        $curriculo->setData($data);

                        $saved = $this->getTable('Admin\Model\Curriculo')->save($curriculo);

                        if ($adapter->receive($File['name'])) {

                            $file = $adapter->getFilter('File\Rename')->getFile();
                            $target = $file[0]['target'];
                            var_dump($target);

                            $curriculo->exchangeArray($form->getData());

                            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/curriculo');
                            //echo 'Profile Name ' . $curriculo->nome . ' upload ' . $curriculo->file;
                        }
                    }
                }
            }

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id > 0) {
                $curriculo = $this->getTable('Admin\Model\Curriculo')->get($id);
                $form->bind($curriculo);
                $form->get('submit')->setAttribute('value', 'Salvar');
            }

//            $view = new ViewModel(array(
//                'form' => $form
//            ));
//            return $view;

            $view = new ViewModel(array(
                'form' => $form,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function minhasvagasestagioAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $Locacao = $this->getTable('Admin\Model\Locacao');
            $sql = $Locacao->getSql();
            $select = $sql->select()
                    ->where(array("locacoes.cliente_id=" . $cliente_session->cliente['id'] . " "));

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

            $view = new ViewModel(array(
                'locacoes' => $paginator,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function novavagaestagioAction() {

        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $form = new LocacaoForm();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $Locacao = new Locacao;
                $form->setInputFilter($Locacao->getInputFilter());

                $nonFile = $request->getPost()->toArray();
                $File = $this->params()->fromFiles('imagem');
                $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
                );

                $form->setData($data);

                if ($form->isValid()) {

                    //HEY, FORM IS VALID...
                    $size = new Size(array('min' => 200000)); //minimum bytes filesize

                    $adapter = new \Zend\File\Transfer\Adapter\Http();
                    //validator can be more than one...
                    //$adapter->setValidators(array($size), $File['name']);

                    if (!$adapter->isValid()) {

                        $dataError = $adapter->getMessages();
                        $error = array();
                        foreach ($dataError as $key => $row) {
                            $error[] = $row;
                        } //set formElementErrors
                        $form->setMessages(array('file' => $error));
                    } else {
                        $data = $form->getData();
                        $data['imagem'] = $File['name'];
                        $data['cliente_id'] = $cliente_session->cliente['id'];
                        $data['ativo'] = '2';
                        unset($data['submit']);
                        //$data['post_date'] = date('Y-m-d H:i:s');
                        $Locacao->setData($data);

                        $saved = $this->getTable('Admin\Model\Locacao')->save($Locacao);
                        $adapter->setDestination('./public_html/data/locacoes');
                        if ($adapter->receive($File['name'])) {
                            $Locacao->exchangeArray($form->getData());

                            //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                        }
                    }


//                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/locacao');
                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhasvagasestagio');
                }
            }
            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id > 0) {
                $Locacao = $this->getTable('Admin\Model\Locacao')->get($id);
                $form->bind($Locacao);
                $form->get('submit')->setAttribute('value', 'Salvar');
            }

            $view = new ViewModel(array(
                'form' => $form,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function excluirvagaestagioAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id == 0) {
                throw new \Exception("Código obrigatório");
            }

            $Locacao = $this->getTable('Admin\Model\Locacao')->get($id);

            // unlink('./public_html/data/portifolios/'.$Portifolio->imagem);

            $this->getTable('Admin\Model\Locacao')->delete($id);

            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhasvagasestagio');
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function minhasvagasempregoAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $Vagas = $this->getTable('Admin\Model\Vagas');
            $sql = $Vagas->getSql();
            $select = $sql->select()
                    ->where(array("vagas.cliente_id=" . $cliente_session->cliente['id'] . " "));

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

            $view = new ViewModel(array(
                'portifolios' => $paginator,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function novavagaempregoAction() {

        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $cat = $this->getTable('Admin\Model\Catvagas')->fetchAll()->toArray();

            $form = new VagasForm($cat);
            $request = $this->getRequest();
            if ($request->isPost()) {
                $Vagas = new Vagas;
                $form->setInputFilter($Vagas->getInputFilter());

                $nonFile = $request->getPost()->toArray();
                $File = $this->params()->fromFiles('imagem');
                $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
                );

                $form->setData($data);

                if ($form->isValid()) {

                    //HEY, FORM IS VALID...
                    $size = new Size(array('min' => 200000)); //minimum bytes filesize

                    $adapter = new \Zend\File\Transfer\Adapter\Http();
                    //validator can be more than one...
                    //$adapter->setValidators(array($size), $File['name']);

                    if (!$adapter->isValid()) {
                        $data = $form->getData();
                        $data['imagem'] = $File['name'];
                        $data['cliente_id'] = $cliente_session->cliente['id'];
                        $data['ativo'] = "2";
                        unset($data['submit']);
                        //$data['post_date'] = date('Y-m-d H:i:s');
                        $Vagas->setData($data);


                        $saved = $this->getTable('Admin\Model\Vagas')->save($Vagas);
                        $adapter->setDestination('./public_html/data/vagas');
                        if ($adapter->receive($File['name'])) {
                            $Vagas->exchangeArray($form->getData());

                            //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                        }
                    } else {
                        $data = $form->getData();
                        $data['imagem'] = $File['name'];
                        $data['cliente_id'] = $cliente_session->cliente['id'];
                        $data['ativo'] = "2";
                        unset($data['submit']);
                        //$data['post_date'] = date('Y-m-d H:i:s');
                        $Vagas->setData($data);


                        $saved = $this->getTable('Admin\Model\Vagas')->save($Vagas);
                        $adapter->setDestination('./public_html/data/vagas');
                        if ($adapter->receive($File['name'])) {
                            $Vagas->exchangeArray($form->getData());

                            //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                        }
                    }

                    $cliente_session = new Container('msg');
                    $cliente_session->msg = 'Adicionado com sucesso!';

                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhasvagasemprego');
                }
            }
            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id > 0) {
                $Vagas = $this->getTable('Admin\Model\Vagas')->get($id);
                $form->bind($Vagas);
                $form->get('submit')->setAttribute('value', 'Salvar');
            }

            $view = new ViewModel(array(
                'form' => $form,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function vagasempregoexcluirAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id == 0) {
                throw new \Exception("Código obrigatório");
            }

            $Vagas = $this->getTable('Admin\Model\Vagas')->get($id);

            // unlink('./public_html/data/portifolios/'.$Portifolio->imagem);

            $this->getTable('Admin\Model\Vagas')->delete($id);

            $cliente_session = new Container('msg');
            $cliente_session->msg = 'Excluido com sucesso!';

            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/minhasvagasemprego');
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function fotosmeusformandosAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $pagid = (int) $this->params()->fromRoute('pagid', 0);
            if ($pagid > 0) {
                $pagfoto = $this->getTable('Admin\Model\Pagfoto');
                $sql = $pagfoto->getSql();
                $select = $sql->select()
                        ->where('pagina_id = ' . $pagid);

                $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
                $paginator = new Paginator($paginatorAdapter);
                $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

                $view = new ViewModel(array(
                    'pagfotos' => $paginator,
                    'pagid' => $pagid,
                    'configuracoes' => $configuracoes
                ));
                return $view;
            }
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function fotosmeusformandossaveAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $pagid = (int) $this->params()->fromRoute('pagid', 0);
            $form = new PagfotoForm($pagid);
            $request = $this->getRequest();
            if ($request->isPost()) {

                $pagfoto = new Pagfoto();
                $form->setInputFilter($pagfoto->getInputFilter());

                $nonFile = $request->getPost()->toArray();
                $File = $this->params()->fromFiles('imagem');
                $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
                );


                //set data post and file ...    
                $form->setData($data);

                if ($form->isValid()) {


                    //HEY, FORM IS VALID...
                    $size = new Size(array('min' => 200000)); //minimum bytes filesize

                    $adapter = new \Zend\File\Transfer\Adapter\Http();
                    //validator can be more than one...
                    //$adapter->setValidators(array($size), $File['name']);

                    if (!$adapter->isValid()) {
                        $dataError = $adapter->getMessages();
                        $error = array();
                        foreach ($dataError as $key => $row) {
                            $error[] = $row;
                        } //set formElementErrors
                        $form->setMessages(array('imagem' => $error));
                    } else {
                        $data = $form->getData();
                        unset($data['submit']);
                        //$data['post_date'] = date('Y-m-d H:i:s');
                        $data['imagem'] = $File['name'];
                        //$data['pagina_id'] = $pagid;
                        $pagfoto->setData($data);

                        $saved = $this->getTable('Admin\Model\Pagfoto')->save($pagfoto);

                        $adapter->setDestination('./public_html/data/paginas');
                        if ($adapter->receive($File['name'])) {
                            $pagfoto->exchangeArray($form->getData());


                            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/fotosmeusformandos/pagid/' . $data['pagina_id']);
                            //echo 'Profile Name ' . $pagfoto->nome . ' upload ' . $pagfoto->file;
                        }
                    }
                }
            }

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id > 0) {
                $pagfoto = $this->getTable('Admin\Model\Pagfoto')->get($id);
                $form->bind($pagfoto);
                $form->get('submit')->setAttribute('value', 'Salvar');
            }

            $view = new ViewModel(array(
                'form' => $form,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function fotosmeusformandosexcluirAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $id = (int) $this->params()->fromRoute('id', 0);
            $pagid = (int) $this->params()->fromRoute('pagid', 0);
            if ($id == 0) {
                throw new \Exception("Código obrigatório");
            }

            $this->getTable('Admin\Model\Pagfoto')->delete($id);
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/fotosmeusformandos/pagid/' . $pagid);
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function meusformandosAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select = $sql->select()
                    ->from('paginas')
//                    ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
                    ->where(array("paginas.cliente_id=" . $cliente_session->cliente['id'] . " "));
//                    ->group(array("paginas.id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $formandos = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'formandos' => $formandos
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function excluirformandoAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id == 0) {
                throw new \Exception("Código obrigatório");
            }

            $this->getTable('Admin\Model\Pagina')->delete($id);
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/meusformandos');
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function novoformandoAction() {
        $cliente_session = new Container('cliente');
        if (isset($cliente_session->cliente)) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $form = new PaginaForm();
            $request = $this->getRequest();
            if ($request->isPost()) {
                $pagina = new Pagina;
                $form->setInputFilter($pagina->getInputFilter());
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $data = $form->getData();
                    $data['cliente_id'] = $cliente_session->cliente['id'];
                    $data['ativo'] = '2';
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $pagina->setData($data);

                    $saved = $this->getTable('Admin\Model\Pagina')->save($pagina);

                    $cliente_session = new Container('msg');
                    $cliente_session->msg = 'Formando Adicionado com sucesso!';

                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/meusformandos');
                }
            }
            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id > 0) {
                $pagina = $this->getTable('Admin\Model\Pagina')->get($id);
                $form->bind($pagina);
                $form->get('submit')->setAttribute('value', 'Salvar');
            }

            $view = new ViewModel(array(
                'form' => $form,
                'configuracoes' => $configuracoes
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function buscaAction() {

        if (isset($_POST['palavra'])) {

            $request = $this->getRequest();
            if ($request->isPost()) {

                $adapter = $this->getServiceLocator()->get('DbAdapter');
                $sql = new Sql($adapter);

                $eventos = " and agenda.descricao LIKE '%" . $_POST['palavra'] . "%'";
                $estagios = " and locacoes.texto LIKE '%" . $_POST['palavra'] . "%'";
                $servicos = " and servicos.texto LIKE '%" . $_POST['palavra'] . "%'";
                $imoveis = " and produtos.descricao LIKE '%" . $_POST['palavra'] . "%'";
                $vagas = " and vagas.texto LIKE '%" . $_POST['palavra'] . "%'";
                $formandos = " and paginas.texto LIKE '%" . $_POST['palavra'] . "%'";
                $tutoriais = " and tutoriais.texto LIKE '%" . $_POST['palavra'] . "%'";
                $noticias = " and posts.texto LIKE '%" . $_POST['palavra'] . "%'";
                $blogs = " and portifolios.texto LIKE '%" . $_POST['palavra'] . "%'";

                $select = $sql->select()
                        ->from('agenda')
                        ->join('agenfoto', 'agenfoto.agenda_id = agenda.id', array('imagem', 'legenda'))
                        ->where(array("academico=1 and agenfoto.principal=1" . $eventos . ""));
                $statement = $sql->prepareStatementForSqlObject($select);
                $evento = $statement->execute();

                $select = $sql->select()
                        ->from('agenda')
                        ->join('agenfoto', 'agenfoto.agenda_id = agenda.id', array('imagem', 'legenda'))
                        ->where(array("academico=2 and agenfoto.principal=1" . $eventos . ""));
                $statement = $sql->prepareStatementForSqlObject($select);
                $agenda = $statement->execute();

                $select = $sql->select()
                        ->from('locacoes')
                        ->where(array("locacoes.id=locacoes.id" . $estagios . ""))
                        ->group(array("locacoes.id"));
                $statement = $sql->prepareStatementForSqlObject($select);
                $estagios = $statement->execute();

                $select = $sql->select()
                        ->from('servicos')
                        ->where(array("servicos.id=servicos.id" . $servicos))
                        ->group(array("servicos.id"));
                $statement = $sql->prepareStatementForSqlObject($select);
                $servicos = $statement->execute();

                $select = $sql->select()
                        ->from('produtos')
                        ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem', 'legenda'))
                        ->where(array("profotos.principal=1 and produtos.id=produtos.id" . $imoveis))
                        ->group(array("produtos.id"));
                $statement = $sql->prepareStatementForSqlObject($select);
                $imoveis = $statement->execute();

                $select = $sql->select()
                        ->from('vagas')
                        ->where(array("vagas.id=vagas.id" . $vagas))
                        ->group(array("vagas.id"));
                $statement = $sql->prepareStatementForSqlObject($select);
                $vagas = $statement->execute();

                $select = $sql->select()
                        ->from('paginas')
                        ->join('pagfotos', 'pagfotos.pagina_id = paginas.id', array('imagem', 'legenda'))
                        ->where(array("pagfotos.principal=1" . $formandos));
                $statement = $sql->prepareStatementForSqlObject($select);
                $formandos = $statement->execute();

                $select = $sql->select()
                        ->from('tutoriais')
                        ->where(array("tutoriais.id=tutoriais.id" . $tutoriais))
                        ->group(array("tutoriais.id"));
                $statement = $sql->prepareStatementForSqlObject($select);
                $tutoriais = $statement->execute();

                $select = $sql->select()
                        ->from('posts')
                        ->order(array("id desc"))
                        ->where(array("status=1" . $noticias));
                $statement = $sql->prepareStatementForSqlObject($select);
                $noticias = $statement->execute();

                $select = $sql->select()
                        ->from('portifolios')
                        ->where(array("portifolios.id=portifolios.id" . $blogs));
                ;
                $statement = $sql->prepareStatementForSqlObject($select);
                $blogs = $statement->execute();

                $select = $sql->select()
                        ->from('configuracoes');
                $statement = $sql->prepareStatementForSqlObject($select);
                $configuracoes = $statement->execute();

                $view = new ViewModel(array(
                    'configuracoes' => $configuracoes,
                    'agenda' => $agenda,
                    'evento' => $evento,
                    'estagios' => $estagios,
                    'servicos' => $servicos,
                    'imoveis' => $imoveis,
                    'vagas' => $vagas,
                    'formandos' => $formandos,
                    'tutoriais' => $tutoriais,
                    'noticias' => $noticias,
                    'blogs' => $blogs,
                ));
                return $view;
            }
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
        }
    }

    public function produtosAction() {

//        $catid = (int) $this->params()->fromRoute('catid', 0);
        $subid = (int) $this->params()->fromRoute('cat', 0);
        if ($subid > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('produtos')
                    ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem', 'legenda'))
                    ->join('marcas', 'produtos.marca_id = marcas.id', array('file'))
                    ->where(array("produtos.subcategoria_id=$subid", 'profotos.principal=1'))
                    ->group("produtos.id");
            $statement = $sql->prepareStatementForSqlObject($select);
            $produtos = $statement->execute();

            $select = $sql->select()
                    ->from('profotos')
                    ->join('produtos', 'profotos.produto_id = produtos.id', array('pnome' => 'nome'))
                    ->where(array("produtos.subcategoria_id=$subid"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $fotos = $statement->execute();

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

//            $select = $sql->select()
//                    ->from('marcas');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_primarias = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_secundarias = $statement->execute();

            $select = $sql->select()
                    ->from('subcategorias')
                    ->order("ordem asc");
            $statement = $sql->prepareStatementForSqlObject($select);
            $subcategorias = $statement->execute();

            $select = $sql->select()
                    ->from('marcas');
            $statement = $sql->prepareStatementForSqlObject($select);
            $marcas = $statement->execute();

            $select = $sql->select()
                    ->from('subcategorias')
                    ->where(array("subcategorias.id=$subid"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $categoria_atual = $statement->execute();

//            $select = $sql->select()
//                    ->from('subcategorias')
//                    ->where(array("id=$subid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $subcategorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias')
//                    ->where(array("id=$catid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $categorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('posts')
//                    ->where(array('status=1'));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $lojas = $statement->execute();

            $view = new ViewModel(array(
//                'cat_primarias' => $cat_primarias,
//                'cat_secundarias' => $cat_secundarias,
                'cat' => $subid,
                'categorias' => $subcategorias,
                'marcas' => $marcas,
                'categoriaatual' => $categoria_atual,
                'configuracoes' => $configuracoes,
//                'categorias2' => $categorias2,
//                'subcategorias2' => $subcategorias2,
//                'lojas' => $lojas,
                'produtos' => $produtos,
                'fotos' => $fotos,
            ));
            return $view;
        } else {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('produtos')
                    ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem', 'legenda'))
                    ->where(array('profotos.principal=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produtos = $statement->execute();

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

//            $select = $sql->select()
//                    ->from('marcas');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_primarias = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_secundarias = $statement->execute();

            $select = $sql->select()
                    ->from('subcategorias')
                    ->order("ordem asc");
            $statement = $sql->prepareStatementForSqlObject($select);
            $subcategorias = $statement->execute();

//            $select = $sql->select()
//                    ->from('subcategorias')
//                    ->where(array("subcategorias.id=$subid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $subcategorias = $statement->execute();
//            $select = $sql->select()
//                    ->from('subcategorias')
//                    ->where(array("id=$subid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $subcategorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias')
//                    ->where(array("id=$catid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $categorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('posts')
//                    ->where(array('status=1'));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $lojas = $statement->execute();

            $view = new ViewModel(array(
//                'cat_primarias' => $cat_primarias,
//                'cat_secundarias' => $cat_secundarias,
                'categorias' => $subcategorias,
                'configuracoes' => $configuracoes,
//                'categorias2' => $categorias2,
//                'subcategorias2' => $subcategorias2,
//                'lojas' => $lojas,
                'produtos' => $produtos,
            ));
            return $view;
        }
    }

    public function motosAction() {

        $cat = (int) $this->params()->fromRoute('cat', 0);
        $q = $this->getRequest()->getPost('q', null);

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->columns(array('id', 'nome', 'frase', 'especificacoes', 'cilindrada'))
                ->from('motos')
                ->join('motfotos', 'motfotos.moto_id = motos.id', array('imagem'))
                ->where(array('motfotos.principal=1'))
                ->order(array("cilindrada asc"))
                ->group(array("motos.id"));


        if ($cat != 0) {
            $select = $sql->select()
                    ->columns(array('id', 'nome', 'frase', 'especificacoes', 'tipo', 'cilindrada'))
                    ->from('motos')
                    ->join('motfotos', 'motfotos.moto_id = motos.id', array('imagem'))
                    ->where(array('motfotos.principal=1', 'tipo=' . $cat))
                    ->order(array("cilindrada asc"))
                    ->group(array("motos.id"));
        }

        if ($q != '') {
            $select = $sql->select()
                    ->columns(array('id', 'nome', 'frase', 'especificacoes', 'tipo', 'cilindrada'))
                    ->from('motos')
                    ->join('motfotos', 'motfotos.moto_id = motos.id', array('imagem'))
                    ->where(array('motfotos.principal=1', "motos.nome like '%" . $q . "%'", "motos.descricao like '%" . $q . "%'"))
                    ->order(array("cilindrada asc"))
                    ->group(array("motos.id"));
        }


        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(9);

        /* Motos Nacionais = 2 */
        $select = $sql->select()
                ->columns(array('id', 'nome', 'frase', 'especificacoes', 'cilindrada'))
                ->from('motos')
//->join('motfotos', 'motfotos.moto_id = motos.id', array('imagem'))
                ->where(array('motos.tipo=2'))
                ->order(array("cilindrada asc"));

        $statement4 = $sql->prepareStatementForSqlObject($select);
        $nacionais = $statement4->execute();

        /* Motos Importadas = 1 */
        $select = $sql->select()
                ->columns(array('id', 'nome', 'frase', 'especificacoes', 'cilindrada'))
                ->from('motos')
//->join('motfotos', 'motfotos.moto_id = motos.id', array('imagem'))
                ->where(array('motos.tipo=1'))
                ->order(array("cilindrada asc"));

        $statement4 = $sql->prepareStatementForSqlObject($select);
        $importadas = $statement4->execute();

        $view = new ViewModel(array(
            'motos' => $paginator,
            'importadas' => $importadas,
            'nacionais' => $nacionais,
            'q' => $q
        ));
        return $view;
    }

    public function equipamentosAction() {

        /* Seleciona banners do site */
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('paginas.id=12'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
//                ->columns(array('id', 'titulo', 'texto'))
                ->from('posts');
//                ->where(array('paginas.id=7'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $equipa = $statement->execute();

        $view = new ViewModel(array(
            'paginas' => $paginas,
            'equipa' => $equipa,
        ));
        return $view;
    }

    public function infraestruturaAction() {

        /* Seleciona banners do site */
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
//                ->columns(array('id', 'titulo', 'texto'))
                ->from('infraestrutura');
//                ->where(array('paginas.id=12'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $sobre = $statement->execute();

        $select = $sql->select()
                // ->columns(array('id', 'titulo', 'texto'))
                ->from('pagfotos')
                ->where(array('pagfotos.pagina_id=14'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $fotos = $statement->execute();

        $select = $sql->select()
//                ->columns(array('id', 'titulo', 'texto'))
                ->from('posts');
//                ->where(array('paginas.id=7'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $equipa = $statement->execute();

        $view = new ViewModel(array(
            'sobre' => $sobre,
            'fotos' => $fotos,
            'equipa' => $equipa,
        ));
        return $view;
    }

    public function produtoAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
//        $subid = (int) $this->params()->fromRoute('subid', 0);
        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('produtos')
                    ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem', 'legenda'))
                    ->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('cnome' => 'nome'))
                    ->where(array("produtos.id=$id", 'profotos.principal=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produtos = $statement->execute();

            $select = $sql->select()
                    ->from('marcas');
            $statement = $sql->prepareStatementForSqlObject($select);
            $cat_primarias = $statement->execute();

            $select = $sql->select()
                    ->from('profotos')
                    ->where(array('profotos.principal!=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $fotos = $statement->execute();

            $select3 = $sql->select()
                    ->from('configuracoes');
            $statement3 = $sql->prepareStatementForSqlObject($select3);
            $configuracoes = $statement3->execute();

//            $select = $sql->select()
//                    ->from('categorias');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_secundarias = $statement->execute();



            $select = $sql->select()
                    ->from('subcategorias');
            $statement = $sql->prepareStatementForSqlObject($select);
            $subcategorias = $statement->execute();

//            $select = $sql->select()
//                    ->from('subcategorias')
//                    ->where(array("id=$subid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $subcategorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias')
//                    ->where(array("id=$catid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $categorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('posts')
//                    ->where(array('status=1'));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $lojas = $statement->execute();

            $view = new ViewModel(array(
                'fotos' => $fotos,
//                'cat_primarias' => $cat_primarias,
//                'cat_secundarias' => $cat_secundarias,
                'configuracoes' => $configuracoes,
                'categorias' => $subcategorias,
//                'categorias2' => $categorias2,
//                'subcategorias2' => $subcategorias2,
//                'lojas' => $lojas,
                'produtos' => $produtos,
            ));
            return $view;
        }
    }

    public function profissionalAction() {

        $id = (int) $this->params()->fromRoute('id', 0);

        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('agenda')
                    ->join('agenfoto', 'agenfoto.agenda_id = agenda.id', array('imagem', 'legenda'))
                    ->where(array("agenfoto.principal=1", "agenda.id=$id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $agendas = $statement->execute();

            $select = $sql->select()
                    ->from('agenfoto')
//                    ->join('agenfoto', 'agenfoto.agenda_id = agenda.id', array('imagem', 'legenda'))
                    ->where(array("agenfoto.principal!=1", "agenfoto.id=$id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $fotos = $statement->execute();


            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

//            $select = $sql->select()
//                    ->from('marcas');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_primarias = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_secundarias = $statement->execute();

            $select = $sql->select()
                    ->from('subcategorias')
                    ->order("ordem asc");
            $statement = $sql->prepareStatementForSqlObject($select);
            $subcategorias = $statement->execute();

            $select = $sql->select()
                    ->from('marcas');
            $statement = $sql->prepareStatementForSqlObject($select);
            $marcas = $statement->execute();



//            $select = $sql->select()
//                    ->from('subcategorias')
//                    ->where(array("id=$subid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $subcategorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias')
//                    ->where(array("id=$catid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $categorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('posts')
//                    ->where(array('status=1'));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $lojas = $statement->execute();

            $view = new ViewModel(array(
//                'cat_primarias' => $cat_primarias,
//                'cat_secundarias' => $cat_secundarias,
//            'cat' => $subid,
                'categorias' => $subcategorias,
                'marcas' => $marcas,
                'agendas' => $agendas,
                'configuracoes' => $configuracoes,
                'fotos' => $fotos,
//            'categoriaatual' => $categoria_atual,
//                'categorias2' => $categorias2,
//                'subcategorias2' => $subcategorias2,
//                'lojas' => $lojas,
//            'produtos' => $produtos,
//            'fotos' => $fotos,
            ));
            return $view;
        }
    }

    public function profissionaisAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('agenda')
                ->join('agenfoto', 'agenfoto.agenda_id = agenda.id', array('imagem', 'legenda'))
                ->where(array("agenfoto.principal=1"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $agendas = $statement->execute();


        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

//            $select = $sql->select()
//                    ->from('marcas');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_primarias = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias');
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $cat_secundarias = $statement->execute();

        $select = $sql->select()
                ->from('subcategorias')
                ->order("ordem asc");
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();

        $select = $sql->select()
                ->from('marcas');
        $statement = $sql->prepareStatementForSqlObject($select);
        $marcas = $statement->execute();



//            $select = $sql->select()
//                    ->from('subcategorias')
//                    ->where(array("id=$subid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $subcategorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('categorias')
//                    ->where(array("id=$catid"));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $categorias2 = $statement->execute();
//            $select = $sql->select()
//                    ->from('posts')
//                    ->where(array('status=1'));
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $lojas = $statement->execute();

        $view = new ViewModel(array(
//                'cat_primarias' => $cat_primarias,
//                'cat_secundarias' => $cat_secundarias,
//            'cat' => $subid,
            'categorias' => $subcategorias,
            'marcas' => $marcas,
            'agendas' => $agendas,
            'configuracoes' => $configuracoes,
//            'categoriaatual' => $categoria_atual,
//                'categorias2' => $categorias2,
//                'subcategorias2' => $subcategorias2,
//                'lojas' => $lojas,
//            'produtos' => $produtos,
//            'fotos' => $fotos,
        ));
        return $view;
    }

    public function detalheAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            $select = $sql->select()
                    ->from('produtos')
                    // ->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('snome' => 'nome'))
                    ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem', 'legenda'))
                    ->where(array("produtos.id=$id", 'profotos.principal=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produtos = $statement->execute();

            $select = $sql->select()
                    ->from('posts')
                    ->where(array('status=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $lojas = $statement->execute();

            $view = new ViewModel(array(
                'lojas' => $lojas,
                'produtos' => $produtos,
            ));

            return $view;
        }
    }

    public function simulacaoAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        $versao = (int) $this->params()->fromRoute('versao', 0);
//$cep = (int) $this->params()->fromRoute('cep', 0);


        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'nome', 'frase', 'especificacoes'))
                    ->from('motos')
                    ->where(array("motos.id=$id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produto = $statement->execute();
            $produto->buffer();
            if ($versao > 0) {
                $select = $sql->select()
                        ->columns(array('qtde', 'valor_parcela', 'valor_credito', 'plano_id'))
                        ->from('parcelas')
                        ->where(array("parcelas.moto_id=$id", "parcelas.versao_id=$versao"))
//->group(array('plano_id asc'))
                        ->order(array('qtde DESC'));
                $statement = $sql->prepareStatementForSqlObject($select);
                $parcelas = $statement->execute();
                $parcelas->buffer();

                $select = $sql->select()
                        ->columns(array('id', 'descricao'))
                        ->from('planos')
                        ->where(array("id IN (SELECT plano_id FROM parcelas WHERE moto_id =" . $id . " and versao_id =" . $versao . ")"));

                $statement = $sql->prepareStatementForSqlObject($select);
                $planos = $statement->execute();
                $planos->buffer();
            } else {
                $select = $sql->select()
                        ->columns(array('qtde', 'valor_parcela', 'valor_credito', 'plano_id'))
                        ->from('parcelas')
                        ->where(array("parcelas.moto_id=$id"))
//->group(array('plano_id asc'))
                        ->order(array('qtde DESC'));
                $statement = $sql->prepareStatementForSqlObject($select);
                $parcelas = $statement->execute();
                $parcelas->buffer();

                $select = $sql->select()
                        ->columns(array('id', 'descricao'))
                        ->from('planos')
                        ->where(array("id IN (SELECT plano_id FROM parcelas WHERE moto_id =" . $id . ")"));
                $statement = $sql->prepareStatementForSqlObject($select);
                $planos = $statement->execute();
                $planos->buffer();
            }


            $select = $sql->select()
                    ->columns(array('id', 'nome'))
                    ->from('versoes')
                    ->where(array("moto_id=" . $id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $versoes = $statement->execute();
            $versoes->buffer();

            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'imagem', 'legenda', 'moto_id'))
                    ->from('motfotos')
                    ->where(array("motfotos.moto_id=$id", 'motfotos.principal=1'))
                    ->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $fotos = $statement1->execute();
            $fotos->buffer();
//Resolve o problema do resultset foward
//$fotos->current();  //Resolve o problema do resultset


            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'imagem', 'legenda', 'moto_id'))
                    ->from('motfotos')
                    ->where(array("motfotos.moto_id=$id", 'motfotos.principal=3'))
                    ->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $fotos2 = $statement1->execute();
            $fotos2->buffer();
            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'imagem', 'legenda', 'moto_id'))
                    ->from('motfotos')
                    ->where(array("motfotos.moto_id=$id", 'motfotos.principal=2'))
                    ->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $fotos3 = $statement1->execute();
            $fotos3->buffer();


            /* Enviar Email */
            $request = $this->getRequest();

            if ($request->isPost()) {

                $select = $sql->select()
                        ->columns(array('parametro'))
                        ->from('configuracoes')
                        ->where(array('configuracoes.id=8'));
                $statement = $sql->prepareStatementForSqlObject($select);
                $email = $statement->execute();

                foreach ($email as $para) {
                    $email_destino = $para['parametro'];
                }

                $subject = $_POST['assunto'];

                $nome2 = $_POST['nome'];

                $email2 = $_POST['email'];

                $cidade = $_POST['cidade_estado'];

                $parcela = $_POST['parcela'];

                $moto = $_POST['moto'];

                $msg2 = $_POST['mensagem'];

                $msg = "NOME: $nome2 \n\nMOTO: $moto \n\nPARCELA: $parcela \n\nE-MAIL: $email2\n\nCidade/Estado: $cidade\n\n MENSAGEM: $msg2";

                $mailheaders = "From: https://www.antaresmotos.com";


                if (mail("$email_destino", "Mensagem do site - $subject", "$msg", "$mailheaders")) {
                    echo "<script>alert('Sua Mensagem foi enviada! Obrigado pela visita!');</script>";
//echo "<script>window.location = 'index';</script>";
                } else {
                    echo "<script>alert('Tente novamente mais tarde!')</script>";
//echo "<script>window.location = 'index';</script>";
                }
            }


            $view = new ViewModel(array(
                'produto' => $produto,
                'parcelas' => $parcelas,
                'planos' => $planos,
                'versoes' => $versoes,
                'fotos' => $fotos,
                'fotos2' => $fotos2,
                'fotos3' => $fotos3,
                'id' => $id,
                'versao_id' => $versao
//'fotos2' => $fotos
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl('/  ');
        }
    }

    public function motoAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
//$cep = (int) $this->params()->fromRoute('cep', 0);


        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'nome', 'frase', 'especificacoes'))
                    ->from('motos')
//->join('marcas', 'produtos.marca_id = marcas.id', array('mnome' => 'nome'))
//->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('snome' => 'nome'))
//->join('profotos', 'profotos.produto_id = produtos.id',array('imagem','legenda'))
//->where(array("produtos.id=$id",'profotos.principal=1'));
                    ->where(array("motos.id=$id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produto = $statement->execute();
            $produto->buffer();
//$produto->next();

            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'imagem', 'legenda', 'moto_id', 'principal'))
                    ->from('motfotos')
                    ->where(array("motfotos.moto_id=$id", 'motfotos.principal=1'))
                    ->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $fotos = $statement1->execute();
            $fotos->buffer(); //Resolve o problema do resultset foward
//$fotos->current();  //Resolve o problema do resultset

            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'imagem', 'legenda', 'moto_id', 'principal'))
                    ->from('motfotos')
                    ->where(array("motfotos.moto_id=$id", "motfotos.principal=3"))
                    ->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $fotos2 = $statement1->execute();
            $fotos->buffer();

            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'imagem', 'legenda', 'moto_id', 'principal'))
                    ->from('motfotos')
                    ->where(array("motfotos.moto_id=$id", 'motfotos.principal=2'))
                    ->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $fotos3 = $statement1->execute();
            $fotos3->buffer();

            $view = new ViewModel(array(
                'produto' => $produto,
                'fotos2' => $fotos2,
                'fotos3' => $fotos3,
//'frete' => $frete_valor,
                'fotos' => $fotos
//'fotos2' => $fotos
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl('/home');
        }
    }

    public function unidadeAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
//$cep = (int) $this->params()->fromRoute('cep', 0);


        if ($id > 0) {
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->from('lojas')
//->join('marcas', 'produtos.marca_id = marcas.id', array('mnome' => 'nome'))
//->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('snome' => 'nome'))
//->join('profotos', 'profotos.produto_id = produtos.id',array('imagem','legenda'))
//->where(array("produtos.id=$id",'profotos.principal=1'));
                    ->where(array("lojas.id=$id"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produto = $statement->execute();
            $produto->buffer();
//$produto->next();

            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'imagem', 'legenda', 'loja_id'))
                    ->from('lojfotos')
                    ->where(array("lojfotos.loja_id=$id"))
                    ->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $fotos = $statement1->execute();
            $fotos->buffer(); //Resolve o problema do resultset foward
//$fotos->current();  //Resolve o problema do resultset




            $view = new ViewModel(array(
                'produto' => $produto,
//'frete' => $frete_valor,
                'fotos' => $fotos
//'fotos2' => $fotos
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl('/home');
        }
    }

//Função para adicionar, alterar,  excluir e listar produtos do carrinho 
    public function carrinhoAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('paginas.id=23'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $select = $sql->select()
                ->from('subcategorias')
// ->where(array('subcategorias.id='.$subcat))
                ->order(array("id asc"))
                ->limit('1');
        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias3 = $statement->execute();

//seleciona os telefone e email
        $select = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $request = $this->getRequest();
        if ($request->isPost()) {

            $adapter1 = $this->getServiceLocator()->get('DbAdapter');
            $sql1 = new Sql($adapter1);

            $sql1 = "INSERT INTO newsletter (nome,email) VALUES ('" . $_POST['nome_news'] . "','" . $_POST['email_news'] . "');";
            $statement = $adapter1->query($sql1);
            $inserir = $statement->execute();
        }

        $select = $sql->select()
                ->columns(array('id', 'nome'))
                ->from('marcas');
// ->join('produtos', 'produtos.marca_id = marcas.id')
// ->where(array('marcas.id=' . $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $marcas3 = $statement->execute();



        $select = $sql->select()
                ->columns(array('id', 'nome', 'marca_id'))
                ->from('produtos');
// ->join('produtos', 'produtos.marca_id = marcas.id')
// ->where(array('marcas.id=' . $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $produto = $statement->execute();

        $carrinho_session = new Container('carrinho');
        $carrinhoprodutos = new Container('carrinhoprodutos');
//$carrinhofrete = new Container('carrinhofrete');

        $id = (int) $this->params()->fromRoute('id', 0);
        $acao = (int) $this->params()->fromRoute('acao', 0);

        $cep = $this->params()->fromQuery('cep', 0);
// $tipo = $this->params()->fromQuery('tipo', 0);

        if ($cep == 0) {
            if ($id > 0 && $acao > 0) {
//Adicionar produto ao carrinho
                if ($acao == 1) {
                    if (!isset($carrinho_session->carrinho[$id])) {
                        $carrinho_session->carrinho[$id] = 1;
                    } else {
                        $carrinho_session->carrinho[$id] += 1;
                    }
                }

//REMOVER CARRINHO
                if ($acao == 2) {
                    if (isset($carrinho_session->carrinho[$id])) {
                        unset($carrinho_session->carrinho[$id]);
                    }
                }
            }
        }
//ALTERAR QUANTIDADE
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dados = $request->getPost();
            if ($dados['acao'] == '3') {
                if (is_array($carrinho_session->carrinho)) {
                    foreach ($carrinho_session->carrinho as $id => $qtd) {
                        if ($id == $dados['id']) {
                            if (!empty($dados['qtde']) || $dados['qtde'] <> 0) {
                                $carrinho_session->carrinho[$id] = (int) $dados['qtde'];
                            } else {
                                unset($carrinho_session->carrinho[$id]);
                            }
                        }
                    }
                }
            }
        }


        $pesototal = 0;

//pegar informações dos produtos que estão no carrinho no banco de dados
        $pro = array();
        foreach ($carrinho_session->carrinho as $id => $qtd) {


            $select = $sql->select()
                    ->columns(array('id', 'nome', 'preco', 'preco_promocao', 'promocao', 'peso'))
                    ->from('produtos')
// ->join('tipos', 'produtos.tipo = tipos.id', array('tipo' => 'nome'))
// ->join('pais', 'produtos.pais = pais.id', array('pais' => 'nome'))
                    ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem'))
                    ->where(array("produtos.id=$id", 'profotos.principal=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
//
            foreach ($result as $product) {
//$pro[1]['codigo'] = $product['id'];
                $pro[$id]['nome'] = $product['nome'];
                $pro[$id]['preco'] = $product['preco'];
                $pro[$id]['preco_promocao'] = $product['preco_promocao'];
                $pro[$id]['promocao'] = $product['promocao'];
//$pro[1]['quantidade'] = 1;
// $pro[$id]['marca'] = $product['mnome'];
                $pro[$id]['imagem'] = $product['imagem'];
                $pesototal += ($product['peso'] * $qtd);
            }
        }
        $carrinhoprodutos->carrinhoprodutos = $pro;
//$carrinhoprodutos->carrinhoprodutos = 10;

        /* Seleciona as fotos do produto */
        $select1 = $sql->select()
                ->columns(array('id', 'parametro'))
                ->from('configuracoes')
                ->where(array("id=4"));
//->order(array('principal ASC', 'ordem ASC'));
        $statement1 = $sql->prepareStatementForSqlObject($select1);
        $ceporigem = $statement1->execute();
        $ceporigem->buffer();

        $ceploja = '';

        foreach ($ceporigem as $cepo) {
            $ceploja = $cepo['parametro'];
        }



        $frete = "";

        if ($cep > 0 && $tipo > 0) {
//$frete = calculafrete('40010','93800000','90200210 ','0.5');

            $frete = $this->Frete()->calculafrete($tipo, $ceploja, $cep, $pesototal);
//$carrinhofrete->carrinhofrete = $frete;
        }

        $view = new ViewModel(array(
            'produtos' => $carrinho_session->carrinho,
            'produtos2' => $pro,
            'marcas3' => $marcas3,
            'configuracoes' => $configuracoes,
            'produto' => $produto,
// 'frete' => $frete,
// 'cep' => $cep,
// 'tipo' => $tipo,
            'subcategorias3' => $subcategorias3,
            'paginas' => $paginas,
        ));
        return $view;
    }

//Função para adicionar, alterar,  excluir e listar produtos do carrinho 
    public function passo1Action() {
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $select = $sql->select()
                ->from('paginas')
                ->where(array("id=16"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $paginas = $statement->execute();

        $view = new ViewModel(array(
            'paginas' => $paginas,
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function pesquisaAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $q = "";
        $request = $this->getRequest();
        if ($request->isPost()) {
            $q = " and nome LIKE '%" . $_POST['palavra'] . "%'";
            $select = $sql->select()
                    ->from('produtos')
                    ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem'))
                    ->where(array('profotos.principal=1' . $q));
            $statement = $sql->prepareStatementForSqlObject($select);
            $produtos = $statement->execute();

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
                'produtos' => $produtos,
            ));
            return $view;
        } else {
            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'configuracoes' => $configuracoes,
            ));
            return $view;
        }
    }

//    public function orcamentoAction() {
//
//        $adapter = $this->getServiceLocator()->get('DbAdapter');
//        $sql = new Sql($adapter);
//
//        //seleciona os telefone e email
//        $select = $sql->select()
//                ->columns(array('id', 'parametro'))
//                ->from('configuracoes');
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $configuracoes = $statement->execute();
//
//        //seleciona contato
//        $select = $sql->select()
//                ->columns(array('id', 'titulo', 'texto'))
//                ->from('paginas');
//
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $paginas = $statement->execute();
//
//        $select = $sql->select()
//                ->columns(array('parametro'))
//                ->from('configuracoes')
//                ->where(array('configuracoes.id=1'));
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $email = $statement->execute();
//
//        foreach ($email as $para) {
//            $email_destino = $para['parametro'];
//        }
//
//        /* Enviar Email */
//        $request = $this->getRequest();
//
//        if ($request->isPost()) {
//
//            $subject = 'Solicitação de Projeto pelo site trajetomoveis.com.br';
//
//            $nome = $_POST['nome'];
//
//
//            $tipo_projeto = $_POST['tipo_projeto'];
//
//            $telefone = $_POST['telefone'];
//
//            $email = $_POST['email'];
//
//            // $cidade = $_POST['cidade_estado'];
//
//            $msg2 = $_POST['comentarios'];
//
//            $msg = "Nome: $nome2 \n\nE-MAIL: $email\n\nTelefone: $telefone\n\nTipo de Projeto: $tipo_projeto\n\n Cometarios: $msg2";
//
//            $mailheaders = "From: https://www.trajetomoveis.com";
//
//
//            if (mail("$email_destino", "$subject", "$msg", "$mailheaders")) {
//                echo "<script>alert('Sua Mensagem foi enviada! Obrigado pela visita!');</script>";
//                echo "<script>window.location = 'index';</script>";
//            } else {
//                echo "<script>alert('Tente novamente mais tarde!')</script>";
//                echo "<script>window.location = 'index';</script>";
//            }
//
//            $view = new ViewModel(array(
////                'qtde_pro' => $qtde_pro,
////                'nome' => $nome,
////                'nome_pro' => $nome_pro,
//            ));
//            return $view;
//        }
//
//        $view = new ViewModel(array(
////            'links' => $links,
//            'configuracoes' => $configuracoes,
//            'paginas' => $paginas,
//        ));
//        return $view;
//    }

    public function seguroAction() {

        /* Seleciona banners do site */
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('paginas.id=1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $empresa = $statement->execute();

        $select = $sql->select()
                ->columns(array('id', 'nome'))
                ->from('motos');
        $statement = $sql->prepareStatementForSqlObject($select);
        $motos = $statement->execute();

        $view = new ViewModel(array(
            'empresa' => $empresa,
            'motos' => $motos
        ));
        return $view;
    }

    public function financiamentoAction() {

        /* Seleciona banners do site */
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('paginas.id=5'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $empresa = $statement->execute();

        $select = $sql->select()
                ->columns(array('id', 'nome'))
                ->from('motos');
        $statement = $sql->prepareStatementForSqlObject($select);
        $motos = $statement->execute();

        /* Enviar Email */
        $request = $this->getRequest();

        if ($request->isPost()) {

            $select = $sql->select()
                    ->columns(array('parametro'))
                    ->from('configuracoes')
                    ->where(array('configuracoes.id=7'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $email = $statement->execute();

            foreach ($email as $para) {
                $email_destino = $para['parametro'];
            }

            $subject = "Simulação Financiamento";

            $nome2 = $_POST['nome'];

            $email2 = $_POST['email'];

            $telefone = $_POST['telefone'];

            $cidade = $_POST['cidade_estado'];

            $parcela = $_POST['parcela'];

            $moto = $_POST['modelo'];

            $entrada = $_POST['entrada'];

            $msg2 = $_POST['observacoes'];

            $msg = "NOME: $nome2 \n\n                
Telefone: $telefone \n\n 
E-MAIL: $email2\n\n 
Cidade/Estado: $cidade\n\n
MODELO:$moto\n\n 
Entrada:$entrada\n\n
Parcela:$parcela\n\n
OBSERVAÇÕES: $msg2";

            $mailheaders = "From: https://www.antaresmotos.com";


            if (mail("$email_destino", "Mensagem do site - $subject", "$msg", "$mailheaders")) {
                echo "<script>alert('Sua Mensagem foi enviada! Obrigado pela visita!');</script>";
//echo "<script>window.location = 'index';</script>";
            } else {
                echo "<script>alert('Tente novamente mais tarde!')</script>";
//echo "<script>window.location = 'index';</script>";
            }
        }

        $view = new ViewModel(array(
            'empresa' => $empresa,
            'motos' => $motos
        ));
        return $view;
    }

    public function oficinaAction() {
        /* Seleciona banners do site */
        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->columns(array('id', 'titulo', 'texto'))
                ->from('paginas')
                ->where(array('paginas.id=4'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $empresa = $statement->execute();

//        $adapter = $this->getServiceLocator()->get('DbAdapter');
//        $sql = new Sql($adapter);
        $select = $sql->select()
                ->columns(array('id', 'pagina_id', 'imagem'))
                ->from('pagfotos')
                ->where(array('pagina_id=4'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $fotos = $statement->execute();

        $view = new ViewModel(array(
            'empresa' => $empresa,
            'fotos' => $fotos
        ));
        return $view;
    }

    public function unidadesAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select2 = $sql->select()
//                ->columns(array('id', 'nome'))
                ->from('lojas');
//                ->join('lojfotos', 'lojfotos.loja_id = lojas.id', array('imagem'));
//                ->where(array('lojfotos.principal=1','lojfotos.loja_id = lojas.id'));
        $statement2 = $sql->prepareStatementForSqlObject($select2);
        $lojas = $statement2->execute();

        $view = new ViewModel(array(
            'lojas' => $lojas
        ));
        return $view;
    }

    private function frete($servico, $CEPorigem, $CEPdestino, $peso, $altura = '4', $largura = '12', $comprimento = '16', $valor = '1.00') {
////////////////////////////////////////////////
// Código dos Serviços dos Correios
// 41106 PAC
// 40010 SEDEX
// 40045 SEDEX a Cobrar
// 40215 SEDEX 10
////////////////////////////////////////////////
// URL do WebService
        $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=" . $CEPorigem . "&sCepDestino=" . $CEPdestino . "&nVlPeso=" . $peso . "&nCdFormato=1&nVlComprimento=" . $comprimento . "&nVlAltura=" . $altura . "&nVlLargura=" . $largura . "&sCdMaoPropria=n&nVlValorDeclarado=" . $valor . "&sCdAvisoRecebimento=n&nCdServico=" . $servico . "&nVlDiametro=0&StrRetorno=xml";
// Carrega o XML de Retorno
        $xml = simplexml_load_file($correios);
// Verifica se não há erros
        if ($xml->cServico->Erro == '0') {
            return $xml->cServico->Valor;
        } else {
            return false;
        }
    }

    public function cadastrarcontaAction() {
        $form = new ClienteForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cliente = new Cliente;
            $form->setInputFilter($cliente->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                unset($data['confirmasenha']);
                $data['data'] = date('Y-m-d H:i:s');
                $cliente->setData($data);

                $saved = $this->getTable('Admin\Model\Cliente')->save($cliente);
                $cliente_session = new Container('cliente');
                $cliente_session->cliente = $data;

//Pega os dados postados pelo formulário HTML e os coloca em variaveis
                if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
//substitua na linha acima a aprte locaweb.com.br por seu domínio.
                    $email_from = 'cadastro@' . $_SERVER[HTTP_HOST]; // Substitua essa linha pelo seu e-mail@seudominio
                } else {
                    $email_from = "cadastro@" . $_SERVER[HTTP_HOST];
//    Na linha acima estamos forçando que o remetente seja 'webmaster@',
// você pode alterar para que o remetente seja, por exemplo, 'contato@'.
                }


                if (PATH_SEPARATOR == ';') {
                    $quebra_linha = "\r\n";
                } elseif (PATH_SEPARATOR == ':') {
                    $quebra_linha = "\n";
                } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                    echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
                }

//pego os dados enviados pelo formulário 
                $nome_para = $data['nome'];
                $email_de = $data['email'];
                $email = $data['email'];
//            $email = "yuri@agenciaw3.com.br,yurimoreira2309@gmail.com";
                $assunto = "Cadastro - LA CASA";
                // $mensagem = $_POST["msg"];
                //formato o campo da mensagem 
                $mensagem_cont = "
			<p style='text-align:justify;font-size:16px;line-height:22px'>CARO(A) <b style='text-transform: uppercase'>" . $nome_para . ",</b><br>
			1. O seu cadastro na loja do site <b><a target='blank' style='text-decoration:none;' href='http://lacasaloja.com.br'>www.lacasaloja.com.br</a></b> foi efetuado com sucesso!<br><br>
			<b>Agora você poderá realizar compras no site e visualizar a situação da sua conta!<b>
			</p>";

                $arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

                if (file_exists($arquivo["tmp_name"]) and ! empty($arquivo)) {

                    $fp = fopen($_FILES["arquivo"]["tmp_name"], "rb");
                    $anexo = fread($fp, filesize($_FILES["arquivo"]["tmp_name"]));
                    $anexo = base64_encode($anexo);

                    fclose($fp);

                    $anexo = chunk_split($anexo);


                    $boundary = "XYZ-" . date("dmYis") . "-ZYX";

                    $mens = "--$boundary" . $quebra_linha . "";
                    $mens .= "Content-Transfer-Encoding: 8bits" . $quebra_linha . "";
                    $mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $quebra_linha . "" . $quebra_linha . ""; //plain 
                    $mens .= "$mensagem_cont" . $quebra_linha . "";
                    $mens .= "--$boundary" . $quebra_linha . "";
                    $mens .= "Content-Type: " . $arquivo["type"] . "" . $quebra_linha . "";
                    $mens .= "Content-Disposition: attachment; filename=\"" . $arquivo["name"] . "\"" . $quebra_linha . "";
                    $mens .= "Content-Transfer-Encoding: base64" . $quebra_linha . "" . $quebra_linha . "";
                    $mens .= "$anexo" . $quebra_linha . "";
                    $mens .= "--$boundary--" . $quebra_linha . "";

                    $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                    $headers .= "From: $email_from " . $quebra_linha . "";
                    $headers .= "Return-Path: $email_from " . $quebra_linha . "";
                    $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"" . $quebra_linha . "";
                    $headers .= "$boundary" . $quebra_linha . "";


//envio o email com o anexo 
                    mail($email, $assunto, $mens, $headers, "-r" . $email_from);

                    echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
                }

//se nao tiver anexo 
                else {

                    $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                    $headers .= "Content-type: text/html; charset=iso-8859-1" . $quebra_linha . "";
                    $headers .= "From: $email_from " . $quebra_linha . "";
                    $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
                    mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);


                    echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
                }

                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/cadastro');
            }
        }

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $view = new ViewModel(array(
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function cadastrarAction() {
        $form = new ClienteForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cliente = new Cliente;
            $form->setInputFilter($cliente->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                unset($data['confirmasenha']);
                $data['data'] = date('Y-m-d H:i:s');
                $cliente->setData($data);

                $saved = $this->getTable('Admin\Model\Cliente')->save($cliente);
                $cliente_session = new Container('cliente');
                $cliente_session->cliente = $data;

//Pega os dados postados pelo formulário HTML e os coloca em variaveis
//                if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
////substitua na linha acima a aprte locaweb.com.br por seu domínio.
//                    $email_from = 'cadastro@' . $_SERVER[HTTP_HOST]; // Substitua essa linha pelo seu e-mail@seudominio
//                } else {
//                $email_from = "cadastro@univercity.com.br";
//    Na linha acima estamos forçando que o remetente seja 'webmaster@',
// você pode alterar para que o remetente seja, por exemplo, 'contato@'.
//                }
//                if (PATH_SEPARATOR == ';') {
//                    $quebra_linha = "\r\n";
//                } elseif (PATH_SEPARATOR == ':') {
//                    $quebra_linha = "\n";
//                } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
//                    echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
//                }
//pego os dados enviados pelo formulário 
//                $nome_para = $data['nome'];
//                $email_de = $data['email'];
//                $email = $data['email'];
////            $email = "yuri@agenciaw3.com.br,yurimoreira2309@gmail.com";
//                $assunto = "Cadastro - LA CASA";
                // $mensagem = $_POST["msg"];
                //formato o campo da mensagem 
//                $mensagem_cont = "
//			<p style='text-align:justify;font-size:16px;line-height:22px'>CARO(A) <b style='text-transform: uppercase'>" . $nome_para . ",</b><br>
//			1. O seu cadastro na loja do site <b><a target='blank' style='text-decoration:none;' href='http://lacasaloja.com.br'>www.lacasaloja.com.br</a></b> foi efetuado com sucesso!<br><br>
//			<b>Agora você poderá realizar compras no site e visualizar a situação da sua conta!<b>
//			</p>";
//                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
//                $headers .= "Content-type: text/html; charset=iso-8859-1" . $quebra_linha . "";
//                $headers .= "From: $email_from " . $quebra_linha . "";
//                $headers .= "Return-Path: $email_from " . $quebra_linha . "";
//envia o email sem anexo 
//                mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);
//                    echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
            }
        }

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $select = $sql->select()
                ->from('configuracoes');
        $statement = $sql->prepareStatementForSqlObject($select);
        $configuracoes = $statement->execute();

        $view = new ViewModel(array(
            'configuracoes' => $configuracoes,
        ));
        return $view;
    }

    public function passo2Action() {

        $carrinho_session = new Container('carrinho');
        $carrinhoprodutos = new Container('carrinhoprodutos');
        $cliente_session = new Container('cliente');
        $casal_session = new Container('casal');
        $frete_session = new Container('frete');
        $preco_session = new Container('preco');
        $tipofrete_session = new Container('tipofrete');
        $codigoloja = new Container('codigoloja');

        if (isset($cliente_session->cliente)) {

            $pesototal = 0;
            $precototal = 0;

            //pegar informações dos produtos que estão no carinho no banco de dados
            $pro = array();
            foreach ($carrinho_session->carrinho as $id => $qtd) {
                $adapter = $this->getServiceLocator()->get('DbAdapter');
                $sql = new Sql($adapter);
                $select = $sql->select()
                        ->columns(array('id', 'nome', 'preco', 'preco_promocao', 'promocao', 'peso', 'codigoloja', 'altura', 'comprimento', 'largura'))
                        ->from('produtos')
//                        ->join('marcas', 'produtos.marca_id = marcas.id', array('mnome' => 'nome'))
//                        ->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('snome' => 'nome'))
                        ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem'))
                        ->where(array("produtos.id=$id", 'profotos.principal=1'));
                $statement = $sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();
                //
                foreach ($result as $product) {
                    //$pro[1]['codigo'] = $product['id'];
                    $pro[$id]['nome'] = $product['nome'];
                    $pro[$id]['preco'] = $product['preco'];
                    $pro[$id]['preco_promocao'] = $product['preco_promocao'];
                    $pro[$id]['promocao'] = $product['promocao'];
                    $pro[$id]['codigoloja'] = $product['codigoloja'];
                    //$pro[1]['quantidade'] = 1;
//                  $pro[$id]['marca'] = $product['mnome'];
                    $pro[$id]['imagem'] = $product['imagem'];
                    $pesototal += ($product['peso'] * $qtd);
                    $pro[$id]['altura'] = $product['altura'];
                    $pro[$id]['largura'] = $product['largura'];
                    $pro[$id]['comprimento'] = $product['comprimento'];
                    $precototal += ($product['preco'] * $qtd);
                }
            }
            $carrinhoprodutos->carrinhoprodutos = $pro;

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);

            /* Seleciona as fotos do produto */
            $select1 = $sql->select()
                    ->columns(array('id', 'parametro'))
                    ->from('configuracoes')
                    ->where(array("id=16"));
            //->order(array('principal ASC', 'ordem ASC'));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $ceporigem = $statement1->execute();
            $ceporigem->buffer();

            $ceploja = '';

            foreach ($ceporigem as $cepo) {
                $ceploja = $cepo['parametro'];
            }


            //ALTERAR QUANTIDADE
            $request = $this->getRequest();
            if ($request->isPost()) {
                $dados = $request->getPost();
                if ($dados['acao'] == '3') {
                    if (is_array($carrinho_session->carrinho)) {
                        foreach ($carrinho_session->carrinho as $id => $qtd) {
                            if ($id == $dados['id']) {
                                if (!empty($dados['qtde']) || $dados['qtde'] <> 0) {
                                    $carrinho_session->carrinho[$id] = (int) $dados['qtde'];
                                } else {
                                    unset($carrinho_session->carrinho[$id]);
                                }
                            }
                        }
                    }
                }
            }

            $select2 = $sql->select()
                    ->columns(array('id', 'cep'))
                    ->from('clientes')
                    ->where(array("id='" . $cliente_session->cliente['id'] . "'"));
            //->order(array('principal ASC', 'ordem ASC'));
            $statement2 = $sql->prepareStatementForSqlObject($select2);
            $cliente = $statement2->execute();
            //$cepcliente->buffer();
//            $idclienteteste = $cliente_session->cliente['id'];
            //$idclienteteste = $cepcliente->cep;
            $cepcliente = '';

            foreach ($cliente as $cepc) {
                $cepcliente = $cepc['cep'];
            }

//            $cep = $this->params()->fromQuery('cep', 0);
            // $tipo = $this->params()->fromQuery('tipo', 0);
            // $tipo="";
            // if(isset($_GET['tipo'])){
            // $tipo = $_GET['tipo'] ;
            // }
            $frete = "";
            $frete3 = "";

            function calcula_frete($servico, $ceploja, $cepcliente, $peso, $altura, $largura, $comprimento, $valor = '1.00') {
                ////////////////////////////////////////////////
                // Código dos Serviços dos Correios
                // 41106 PAC
                // 40010 SEDEX
                // 40045 SEDEX a Cobrar
                // 40215 SEDEX 10
                ////////////////////////////////////////////////
                // URL do WebService
                $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=" . $ceploja . "&sCepDestino=" . $cepcliente . "&nVlPeso=" . $peso . "&nCdFormato=1&nVlComprimento=" . $comprimento . "&nVlAltura=" . $altura . "&nVlLargura=" . $largura . "&sCdMaoPropria=n&nVlValorDeclarado=" . $valor . "&sCdAvisoRecebimento=n&nCdServico=" . $servico . "&nVlDiametro=0&StrRetorno=xml";
                // Carrega o XML de Retorno
                $xml = simplexml_load_file($correios);
                // Verifica se não há erros
                if ($xml->cServico->Erro == '0') {
                    return $xml->cServico->Valor;
                } else {
                    return false;
                }
            }

            // $frete2 = $this->Frete()->calculafrete('40010','39403076','39400076','0.5');

            if (isset($_POST['casal'])) {
                $request = $this->getRequest();
                if ($request->isPost()) {
                    $casal_session->casal = $_POST['casal'];
                }
            }

            $preco_frete = 0;
            $preco = 0;
            foreach ($carrinho_session->carrinho as $id => $qtd) {
                $adapter = $this->getServiceLocator()->get('DbAdapter');
                $sql = new Sql($adapter);
                $select = $sql->select()
                        ->columns(array('id', 'peso', 'altura', 'comprimento', 'largura'))
                        ->from('produtos')
//                        ->join('marcas', 'produtos.marca_id = marcas.id', array('mnome' => 'nome'))
//                        ->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('snome' => 'nome'))
                        ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem'))
                        ->where(array("produtos.id=$id", 'profotos.principal=1'));
                $statement = $sql->prepareStatementForSqlObject($select);
                $prod_frete = $statement->execute();
                //
                foreach ($prod_frete as $prod) {

                    $altura3 = number_format($prod['altura']);
                    $largura3 = number_format($prod['largura']);
                    $comprimento3 = number_format($prod['comprimento']);
                    $peso3 = number_format($prod['peso']);

//                    $altura3 = '6';
//                    $largura3 = '12';
//                    $comprimento3 = '18';
//                    $peso3 = '0.5';

                    $tipo2 = "";
                    $request = $this->getRequest();
                    if ($request->isPost()) {
                        if (isset($_POST['tipo'])) {
                            $tipo2 = $_POST['tipo'];
                        }
                    }

                    $a = $tipo2 . " " . $ceploja . " " . $cepcliente . " " . $pesototal;
                    if (isset($tipo2)) {
//                         $frete2 = calcula_frete(40010,39403076,39400076,0.5,6,12,18);
                        $frete2 = calcula_frete($tipo2, $ceploja, $cepcliente, $peso3, $altura3, $largura3, $comprimento3, $valor = '1.00');
                        $frete3 = str_replace(",", ".", $frete2);
                        $preco1 = ($preco_frete += $frete3) * $qtd;
                        $frete4 = str_replace(",", ".", $frete2);
                        // $frete3 = "calcula_frete($tipo2,$ceploja,$cepcliente,$pesototal2)";
                        // $frete2 = $this->Frete()->calculafrete($tipo2, $ceploja, $cepcliente, $pesototal);
//                $this->Frete()->calculafrete($tipo, $ceploja, $cep, $peso);
                        // $frete = str_replace(",", ".", $frete);

                        $tipofrete_session->tipofrete = (int) $tipo2;
                        //$carrinhofrete->carrinhofrete = $frete;
                    }
                    $preco += $preco1;
                }
            }
            $frete_session->frete = floatval($preco);
            $preco_session->preco = floatval($precototal);

            $select = $sql->select()
                    ->from('noivos');
            $statement = $sql->prepareStatementForSqlObject($select);
            $casais = $statement->execute();

            $select = $sql->select()
                    ->columns(array('id', 'parametro'))
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $select4 = $sql->select()
                    ->columns(array('id', 'file', 'nome', 'ativo'))
                    ->from('marcas');
            //->where(array('marcas.ativo=1'));	

            $view = new ViewModel(array(
                'tipo2' => $tipo2,
                'frete' => $frete,
                'casal2' => $casal_session->casal,
                'teste' => $frete_session->frete,
                'total' => $preco_session->preco,
                'configuracoes' => $configuracoes,
                'casais' => $casais,
                'frete2' => $frete2,
                'frete3' => $frete3,
                'a' => $a,
                'preco' => $preco,
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'])) . '/application/index/passo1';
        }
    }

    public function passo3Action() {
        $frete_session = new Container('frete');
        $preco_session = new Container('preco');
        $tipofrete_session = new Container('tipofrete');
        $cliente_session = new Container('cliente');
        $pedido_session = new Container('pedido');
        if (isset($cliente_session->cliente) && isset($pedido_session->pedido)) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('parametro'))
                    ->from('configuracoes')
                    ->where(array('configuracoes.id=1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracao = $statement->execute();

            //Dados do pedido
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'data', 'frete', 'valor_produtos', 'valor_frete', 'status', 'pago', 'clientes_id'))
                    ->from('pedidos')
                    ->join('clientes', 'clientes.id = pedidos.clientes_id', array('cnome' => 'nome'))
                    ->where(array('pedidos.id=' . $pedido_session->pedido));

            $statement = $sql->prepareStatementForSqlObject($select);
            $pedido = $statement->execute();

            //Dados do cliente
            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'nome', 'email', 'rua', 'bairro', 'numero', 'cidade', 'estado', 'cep'))
                    ->from('clientes')
                    ->where(array('clientes.id=' . $cliente_session->cliente['id']));

            $statement = $sql->prepareStatementForSqlObject($select);
            $cliente = $statement->execute();


            //Produtos do pedido
            $select2 = $sql->select()
                    ->columns(array('id', 'produto_id', 'pedido_id', 'quantidade',))
                    ->from('pedido_produtos')
                    ->join('produtos', 'pedido_produtos.produto_id = produtos.id', array('nome', 'preco', 'preco_promocao', 'promocao', 'descricao'))
                    ->where(array('pedido_produtos.pedido_id=' . $pedido_session->pedido));

            $statement2 = $sql->prepareStatementForSqlObject($select2);
            $produtos = $statement2->execute();

            $select = $sql->select()
                    ->from('configuracoes');
            $statement = $sql->prepareStatementForSqlObject($select);
            $configuracoes = $statement->execute();

            $view = new ViewModel(array(
                'id_pedido' => $pedido_session->pedido,
                'pedidos' => $pedido,
                'produtos' => $produtos,
                'produtospag' => $produtos,
                'cliente' => $cliente,
                'configuracao' => $configuracao,
                'configuracoes' => $configuracoes,
                'total' => $preco_session->preco,
                'frete' => $frete_session->frete,
                'tipofrete' => $tipofrete_session->tipofrete,
            ));
            return $view;
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'])) . '/application/index/passo1';
        }
    }

    public function loginAction() {

        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost();
            $data = $data->toArray();

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id', 'email', 'nome', 'telefone', 'cpf', 'rua', 'bairro', 'numero', 'cidade', 'estado', 'cep'))
                    ->from('clientes')
                    ->where(array("clientes.email='" . $data['email'] . "'", "clientes.senha='" . $data['senha'] . "'"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            foreach ($result as $cliente) {
                $cli['id'] = $cliente['id'];
                $cli['email'] = $cliente['email'];
                $cli['nome'] = $cliente['nome'];
            }
//if (!empty($result)) {
            $cliente_session = new Container('cliente');
            $cliente_session->cliente = $cli;
//}
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/passo2');
        } else {
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/passo1');
        }
    }

    public function confirmarAction() {
        $cliente_session = new Container('cliente');
        $pedido_session = new Container('pedido');
        $carrinho_session = new Container('carrinho');
        $carrinhoprodutos = new Container('carrinhoprodutos');
        $casal_session = new Container('casal');
        $frete_session = new Container('frete');
        $tipofrete_session = new Container('tipofrete');
//        $valor_frete = str_replace(",",".",$frete_session->frete);
//        $tipofrete = $tipofrete_session->tipofrete;
        $valor_frete = $frete_session->frete;
        $cas = $casal_session->casal;
        $tipofrete = $tipofrete_session->tipofrete;

        if (isset($cliente_session->cliente) && isset($carrinho_session->carrinho)) {

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id'))
                    ->from('clientes')
                    ->where(array("clientes.email='" . $cliente_session->cliente['email'] . "'"));
            $statement = $sql->prepareStatementForSqlObject($select);
            $cliente = $statement->execute();

            $cliente_id = 0;

            $valor_frete = number_format((double) $valor_frete, 2, '.', ',');
            //echo $valor_frete;
            //die($valor_frete);
            $valor_produtos = 0;


            foreach ($cliente as $cli) {
                $cliente_id = $cli['id'];
            }

            $produtos = '';
            $procarrinho = array();
            foreach ($carrinho_session->carrinho as $id => $qtd) {
                $adapter = $this->getServiceLocator()->get('DbAdapter');
                $sql = new Sql($adapter);
                $select = $sql->select()
                        ->columns(array('id', 'nome', 'preco', 'preco_promocao', 'promocao'))
                        ->from('produtos')
                        ->where(array("produtos.id=$id"));
                $statement = $sql->prepareStatementForSqlObject($select);
                $produtos = $statement->execute();

                foreach ($produtos as $produto) {
                    $procarrinho[$id]['id'] = $produto['id'];
                    $procarrinho[$id]['preco'] = $produto['preco'];
                    $procarrinho[$id]['promocao'] = $produto['promocao'];
                    $procarrinho[$id]['nome'] = $produto['nome'];
                    $procarrinho[$id]['preco_promocao'] = $produto['preco_promocao'];
                    $procarrinho[$id]['quantidade'] = $qtd;
                    if ($produto['promocao'] == '1') {
                        $valor_produtos += $produto['preco_promocao'] * $qtd;
                    } else {
                        $valor_produtos += $produto['preco'] * $qtd;
                    }
                }
            }

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $sql->getAdapter()->getDriver()->getConnection()->beginTransaction();
            $select = $sql->insert()
                    ->into('pedidos')
                    ->columns(array('valor_produtos', 'valor_frete', 'clientes_id', 'tipo_frete'))
                    ->values(array('valor_produtos' => $valor_produtos, 'valor_frete' => $valor_frete, 'clientes_id' => $cliente_id, 'tipo_frete' => $tipofrete, 'casal_id' => $cas));
            $statement = $sql->prepareStatementForSqlObject($select);
            $pedido = $statement->execute();

            $pedido_id = $sql->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('pedidos.id');

            $pedido_session->pedido = $pedido_id;

            foreach ($procarrinho as $produto) {
                $select = $sql->insert()
                        ->into('pedido_produtos')
                        ->columns(array('produto_id', 'pedido_id', 'quantidade'))
                        ->values(array('produto_id' => $produto['id'], 'pedido_id' => $pedido_id, 'quantidade' => $produto['quantidade']));
                $statement = $sql->prepareStatementForSqlObject($select);
                $pedido = $statement->execute();

                $adapter2 = $this->getServiceLocator()->get('DbAdapter');
                $sql2 = new Sql($adapter2);
                $sql2 = "UPDATE produtos
                SET estoque=estoque-" . $produto['quantidade'] . "
                WHERE id=" . $produto['id'] . ";";
                $statement2 = $adapter->query($sql2);
                $atualizar2 = $statement2->execute();
            }
            $sql->getAdapter()->getDriver()->getConnection()->commit();
            $a = "";
            $request = $this->getRequest();
            if ($request->isPost()) {
                $a = "?cep=" . $_POST['cep'];
            }

//Pega os dados postados pelo formulário HTML e os coloca em variaveis
            if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
//substitua na linha acima a aprte locaweb.com.br por seu domínio.
                $email_from = 'cadastro@' . $_SERVER[HTTP_HOST]; // Substitua essa linha pelo seu e-mail@seudominio
            } else {
                $email_from = "cadastro@" . $_SERVER[HTTP_HOST];
//    Na linha acima estamos forçando que o remetente seja 'webmaster@',
// você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            }


            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }

//pego os dados enviados pelo formulário 
            $nome_para = $cliente_session->cliente['nome'];
            $email_de = $cliente_session->cliente['email'];
            $email = $cliente_session->cliente['email'];
//            $email = "yuri@agenciaw3.com.br,yurimoreira2309@gmail.com";
            $assunto = "Compra - LA CASA";
            // $mensagem = $_POST["msg"];
            //formato o campo da mensagem 
            $mensagem_cont = "
			<p style='text-align:justify;font-size:16px;line-height:22px'>CARO(A) <b style='text-transform: uppercase'>" . $nome_para . ",</b><br>
			1. O sua compra na loja do site <b><a target='blank' style='text-decoration:none;' href='http://lacasaloja.com.br'>www.lacasaloja.com.br</a></b> foi efetuada com sucesso;<br>
			2. Imprima o boleto, pague em qualquer casa lotérica e aguarde o recebimento do(s) produto(s);<br>
			2. Para maiores informações entre em contato conosco pelo formulário de contato do site ou por telefone;<br><br>
			<b>Agradecemos à preferência!<b>
			</p>";

            $arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

            if (file_exists($arquivo["tmp_name"]) and ! empty($arquivo)) {

                $fp = fopen($_FILES["arquivo"]["tmp_name"], "rb");
                $anexo = fread($fp, filesize($_FILES["arquivo"]["tmp_name"]));
                $anexo = base64_encode($anexo);

                fclose($fp);

                $anexo = chunk_split($anexo);


                $boundary = "XYZ-" . date("dmYis") . "-ZYX";

                $mens = "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: 8bits" . $quebra_linha . "";
                $mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"" . $quebra_linha . "" . $quebra_linha . ""; //plain 
                $mens .= "$mensagem_cont" . $quebra_linha . "";
                $mens .= "--$boundary" . $quebra_linha . "";
                $mens .= "Content-Type: " . $arquivo["type"] . "" . $quebra_linha . "";
                $mens .= "Content-Disposition: attachment; filename=\"" . $arquivo["name"] . "\"" . $quebra_linha . "";
                $mens .= "Content-Transfer-Encoding: base64" . $quebra_linha . "" . $quebra_linha . "";
                $mens .= "$anexo" . $quebra_linha . "";
                $mens .= "--$boundary--" . $quebra_linha . "";

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";
                $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"" . $quebra_linha . "";
                $headers .= "$boundary" . $quebra_linha . "";


//envio o email com o anexo 
                mail($email, $assunto, $mens, $headers, "-r" . $email_from);

                // echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }

//se nao tiver anexo 
            else {

                $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                $headers .= "Content-type: text/html; charset=iso-8859-1" . $quebra_linha . "";
                $headers .= "From: $email_from " . $quebra_linha . "";
                $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
                mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);


                // echo "<script>alert('Cadastro realizado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
            }

            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/passo3' . $a);
        } else {
            return $this->redirect()->toUrl('/application/index/passo1');
        }
    }

    /* public function produtosAction() {


      $adapter = $this->getServiceLocator()->get('DbAdapter');
      $sql = new Sql($adapter);
      $select = $sql->select()
      ->columns(array('id', 'nome', 'preco', 'preco_promocao', 'descricao'))
      ->from('produtos')
      ->join('profotos', 'profotos.produto_id = produtos.id', array('imagem'))
      ->where(array('produtos.promocao=1', 'profotos.principal=1'));
      $statement = $sql->prepareStatementForSqlObject($select);
      $produtos = $statement->execute();

      $view = new ViewModel(array(
      'produtos' => $produtos
      ));
      return $view;
      } */
}
