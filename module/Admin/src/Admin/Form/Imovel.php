<?php
namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Imovel extends Form
{
    public function __construct()
    {
        parent::__construct('imovel');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/imovel/cadastrar');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                ),
            ));

        $this->add(array(
            'name' => 'plano_id',
            'attributes' => array(
                'type'  => 'hidden',
                ),
            ));
        
        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Titulo',
                ),
            ));

        $this->add(array(
            'name' => 'preco',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Preço',
                ),
            ));

        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Descrição',
                ),
            ));

        

        $this->add(array(
            'name' => 'estado_id',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Estado',
                ),
            ));
        
        $this->add(array(
            'name' => 'cidade_id',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Cidade',
                ),
            ));

        $this->add(array(
            'name' => 'bairro',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Bairro',
                ),
            ));

        $this->add(array(
            'name' => 'tipo_id',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Tipo',
                ),
            ));

        $this->add(array(
            'name' => 'finalidade',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Finalidade',
                ),
            ));

        $this->add(array(
            'name' => 'quartos',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Quartos',
                ),
            ));

        $this->add(array(
            'name' => 'banheiros',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Banheiros',
                ),
            ));

        $this->add(array(
            'name' => 'area_total',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Área total',
                ),
            ));

        $this->add(array(
            'name' => 'area_construida',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Área Construida',
                ),
            ));

        $this->add(array(
            'name' => 'garagem',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Garagem',
                ),
            ));

        $this->add(array(
            'name' => 'suites',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Suites',
                ),
            ));

        $this->add(array(
            'name' => 'condominio',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Condominio',
                ),
            ));

        $this->add(array(
            'name' => 'usuario_id',
            'attributes' => array(
                'type'  => 'hidden',
                ),
            ));

        
        
        $this->add(array(
            'name' => 'condominio',
            'attributes' => array(
                'type'  => 'text',
                ),
            'options' => array(
                'label' => 'Condomínio',
                ),
            ));
        
        $this->add(array(
            'name' => 'criacao',
            'attributes' => array(
                'type'  => 'hidden',
                ),
            ));
        
        $this->add(array(
            'name' => 'validade',
            'attributes' => array(
                'type'  => 'hidden',
                ),
            ));

         $this->add(array(
            'name' => 'status',
            'attributes' => array(
                'type'  => 'hidden',
                ),
            ));

        

        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Finalizar',
                'id' => 'submitbutton',
                ),
            ));
    }
}