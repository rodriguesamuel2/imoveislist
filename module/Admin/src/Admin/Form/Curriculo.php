<?php
namespace Admin\Form;
 
use Zend\Form\Form;
 
class Curriculo extends Form
{
    public function __construct()
    {
        parent::__construct('Curriculo');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/curriculo/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
//        $this->add(array(
//            'name' => 'clienteid',
//            'attributes' => array(
//                'type'  => 'hidden',
//            ),
//        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Nome Completo',
            ),
        ));
        
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        
        $this->add(array(
            'name' => 'objetivo',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Objetivo Profissional',
            ),
        ));
        
        $this->add(array(
            'name' => 'perfil',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Qualificações Profissionais simplificada',
            ),
        ));
        
        $this->add(array(
            'name' => 'arquivo',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Arquivo curriculo(.doc)',
            ),
        )); 
        
       
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}