<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Parcela extends Form {

    public function __construct($motoid, $planos, $versoes) {
        parent::__construct('Parcela');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/parcela/save/motoid/'.$motoid);

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'moto_id',
            'attributes' => array(
                'type' => 'hidden',
                'value' => $motoid
            ),
        ));

        $planos2;
        foreach ($planos as $plano) {
            $planos2[$plano['id']] = $plano['descricao'];
        }
        //Preenche o select com o array de categorias
        $plano = new Element\Select('plano_id');
        $plano->setLabel('Planos');
        $plano->setValueOptions($planos2);
        $this->add($plano);

        $versoes2;
        foreach ($versoes as $versao) {
            $versoes2[$versao['id']] = $versao['nome'];
        }
        //Preenche o select com o array de categorias
        $versao = new Element\Select('versao_id');
        $versao->setLabel('Versão');
        $versao->setValueOptions($versoes2);
        $this->add($versao);

        $this->add(array(
            'name' => 'qtde',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Quantidade de parcelas',
            ),
        ));

        $this->add(array(
            'name' => 'valor_parcela',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Valor das parcelas',
            ),
        ));

        $this->add(array(
            'name' => 'valor_credito',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Valor do Crédito',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar'
            ),
        ));
    }

}