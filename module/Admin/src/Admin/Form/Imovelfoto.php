<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Imovelfoto extends Form {

    public function __construct($imovelid) {
        parent::__construct('Imovelfoto');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovelfoto/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                'label' => 'Imagem(1024x768 pixels)',
            ),
        ));

        $this->add(array(
            'name' => 'imovel_id',
            'attributes' => array(
                'type' => 'hidden',
                'value' => $imovelid
            ),
        ));       


        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar'
            ),
        ));
    }

}
