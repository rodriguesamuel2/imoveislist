<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Produto extends Form {

    public function __construct($marcas, $subcategorias) {
        parent::__construct('produto');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/produto/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Nome do Produto',
            ),
        ));
        $this->add(array(
            'name' => 'endereco',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Link do site do fabricante',
            ),
        ));

        // $this->add(array(
        // 'name' => 'codigoloja',
        // 'attributes' => array(
        // 'type' => 'text',
        // ),
        // 'options' => array(
        // 'label' => 'Código do Produto',
        // ),
        // ));

//        $this->add(array(
//            'name' => 'preco',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Preço a Vista',
//            ),
//        ));
//        $this->add(array(
//            'name' => 'preco_promocao',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Preço a Prazo',
//            ),
//        ));

//        $this->add(array(
//            'name' => 'comprimento',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Comprimento(cm)',
//            ),
//        ));

        

//        $this->add(array(
//            'name' => 'estoque',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Estoque(UN)',
//            ),
//        ));

//        $this->add(array(
//            'name' => 'largura',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Largura(cm)',
//            ),
//        ));

//        $this->add(array(
//            'name' => 'funcao',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Vagas Para veículos',
//            ),
//        ));
        
//        $this->add(array(
//            'name' => 'descricao',
//            'attributes' => array(
//                'type' => 'textarea',
//                'class' => 'campos texto'
//            ),
//            'options' => array(
//                'label' => 'Detalhes do Produto',
//            ),
//        ));

//        $this->add(array(
//            'name' => 'beneficios',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Especificações',
//            ),
//        ));

        



//        $this->add(array(
//            'name' => 'embalagem',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Garantia',
//            ),
//        ));
        
//        $this->add(array(
//            'name' => 'caracteristicas',
//            'attributes' => array(
//                'type' => 'text',
//            ),
//            'options' => array(
//                'label' => 'Cidade',
//            ),
//        ));


        $cat[1] = 'Sim';
        $cat[2] = 'Não';
//        $cat["ALUGUEL DE QUARTO"] = 'ALUGUEL DE QUARTO';
//        $promocao = new Element\Select('funcao');
//        $promocao->setLabel('Tipo do Imóvel:');
//        $promocao->setValueOptions($cat);
//        $this->add($promocao);
        // $promocao2 = new Element\Select('novo');
        // $promocao2->setLabel('Produto é novidade?');
        // $promocao2->setValueOptions($cat);
        // $this->add($promocao2);
//         $oferta = new Element\Select('oferta');
//         $oferta->setLabel('Produto em Promoção(HOME)');
//         $oferta->setValueOptions($cat);
//         $this->add($oferta);
        // $novo = new Element\Select('novo');
        // $novo->setLabel('Produto é novo');
        // $novo->setValueOptions($cat);
        // $this->add($novo);
         $marcas2;
         foreach ($marcas as $marca) {
         $marcas2[$marca['id']] = $marca['nome'];
         }
//         Preenche o select com o array de categorias
         $marca = new Element\Select('marca_id');
         $marca->setLabel('Marca');
         $marca->setValueOptions($marcas2);
         $this->add($marca);

        $subcategorias2;
        foreach ($subcategorias as $subcategoria) {
            $subcategorias2[$subcategoria['id']] = $subcategoria['nome'];
        }
        // Preenche o select com o array de categorias
        $subcategoria = new Element\Select('subcategoria_id');
        $subcategoria->setLabel('Categoria');
        $subcategoria->setValueOptions($subcategorias2);
        $this->add($subcategoria);


        // $ativo = new Element\Select('ativo');
        // $ativo->setLabel('Produto vai aparecer no site?');
        // $ativo->setValueOptions($cat);
        // $this->add($ativo);
        // $this->add(array(
        // 'name' => 'estoque',
        // 'attributes' => array(
        // 'type' => 'text',
        // ),
        // 'options' => array(
        // 'label' => 'Estoque do produto',
        // ),
        // ));
        // $this->add(array(
        // 'name' => 'peso',
        // 'attributes' => array(
        // 'type' => 'text',
        // ),
        // 'options' => array(
        // 'label' => 'Peso',
        // ),
        // ));
        // $this->add(array(
        // 'name' => 'altura',
        // 'attributes' => array(
        // 'type' => 'text',
        // ),
        // 'options' => array(
        // 'label' => 'Altura',
        // ),
        // ));
        // $this->add(array(
        // 'name' => 'largura',
        // 'attributes' => array(
        // 'type' => 'text',
        // ),
        // 'options' => array(
        // 'label' => 'Largura',
        // ),
        // ));
        // $this->add(array(
        // 'name' => 'comprimento',
        // 'attributes' => array(
        // 'type' => 'text',
        // ),
        // 'options' => array(
        // 'label' => 'Comprimento',
        // ),
        // ));




        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar'
            ),
        ));
    }

}
