<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;
 
class Moto extends Form
{
    public function __construct()
    {
        parent::__construct('moto');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/moto/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
        $this->add(array(
            'name' => 'frase',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Frase',
            ),
        ));
        $this->add(array(
            'name' => 'especificacoes',
            'attributes' => array(
                'type'  => 'textarea',
                'class'=>'campos texto'
            ),
            'options' => array(
                'label' => 'Texto da moto',
            ),
        ));
        
        $cat[1]='Sim';
        $cat[2]='Não';
        $ativo = new Element\Select('ativo');
        $ativo->setLabel('Essa moto irá aprecer no site?');
        $ativo->setValueOptions($cat);
        $this->add($ativo);
        
        //$cat[1]='Sim';
        //$cat[2]='Não';
        $consorcio = new Element\Select('consorcio');
        $consorcio->setLabel('Essa moto faz parte do consórcio?');
        $consorcio->setValueOptions($cat);
        $this->add($consorcio);
        
        $tipos[1]='Internacional';
        $tipos[2]='Nacional';
        $tipo = new Element\Select('tipo');
        $tipo->setLabel('A moto é internacional ou nacional?');
        $tipo->setValueOptions($tipos);
        $this->add($tipo);
        
        $this->add(array(
            'name' => 'cilindrada',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Cilindrada',
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}