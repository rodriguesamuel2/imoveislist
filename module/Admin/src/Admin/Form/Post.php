<?php

namespace Admin\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class Post extends Form {

    public function __construct() {
        parent::__construct('Post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/post/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Título da Notícia',
            ),
        ));

        // $this->add(array(
            // 'name' => 'autor',
            // 'attributes' => array(
                // 'type' => 'text',
            // ),
            // 'options' => array(
                // 'label' => 'Telefone da Loja',
            // ),
        // ));

        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Descrição da Notícia',
            ),
        ));

        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                // 'label' => 'Foto da Loja (398x193 pixels)',
                'label' => 'Imagem da Notícia',
            ),
        ));
		
        // $this->add(array(
            // 'name' => 'video',
            // 'attributes' => array(
                // 'type' => 'text',
                // 'class' => 'campos',
            // ),
            // 'options' => array(
                // 'label' => 'Link do Google Maps',
            // ),
        // ));	

        // $this->add(array(
            // 'name' => 'endereco',
            // 'attributes' => array(
                // 'type' => 'text',
                // 'class' => 'campos',
            // ),
            // 'options' => array(
                // 'label' => 'Endereço da Loja',
            // ),
        // ));			

//        $this->add(array(
//            'name' => 'video',
//            'attributes' => array(
//                'type' => 'textarea',
//            ),
//            'options' => array(
//                'label' => 'Video',
//            ),
//        ));

        $opcoes[1] = 'Sim';
        $opcoes[2] = 'Não';
        $status = new Element\Select('status');
        $status->setLabel('Mostrar no site?');
        $status->setValueOptions($opcoes);
        $this->add($status);

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

}