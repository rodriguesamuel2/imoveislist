<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Prodlista extends Form {

    public function __construct($casalid,$produtos,$listas) {
        parent::__construct('prodlista');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/prodlista/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

         $this->add(array(
            'name' => 'noivo_id',
            'attributes' => array(
                'type'  => 'hidden',
                'value' => $casalid
            ),
        ));		

        $x=0;
        $cat3;
        foreach ($listas as $lista) {
          $cat3[$lista['id']] = $lista['produto_id'];
		$x++;  
        }	
		
        if($x>0){ 
			$cat2;
			foreach ($produtos as $prod) {
				if($cat3[$lista['id']]!=$prod['id']){
					$cat2[$prod['id']] = $prod['nome'];
				}
			}
			// Preenche o select com o array de PRODUTOS
			$select2 = new Element\Select('produto_id');
			$select2->setLabel('Selecione o Produto');
			$select2->setValueOptions($cat2);
			$this->add($select2);	
		}else{
			$cat2;
			foreach ($produtos as $prod) {
					$cat2[$prod['id']] = $prod['nome'];
			}
			// Preenche o select com o array de PRODUTOS
			$select2 = new Element\Select('produto_id');
			$select2->setLabel('Selecione o Produto');
			$select2->setValueOptions($cat2);
			$this->add($select2);
		}

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

}
