<?php

namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class Banner extends Form
{
    public function __construct()
    {
        parent::__construct('Banner');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('action',str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/banner/save');
        
         $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
         
         
         
        $this->add(array(
            'name' => 'legenda',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Título',
            ),
        ));
		
         $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Link',
            ),
        ));	 
		
        // $this->add(array(
            // 'name' => 'ordem',
            // 'attributes' => array(
                // 'type'  => 'text',
            // ),
            // 'options' => array(
                // 'label' => 'Ordem',
            // ),
        // ));
 
         
        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Imagem (1920x750 pixels)',
            ),
        )); 
        
         // $this->add(array(
            // 'name' => 'link',
            // 'attributes' => array(
                // 'type'  => 'text',
            // ),
            // 'options' => array(
                // 'label' => 'Link',
            // ),
        // )); 
        
        $cat[1]='Sim';
        $cat[2]='Não';
        $ativo = new Element\Select('ativo');
        $ativo->setLabel('O banner vai aparecer no site?');
        $ativo->setValueOptions($cat);
        $this->add($ativo);
         
         
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar'
            ),
        )); 
    }
}