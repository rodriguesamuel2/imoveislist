<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Vagas extends Form {

    public function __construct($cat) {
        parent::__construct('Vagas');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/vagas/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'cliente_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Nome da Vaga Emprego',
            ),
        ));
		
        $estado["AC"]='AC';
        $estado["AL"]='AL';
        $estado["AP"]='AP';
        $estado["AM"]='AM';
        $estado["BA"]='BA';
        $estado["CE"]='CE';
        $estado["DF"]='DF';
        $estado["ES"]='ES';
        $estado["GO"]='GO';
        $estado["MA"]='MA';
        $estado["MS"]='MS';
        $estado["MT"]='MT';
        $estado["MG"]='MG';
        $estado["PA"]='PA';
        $estado["PB"]='PB';
        $estado["PR"]='PR';
        $estado["PE"]='PE';
        $estado["PI"]='PI';
        $estado["RJ"]='RJ';
        $estado["RN"]='RN';
        $estado["RS"]='RS';
        $estado["RO"]='RO';
        $estado["RR"]='RR';
        $estado["SC"]='SC';
        $estado["SP"]='SP';
        $estado["SE"]='SE';
        $estado["TO"]='TO';

        $estado2 = new Element\Select('estado');
        $estado2->setLabel('Estado da Vaga Emprego');
        $estado2->setValueOptions($estado);
        $this->add($estado2);		
		
        $subcategorias2;
        foreach ($cat as $subcategoria) {
          $subcategorias2[$subcategoria['id']] = $subcategoria['nome'];
        }
        // Preenche o select com o array de categorias
        $subcategoria = new Element\Select('nivel');
        $subcategoria->setLabel('Área de Trabalho');
        $subcategoria->setValueOptions($subcategorias2);
        $this->add($subcategoria);		

        $this->add(array(
            'name' => 'cidade',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Cidade da Vaga Emprego',
            ),
        ));		
		
        $cat123["MEIO TURNO (22H SEMANAIS)"]='MEIO TURNO (22H SEMANAIS)';
        $cat123["TURNO COMPLETO (44H SEMANAIS)"]='TURNO COMPLETO (44H SEMANAIS)';
        $cat123["OUTROS"]='OUTROS';
        $promocao = new Element\Select('area');
        $promocao->setLabel('Turno da Vaga Emprego:');
        $promocao->setValueOptions($cat123);
        $this->add($promocao);		
		
        $this->add(array(
            'name' => 'codigo',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Faixa Salarial da Vaga Emprego',
            ),
        ));

        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type' => 'textarea',
                // 'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Descrição da Vaga Emprego',
            ),
        ));		
       

        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                'label' => 'Imagem da Vaga Emprego',
            ),
        ));
        
        $cat2['1']='Sim';
        $cat2['2']='Não';
        $ativo = new Element\Select('ativo');
        $ativo->setLabel('Vaga pode aparecer no site?');
        $ativo->setValueOptions($cat2);
        $this->add($ativo);
		

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

}