<?php
namespace Admin\Form;
 
use Zend\Form\Form;
 
class Login extends Form
{
    public function __construct()
    {
        parent::__construct('login');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/auth/login');
        
        $this->add(array(
            'name' => 'nomeusuario',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'nomeusuario',
            ),
        ));
        $this->add(array(
            'name' => 'senha',
            'attributes' => array(
                'type'  => 'password',
            ),
            'options' => array(
                'label' => 'senha',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Entrar',
                'id' => 'submitbutton',
            ),
        ));
    }
}