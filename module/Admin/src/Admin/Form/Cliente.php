<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;
 
class Cliente extends Form
{
    public function __construct()
    {
        parent::__construct('Cliente');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/cliente/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
        
//        $this->add(array(
//            'name' => 'imagem',
//            'attributes' => array(
//                'type'  => 'file',
//            ),
//            'options' => array(
//                'label' => 'Logomarca(300x300 pixels)',
//            ),
//        )); 
        
        $this->add(array(
            'name' => 'cpf',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Cpf',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Telefone',
            ),
        ));
       
        $cat[1]='Sim';
        $cat[0]='Não';
        $promocao = new Element\Select('ativo');
        $promocao->setLabel('Cliente está ativo?');
        $promocao->setValueOptions($cat);
        $this->add($promocao);
        
        $this->add(array(
            'name' => 'data',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Cadastro',
            ),
        ));
        $this->add(array(
            'name' => 'cep',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Cep',
            ),
        ));
        $this->add(array(
            'name' => 'rua',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Rua',
            ),
        ));
        $this->add(array(
            'name' => 'bairro',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Bairro',
            ),
        ));
        $this->add(array(
            'name' => 'numero',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Número',
            ),
        ));
        $this->add(array(
            'name' => 'cidade',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Cidade',
            ),
        ));
        $this->add(array(
            'name' => 'estado',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Estado',
            ),
        ));
        $this->add(array(
            'name' => 'site',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Link do Site',
            ),
        ));
        
        $this->add(array(
            'name' => 'observacoes',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Observações',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}