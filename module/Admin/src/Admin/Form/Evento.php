<?php
namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class Evento extends Form
{
    public function __construct()
    {
        parent::__construct('evento');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');		
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/evento/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Título do Evento',
            ),
        ));

        $this->add(array(
		'name' => 'dia',
		'attributes' => array(
		'type'  => 'int',
		// 'class'=>'campos texto'
		),
		'options' => array(
		'label' => 'Dia do Evento',
		),
        ));		
		
		$mes[1]='Janeiro';
        $mes[2]='Fevereiro';
        $mes[3]='Março';
        $mes[4]='Abril';
        $mes[5]='Maio';
        $mes[6]='Junho';
        $mes[7]='Julho';
        $mes[8]='Agosto';
        $mes[9]='Setembro';
        $mes[10]='Outubro';
        $mes[11]='Novembro';
        $mes[12]='Dezembro';
        $status = new Element\Select('mes');
        $status->setLabel('Mês do Evento:');
        $status->setValueOptions($mes);
        $this->add($status);				
		
        $y = date('Y');
		
        $x = '2000';
		
		
		
        while ($x <= $y) {
			
			$ano[$x] = $x;
			
            $x++;
			
        }
		
        $anos = new Element\Select('ano');
		
        $anos->setLabel('Ano do Evento:');
		
        $anos->setValueOptions($ano);
		
        $this->add($anos);
		
        $this->add(array(
            'name' => 'depoimento',
            'attributes' => array(
                'type'  => 'textarea',
                'class'=>'campos texto'
            ),
            'options' => array(
                'label' => 'Texto do Evento',
            ),
        ));
 
        $this->add(array(
		'name' => 'imagem',
		'attributes' => array(
		'type' => 'file',
		),
		'options' => array(
		'label' => 'Imagem do Evento',
		),
        ));
 
        $opcoes[1]='Sim';
        $opcoes[2]='Não';
        $status = new Element\Select('status');
        $status->setLabel('Mostrar este Evento no site?');
        $status->setValueOptions($opcoes);
        $this->add($status);
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}