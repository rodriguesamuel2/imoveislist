<?php
namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class Imobiliaria extends Form
{
    public function __construct()
    {
        parent::__construct('imobiliaria');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/imobiliaria/registrar');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Descrição',
            ),
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));
        
        $this->add(array(
            'name' => 'endereco',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));
        
        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));
        
        $this->add(array(
            'name' => 'cep',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));
        
        $this->add(array(
            'name' => 'website',
            'attributes' => array(
                'type'  => 'text',
            ),
        ));
        $this->add(array(
            'name' => 'usuario_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'logomarca',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Foto',
            ),
        ));
        $this->add(array(
            'name' => 'twitter',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Twitter',
            ),
        ));
        

        

        $this->add(array(
            'name' => 'facebook',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Facebook',
            ),
        ));

        $this->add(array(
            'name' => 'skype',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Skype',
            ),
        ));
        $this->add(array(
            'name' => 'celular',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Celular',
            ),
        ));

        $this->add(array(
            'name' => 'cnpj',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'cnpj',
            ),
        ));
        
        $this->add(array(
            'name' => 'estado_id',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'cnpj',
            ),
        ));
        
        $this->add(array(
            'name' => 'cidade_id',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'cnpj',
            ),
        ));
        

        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}