<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Servicos extends Form {

    public function __construct($cat) {
        parent::__construct('Servicos');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/servicos/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Nome do Serviço',
            ),
        ));
		
        $subcategorias2;
        foreach ($cat as $subcategoria) {
          $subcategorias2[$subcategoria['id']] = $subcategoria['nome'];
        }
        // Preenche o select com o array de categorias
        $subcategoria = new Element\Select('area');
        $subcategoria->setLabel('Categoria');
        $subcategoria->setValueOptions($subcategorias2);
        $this->add($subcategoria);		

        $cat123["EMPRESAS ESPECIALIZADAS"]='EMPRESAS ESPECIALIZADAS';
        $cat123["DIARISTAS"]='DIARISTAS';
        $promocao = new Element\Select('iframe');
        $promocao->setLabel('Tipo do Serviço:');
        $promocao->setValueOptions($cat123);
        $this->add($promocao);		
		
        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Preço do Serviço',
            ),
        ));

        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type' => 'textarea',
                // 'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Descrição do Serviço',
            ),
        ));
		
        // $this->add(array(
            // 'name' => 'telefone',
            // 'attributes' => array(
                // 'type' => 'text',
                // 'class' => 'campos',
            // ),
            // 'options' => array(
                // 'label' => 'Telefones',
            // ),
        // ));		
       

        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                'label' => 'Imagem do Serviço',
            ),
        ));

        // $this->add(array(
            // 'name' => 'iframe',
            // 'attributes' => array(
                // 'type' => 'text',
                // 'class' => 'campos',
            // ),
            // 'options' => array(
                // 'label' => 'Link do Blog',
            // ),
        // ));		

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

}