<?php
namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class Depoimento extends Form
{
    public function __construct()
    {
        parent::__construct('depoimento');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/depoimento/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
        $this->add(array(
            'name' => 'depoimento',
            'attributes' => array(
                'type'  => 'textarea',
                'class'=>'campos texto'
            ),
            'options' => array(
                'label' => 'Texto do depoimento',
            ),
        ));
        
        $opcoes[1]='Sim';
        $opcoes[2]='Não';
        $status = new Element\Select('status');
        $status->setLabel('Mostrar este depoimento no site?');
        $status->setValueOptions($opcoes);
        $this->add($status);
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}