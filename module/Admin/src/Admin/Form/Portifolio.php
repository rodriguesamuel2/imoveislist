<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Portifolio extends Form {

    public function __construct() {
        parent::__construct('Portifolio');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/portifolio/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));

        // $this->add(array(
            // 'name' => 'area',
            // 'attributes' => array(
                // 'type' => 'text',
                // 'class' => 'campos',
            // ),
            // 'options' => array(
                // 'label' => 'Área de atuaçao',
            // ),
        // ));

        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type' => 'textarea',
                // 'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Descrição',
            ),
        ));
		
        // $this->add(array(
            // 'name' => 'telefone',
            // 'attributes' => array(
                // 'type' => 'text',
                // 'class' => 'campos',
            // ),
            // 'options' => array(
                // 'label' => 'Telefones',
            // ),
        // ));		
       

        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                'label' => 'Imagem',
            ),
        ));

        $this->add(array(
            'name' => 'iframe',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Link do Blog',
            ),
        ));		

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

}