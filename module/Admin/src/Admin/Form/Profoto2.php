<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Profoto2 extends Form {

    public function __construct($proid) {
        parent::__construct('Profoto');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/profoto/editarlegenda');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'produto_id',
            'attributes' => array(
                'type' => 'hidden',
                'value' => $proid
            ),
        ));

        $this->add(array(
            'name' => 'legenda',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Legenda',
            ),
        ));

        $this->add(array(
            'name' => 'ordem',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Ordem',
            ),
        ));


//         $this->add(array(
//             'name' => 'imagem',
//             'attributes' => array(
//                 'type' => 'hidden',
//             ),
           
//         ));

        $cat[2] = 'Não';
        $cat[1] = 'Sim';

        $principal = new Element\Select('principal');
        $principal->setLabel('Essa será a foto principal?');
        $principal->setValueOptions($cat);
        $this->add($principal);


        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar'
            ),
        ));
    }

}
