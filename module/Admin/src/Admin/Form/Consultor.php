<?php
namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class Consultor extends Form
{
    public function __construct()
    {
        parent::__construct('consultor');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/consultor/registrar');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Descrição',
            ),
        ));
        
        $this->add(array(
            'name' => 'usuario_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'foto',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Foto',
            ),
        ));
        $this->add(array(
            'name' => 'twitter',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Twitter',
            ),
        ));
        

        

        $this->add(array(
            'name' => 'facebook',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Facebook',
            ),
        ));

        $this->add(array(
            'name' => 'skype',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Skype',
            ),
        ));
        $this->add(array(
            'name' => 'celular',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Celular',
            ),
        ));

        $this->add(array(
            'name' => 'creci',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Creci',
            ),
        ));
        $this->add(array(
            'name' => 'estado_id',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Estado',
            ),
        ));
        $this->add(array(
            'name' => 'cidade_id',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Cidade',
            ),
        ));
        

      
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}