<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;
 
class Pedido extends Form
{
    public function __construct($clientes)
    {
        parent::__construct('Pedido');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/pedido/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        //Preenche o array de categorias recebido pela classe para o exibir o select
        $cli;
        foreach ($clientes as $cliente) {
          $cli[$cliente['id']] = $cliente['nome'];
        }
        //Preenche o select com o array de categorias
        $select = new Element\Select('cliente_id');
        $select->setLabel('Cliente');
        $select->setValueOptions($cli);
        $this->add($select);
        
        $this->add(array(
            'name' => 'data',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Data',
            ),
        ));
        $this->add(array(
            'name' => 'valor_produtos',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Valor Produtos',
            ),
        ));
		
	
        $this->add(array(
            'name' => 'valor_frete',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Frete',
            ),
        ));
        
       
        $cat[1]='Pendente';
        $cat[2]='Saiu para Entrega';
        $cat[3]='Entregue';
        $status = new Element\Select('status');
        $status->setLabel('Qual é o status do pedido?');
        $status->setValueOptions($cat);
        $this->add($status);
        
        $pag[0]='Não Pago';
        $pag[1]='Pago';
        $pago = new Element\Select('pago');
        $pago->setLabel('Pedido ja foi pago?');
        $pago->setValueOptions($pag);
        $this->add($pago);
        
        
        /*$this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Texto do Pedido',
            ),
        ));*/
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}