<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Lista extends Form {

    public function __construct($casal,$produtos) {
        parent::__construct('lista');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/lista/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $cat;
        foreach ($casal as $categoria) {
          $cat[$categoria['id']] = $categoria['nome'];
        }
        // Preenche o select com o array de casal
        $select = new Element\Select('noivo_id');
        $select->setLabel('Selecione o Casal');
        $select->setValueOptions($cat);
        $this->add($select);

        // $cat2;
        // foreach ($produtos as $categoria2) {
          // $cat2[$categoria2['id']] = $categoria2['nome'];
        // }
        // Preenche o select com o array de casal
        // $select2 = new Element\Select('produto_id');
        // $select2->setLabel('Selecione o Produto');
        // $select2->setValueOptions($cat2);
        // $this->add($select2);		
		
        // $this->add(array(
            // 'name' => 'nome',
            // 'attributes' => array(
                // 'type' => 'text',
            // ),
            // 'options' => array(
                // 'label' => 'Nome',
            // ),
        // ));


        /* $this->add(array(
          'name' => 'description',
          'attributes' => array(
          'type'  => 'textarea',
          ),
          'options' => array(
          'label' => 'Texto do categoria',
          ),
          )); */
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Prosseguir',
                'id' => 'submitbutton',
            ),
        ));
    }

}
