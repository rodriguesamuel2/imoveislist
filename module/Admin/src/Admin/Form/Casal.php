<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;
 
class Casal extends Form
{
    public function __construct()
    {
        parent::__construct('Casal');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/casal/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Nome do Casal',
            ),
        ));
        
        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Foto do Casal',
            ),
        )); 
        
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Email para contato',
            ),
        ));
        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Telefone para contato',
            ),
        ));
       
        $cat[1]='Sim';
        $cat[0]='Não';
        $promocao = new Element\Select('ativo');
        $promocao->setLabel('Casal está ativo?');
        $promocao->setValueOptions($cat);
        $this->add($promocao);
        
        $this->add(array(
            'name' => 'data',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Data do Casamento',
            ),
        ));
		
        $this->add(array(
            'name' => 'endereco',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Endereço do Local de Casamento',
            ),
        ));
		
        $this->add(array(
            'name' => 'cidade',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Cidade do Local de Casamento',
            ),
        ));
		
        $estado["AC"]='AC';
        $estado["AL"]='AL';
        $estado["AP"]='AP';
        $estado["AM"]='AM';
        $estado["BA"]='BA';
        $estado["CE"]='CE';
        $estado["DF"]='DF';
        $estado["ES"]='ES';
        $estado["GO"]='GO';
        $estado["MA"]='MA';
        $estado["MS"]='MS';
        $estado["MT"]='MT';
        $estado["MG"]='MG';
        $estado["PA"]='PA';
        $estado["PB"]='PB';
        $estado["PR"]='PR';
        $estado["PE"]='PE';
        $estado["PI"]='PI';
        $estado["RJ"]='RJ';
        $estado["RN"]='RN';
        $estado["RS"]='RS';
        $estado["RO"]='RO';
        $estado["RR"]='RR';
        $estado["SC"]='SC';
        $estado["SP"]='SP';
        $estado["SE"]='SE';
        $estado["TO"]='TO';

        $estado2 = new Element\Select('estado');
        $estado2->setLabel('Estado onde vão se casar');
        $estado2->setValueOptions($estado);
        $this->add($estado2);		
		
        // $this->add(array(
            // 'name' => 'estado',
            // 'attributes' => array(
                // 'type'  => 'text',
                // 'class'  => 'campos',
            // ),
            // 'options' => array(
                // 'label' => 'Estado do Local de Casamento',
            // ),
        // ));
		
        $this->add(array(
            'name' => 'senha',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Senha',
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}