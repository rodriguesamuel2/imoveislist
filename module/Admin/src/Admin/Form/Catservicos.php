<?php
namespace Admin\Form;
 
use Zend\Form\Element; 
use Zend\Form\Form;
 
class Catservicos extends Form
{
    public function __construct($marcas)
    {
        parent::__construct('catservicos');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/catservicos/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
		
		
        // Preenche o array de categorias recebido pela classe para o exibir o select
        // $cat;
        // foreach ($marcas as $categoria) {
          // $cat[$categoria['id']] = $categoria['nome'];
        // }
        // Preenche o select com o array de categorias
        // $select = new Element\Select('marca_id');
        // $select->setLabel('Categoria Primária');
        // $select->setValueOptions($cat);
        // $this->add($select);
		
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}