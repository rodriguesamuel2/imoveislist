<?php
namespace Admin\Form;
 
use Zend\Form\Form;
 
class Faq extends Form
{
    public function __construct()
    {
        parent::__construct('faq');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/faq/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Titulo da Dica',
            ),
        ));
        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type'  => 'textarea',
                'class'=>'campos texto'
            ),
            'options' => array(
                'label' => 'Texto da Dica',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}