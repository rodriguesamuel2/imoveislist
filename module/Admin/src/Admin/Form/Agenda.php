<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Agenda extends Form {

    public function __construct() {
        parent::__construct('agenda');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/agenda/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
		
//        $this->add(array(
//            'name' => 'data',
//            'attributes' => array(
//                'type' => 'date',
//            ),
//            'options' => array(
//                'label' => 'Data do Evento',
//            ),
//        ));		
		
        // $this->add(array(
            // 'name' => 'dia',
            // 'attributes' => array(
                // 'type' => 'text',
            // ),
            // 'options' => array(
                // 'label' => 'Dia do Evento',
            // ),
        // ));

		// $mes[1]='Janeiro';
        // $mes[2]='Fevereiro';
        // $mes[3]='Março';
        // $mes[4]='Abril';
        // $mes[5]='Maio';
        // $mes[6]='Junho';
        // $mes[7]='Julho';
        // $mes[8]='Agosto';
        // $mes[9]='Setembro';
        // $mes[10]='Outubro';
        // $mes[11]='Novembro';
        // $mes[12]='Dezembro';
        // $status = new Element\Select('mes');
        // $status->setLabel('Mês do Trabalho:');
        // $status->setValueOptions($mes);
        // $this->add($status);				
		
        // $y = date('Y');
        // $x = '2000';
        // while ($x <= $y) {	
			// $ano[$x] = $x;	
            // $x++;	
        // }
//         $anos = new Element\Select('ano');
//         $anos->setLabel('Ano do Trabalho:');
//         $anos->setValueOptions($ano);
//         $this->add($anos);		
		
        $this->add(array(
            'name' => 'localidade',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Endereco',
            ),
        ));		
        
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));	
        
        $this->add(array(
            'name' => 'facebook',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Facebook',
            ),
        ));		
        
        $this->add(array(
            'name' => 'instagram',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'instagram',
            ),
        ));		
        
        $this->add(array(
            'name' => 'in',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'in',
            ),
        ));		
		
        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type' => 'text',
                 'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Telefone',
            ),
        ));
		
		
//        $cat[1]='Sim';
//        $cat[2]='Não';
//        $promocao = new Element\Select('ativo');
//        $promocao->setLabel('Evento vai aparecer no site');
//        $promocao->setValueOptions($cat);
//        $this->add($promocao);
	 
//        $oferta = new Element\Select('academico');
//        $oferta->setLabel('Esse evento é ACADÊMICO?');
//        $oferta->setValueOptions($cat);
//        $this->add($oferta);       


        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar'
            ),
        ));
    }

}