<?php
namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class Usuario extends Form
{
    public function __construct()
    {
        parent::__construct('categoria');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/usuario/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
        
        $this->add(array(
            'name' => 'nomeusuario',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Login',
            ),
        ));
        $this->add(array(
            'name' => 'senha',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Senha',
            ),
        ));
         $this->add(array(
            'name' => 'senhanova',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'valid',
            'attributes' => array(
                'type'  => 'hidden',
            ),
            'options' => array(
                'value' => '1',
            ),
        ));

        $this->add(array(
            'name' => 'role',
            'attributes' => array(
                'type'  => 'hidden',
            ),
            'options' => array(
                'value' => 'usuario',
            ),
        ));



        $this->add(array(
            'name' => 'newsletter',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'newsletter',
            ),
        ));

        $this->add(array(
            'name' => 'tipo_id',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'tipo',
            ),
        ));
        $this->add(array(
            'name' => 'plano_id',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'plano',
            ),
        ));
        $this->add(array(
            'name' => 'imobiliaria_id',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'plano',
            ),
        ));

        /*$role['admin']='admin';
        $role['usuario']='usuario';
        $role['visitante']='visitante';
        $permissao = new Element\Select('role');
        $permissao->setLabel('Qual é a permissão desse usuário?');
        $permissao->setValueOptions($role);
        $this->add($permissao);*/

        /*$this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Texto do categoria',
            ),
        ));*/
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}