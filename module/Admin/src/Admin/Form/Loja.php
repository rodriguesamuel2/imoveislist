<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Loja extends Form {

    public function __construct() {
        parent::__construct('loja');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/loja/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Nome da Loja',
            ),
        ));

        $this->add(array(
            'name' => 'rua',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Rua',
            ),
        ));

        $this->add(array(
            'name' => 'bairro',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Bairro',
            ),
        ));

        $this->add(array(
            'name' => 'numero',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Número',
            ),
        ));
        
        
        $this->add(array(
            'name' => 'cidade',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Cidade',
            ),
        ));
        
        $this->add(array(
            'name' => 'estado',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Estado',
            ),
        ));
        $this->add(array(
            'name' => 'cep',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Cep',
            ),
        ));
        $this->add(array(
            'name' => 'telefone1',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Telefone 1',
            ),
        ));
        $this->add(array(
            'name' => 'telefone2',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Telefone 2',
            ),
        ));
        $this->add(array(
            'name' => 'telefone3',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Telefone 3',
            ),
        ));
        
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        $this->add(array(
            'name' => 'mapa',
            'attributes' => array(
                'type' => 'text',
                'class'=>'campos texto'
            ),
            'options' => array(
                'label' => 'Google Maps',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar'
            ),
        ));
    }

}