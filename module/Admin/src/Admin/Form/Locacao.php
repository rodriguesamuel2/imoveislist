<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Locacao extends Form {

    public function __construct() {
        parent::__construct('Locacao');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/locacao/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'cliente_id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Nome do Estágio',
            ),
        ));

        $this->add(array(
            'name' => 'codigo',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Código do Estágio',
            ),
        ));

        $estado["AC"] = 'AC';
        $estado["AL"] = 'AL';
        $estado["AP"] = 'AP';
        $estado["AM"] = 'AM';
        $estado["BA"] = 'BA';
        $estado["CE"] = 'CE';
        $estado["DF"] = 'DF';
        $estado["ES"] = 'ES';
        $estado["GO"] = 'GO';
        $estado["MA"] = 'MA';
        $estado["MS"] = 'MS';
        $estado["MT"] = 'MT';
        $estado["MG"] = 'MG';
        $estado["PA"] = 'PA';
        $estado["PB"] = 'PB';
        $estado["PR"] = 'PR';
        $estado["PE"] = 'PE';
        $estado["PI"] = 'PI';
        $estado["RJ"] = 'RJ';
        $estado["RN"] = 'RN';
        $estado["RS"] = 'RS';
        $estado["RO"] = 'RO';
        $estado["RR"] = 'RR';
        $estado["SC"] = 'SC';
        $estado["SP"] = 'SP';
        $estado["SE"] = 'SE';
        $estado["TO"] = 'TO';

        $estado2 = new Element\Select('estado');
        $estado2->setLabel('Estado do Estágio');
        $estado2->setValueOptions($estado);
        $this->add($estado2);

        $cat123["FUNDAMENTAL INCOMPLETO"] = 'FUNDAMENTAL INCOMPLETO';
        $cat123["FUNDAMENTAL COMPLETO"] = 'FUNDAMENTAL COMPLETO';
        $cat123["MÉDIO INCOMPLETO"] = 'MÉDIO INCOMPLETO';
        $cat123["MÉDIO COMPLETO"] = 'MÉDIO COMPLETO';
        $cat123["SUPERIOR INCOMPLETO"] = 'SUPERIOR INCOMPLETO';
        $cat123["SUPERIOR COMPLETO"] = 'SUPERIOR COMPLETO';
        $promocao = new Element\Select('nivel');
        $promocao->setLabel('Nivel Acadêmico do Estágio:');
        $promocao->setValueOptions($cat123);
        $this->add($promocao);

        $this->add(array(
            'name' => 'cidade',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Cidade do Estágio',
            ),
        ));

        $this->add(array(
            'name' => 'area',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Area Profissional do Estágio',
            ),
        ));

        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Descrição do Estágio',
            ),
        ));



        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                'label' => 'Imagem do Estágio',
            ),
        ));

        $cat2['1'] = 'Sim';
        $cat2['2'] = 'Não';
        $ativo = new Element\Select('ativo');
        $ativo->setLabel('Vaga pode aparecer no site?');
        $ativo->setValueOptions($cat2);
        $this->add($ativo);



        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

}
