<?php
namespace Admin\Form;
 
use Zend\Form\Form;
 
class Parceiro extends Form
{
    public function __construct()
    {
        parent::__construct('Parceiro');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/parceiro/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));
        
        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Logomarca (70x70 pixels)',
            ),
        )); 
        
       
        $this->add(array(
            'name' => 'site',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Link do Site',
            ),
        ));
        
       
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}