<?php
namespace Admin\Form;
 
use Zend\Form\Form;
 
class Link extends Form
{
    public function __construct()
    {
        parent::__construct('link');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/link/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Descricao',
            ),
        ));
        
        $this->add(array(
            'name' => 'link',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Link',
            ),
        ));
       
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}