<?php

namespace Admin\Form;
 
use Zend\Form\Element;
use Zend\Form\Form;
 
class Imoveldifatual extends Form
{
    public function __construct()
    {
        parent::__construct('Imoveldifatual');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('action',str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/imoveldifatual/save');
        
         $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
         
         $this->add(array(
            'name' => 'imovel_id',
            'attributes' => array(
                'type'  => 'hidden',
              
            ),
        ));

           $this->add(array(
            'name' => 'diferencial_id',
            'attributes' => array(
                'type'  => 'hidden',
                
            ),
        ));
         
       
         
         
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar'
            ),
        )); 
    }
}