<?php

namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;
 
class Marca extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Marca');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('action',str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/marca/save');
        
         $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
         
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
                'class'  => 'campos',
            ),
            'options' => array(
                'label' => 'Nome da Marca',
            ),
        ));
 
//        $cat[1]='LADO DIREITO DO HOME';
//        $cat[2]='FINAL DO HOME';
//        $cat[3]='PÁGINA DE SERVIÇOS';
//        $cat[4]='PÁGINA DE IMÓVEIS';
//        $cat[5]='PÁGINA DE VAGAS DE EMPREGO';
//        $cat[6]='PÁGINA DE TUTORIAS';
//        $promocao = new Element\Select('local');
//        $promocao->setLabel('Local onde a publicidade aparecerá:');
//        $promocao->setValueOptions($cat);
//        $this->add($promocao);  
         
        $this->add(array(
            'name' => 'file',
            'attributes' => array(
                'type'  => 'file',
            ),
            'options' => array(
                'label' => 'Imagem',
            ),
        )); 
         
         
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar'
            ),
        )); 
    }
}