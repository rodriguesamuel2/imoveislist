<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;
 
class Pagina extends Form
{
    public function __construct()
    {
        parent::__construct('pagina');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action',str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']). '/admin/pagina/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
//        $this->add(array(
//            'name' => 'cliente_id',
//            'attributes' => array(
//                'type'  => 'hidden',
//            ),
//        ));
        
        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type'  => 'text',
                'class'=>'campos'
            ),
            'options' => array(
                'label' => 'Nome do Formando',
            ),
        ));
		
//        $estado["AC"]='AC';
//        $estado["AL"]='AL';
//        $estado["AP"]='AP';
//        $estado["AM"]='AM';
//        $estado["BA"]='BA';
//        $estado["CE"]='CE';
//        $estado["DF"]='DF';
//        $estado["ES"]='ES';
//        $estado["GO"]='GO';
//        $estado["MA"]='MA';
//        $estado["MS"]='MS';
//        $estado["MT"]='MT';
//        $estado["MG"]='MG';
//        $estado["PA"]='PA';
//        $estado["PB"]='PB';
//        $estado["PR"]='PR';
//        $estado["PE"]='PE';
//        $estado["PI"]='PI';
//        $estado["RJ"]='RJ';
//        $estado["RN"]='RN';
//        $estado["RS"]='RS';
//        $estado["RO"]='RO';
//        $estado["RR"]='RR';
//        $estado["SC"]='SC';
//        $estado["SP"]='SP';
//        $estado["SE"]='SE';
//        $estado["TO"]='TO';
//
//        $estado2 = new Element\Select('estado');
//        $estado2->setLabel('Estado do Formando');
//        $estado2->setValueOptions($estado);
//        $this->add($estado2);	

//        $this->add(array(
//            'name' => 'cidade',
//            'attributes' => array(
//                'type'  => 'text',
//                'class'=>'campos'
//            ),
//            'options' => array(
//                'label' => 'Cidade do Formando',
//            ),
//        ));	

//        $this->add(array(
//            'name' => 'facul',
//            'attributes' => array(
//                'type'  => 'text',
//                'class'=>'campos'
//            ),
//            'options' => array(
//                'label' => 'Faculdade/Universidade do Formando',
//            ),
//        ));	

//        $this->add(array(
//            'name' => 'curso',
//            'attributes' => array(
//                'type'  => 'text',
//                'class'=>'campos'
//            ),
//            'options' => array(
//                'label' => 'Curso do Formando',
//            ),
//        ));	

//        $this->add(array(
//            'name' => 'ano',
//            'attributes' => array(
//                'type'  => 'text',
//            ),
//            'options' => array(
//                'label' => 'Ano de Conclusão do Curso',
//            ),
//        ));		
		
        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type'  => 'textarea',
                'class'=>'campos texto'
            ),
            'options' => array(
                'label' => 'Texto do Formando',
            ),
        ));
        
//        $cat2['1']='Sim';
//        $cat2['2']='Não';
//        $ativo = new Element\Select('ativo');
//        $ativo->setLabel('Formando pode aparecer no site?');
//        $ativo->setValueOptions($cat2);
//        $this->add($ativo);
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}