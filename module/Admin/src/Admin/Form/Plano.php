<?php
namespace Admin\Form;
 
use Zend\Form\Form;
 
class Plano extends Form
{
    public function __construct()
    {
        parent::__construct('plano');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/plano/save');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
//        $this->add(array(
//            'name' => 'descricao',
//            'attributes' => array(
//                'type'  => 'text',
//            ),
//            'options' => array(
//                'label' => 'Descrição',
//            ),
//        ));
        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Descrição do plano',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}