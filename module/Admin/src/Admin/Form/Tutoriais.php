<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Tutoriais extends Form {

    public function __construct($cat) {
        parent::__construct('Tutoriais');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/tutoriais/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Nome da Tutoria',
            ),
        ));
		
        $estado["AC"]='AC';
        $estado["AL"]='AL';
        $estado["AP"]='AP';
        $estado["AM"]='AM';
        $estado["BA"]='BA';
        $estado["CE"]='CE';
        $estado["DF"]='DF';
        $estado["ES"]='ES';
        $estado["GO"]='GO';
        $estado["MA"]='MA';
        $estado["MS"]='MS';
        $estado["MT"]='MT';
        $estado["MG"]='MG';
        $estado["PA"]='PA';
        $estado["PB"]='PB';
        $estado["PR"]='PR';
        $estado["PE"]='PE';
        $estado["PI"]='PI';
        $estado["RJ"]='RJ';
        $estado["RN"]='RN';
        $estado["RS"]='RS';
        $estado["RO"]='RO';
        $estado["RR"]='RR';
        $estado["SC"]='SC';
        $estado["SP"]='SP';
        $estado["SE"]='SE';
        $estado["TO"]='TO';

        $estado2 = new Element\Select('estado');
        $estado2->setLabel('Estado da Tutoria');
        $estado2->setValueOptions($estado);
        $this->add($estado2);		
		
        $subcategorias2;
        foreach ($cat as $subcategoria) {
          $subcategorias2[$subcategoria['id']] = $subcategoria['nome'];
        }
        // Preenche o select com o array de categorias
        $subcategoria = new Element\Select('nivel');
        $subcategoria->setLabel('Disciplina da Tutoria');
        $subcategoria->setValueOptions($subcategorias2);
        $this->add($subcategoria);		

        $this->add(array(
            'name' => 'cidade',
            'attributes' => array(
                'type' => 'text',
                'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Cidade da Tutoria',
            ),
        ));	

        $this->add(array(
            'name' => 'date',
            'attributes' => array(
                'type' => 'date',
            ),
            'options' => array(
                'label' => 'Data da Tutoria',
            ),
        ));		
		
        $cat123["MATUTINO"]='MATUTINO';
        $cat123["VESPERTINO"]='VESPERTINO';
        $cat123["NOTURNO"]='NOTURNO';
        $promocao = new Element\Select('area');
        $promocao->setLabel('Horário da Tutoria:');
        $promocao->setValueOptions($cat123);
        $this->add($promocao);	

        $this->add(array(
            'name' => 'hora',
            'attributes' => array(
                'type' => 'time',
            ),
            'options' => array(
                'label' => 'Hora da Tutoria',
            ),
        ));		
		
        $this->add(array(
            'name' => 'codigo',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Faixa de Preço por Aula da Tutoria',
            ),
        ));

        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type' => 'textarea',
                // 'class' => 'campos',
            ),
            'options' => array(
                'label' => 'Descrição da Tutoria',
            ),
        ));		
       

        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type' => 'file',
            ),
            'options' => array(
                'label' => 'Imagem da Tutoria',
            ),
        ));
		

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

}