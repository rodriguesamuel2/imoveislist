<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Novidade;
use Admin\Form\Novidade as NovidadeForm;

/**
 * Controlador que gerencia as faqs do site
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class NovidadeController extends ActionController {

    /**
     * Mostra as faqs cadastrados
     * @return void
     */
    public function indexAction() {
        $Novidade = $this->getTable('Admin\Model\Novidade');
        $sql = $Novidade->getSql();
        $select = $sql->select();

        $faqtorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $faqtor = new Paginator($faqtorAdapter);
        $faqtor->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'novidades' => $faqtor
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $form = new NovidadeForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Novidade = new Novidade;
            $form->setInputFilter($Novidade->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $Novidade->setData($data);

                $saved = $this->getTable('Admin\Model\Novidade')->save($Novidade);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/novidade');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Novidade = $this->getTable('Admin\Model\Novidade')->get($id);
            $form->bind($Novidade);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Novidade')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/novidade');
    }

}