<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Depoimento;
use Admin\Form\Depoimento as DepoimentoForm;
/**
 * Controlador que gerencia as depoimentos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class DepoimentoController extends ActionController {

    /**
     * Mostra as depoimentos cadastrados
     * @return void
     */
    public function indexAction() {
        $depoimento = $this->getTable('Admin\Model\Depoimento');
        $sql = $depoimento->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        
        $view = new ViewModel(array(
            'depoimentos' => $paginator
        ));
        return $view;
    }
    
    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction()
    {
        $form = new DepoimentoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $depoimento = new Depoimento;
            $form->setInputFilter($depoimento->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $depoimento->setData($data);
                
                $saved =  $this->getTable('Admin\Model\Depoimento')->save($depoimento);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/depoimento');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {    
            $depoimento = $this->getTable('Admin\Model\Depoimento')->get($id);
            $form->bind($depoimento);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
       
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }
     
    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $this->getTable('Admin\Model\Depoimento')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/depoimento');
    }

}