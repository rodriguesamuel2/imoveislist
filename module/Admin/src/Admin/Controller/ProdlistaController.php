<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Produto;
use Admin\Model\Casal;
use Admin\Model\Prodlista;
use Admin\Form\Prodlista as ProdlistaForm;

/**
 * Controlador que gerencia as Listas dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ProdlistaController extends ActionController {

    /**
     * Mostra as Listas cadastrados
     * @return void
     */
    public function indexAction() {
        
        $casalid = (int) $this->params()->fromRoute('casalid', 0);
        if ($casalid > 0) {
            $lista = $this->getTable('Admin\Model\Lista');
            $sql = $lista->getSql();
            $select = $sql->select()
                    ->where('noivo_id = ' . $casalid);

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

			$casais = $this->getTable('Admin\Model\Casal')->fetchAll(null,null,null,null,array('nome asc'))->toArray();	
			$produtos = $this->getTable('Admin\Model\Produto')->fetchAll(null,null,null,null,array('nome asc'))->toArray();				

            $view = new ViewModel(array(
                'listas' => $paginator,
                'produtos' => $produtos,
                'casais' => $casais,
                'casalid' => $casalid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $casalid = (int) $this->params()->fromRoute('casalid', 0);
		$produtos = $this->getTable('Admin\Model\Produto')->fetchAll()->toArray();
		$listas = $this->getTable('Admin\Model\Lista')->fetchAll()->toArray();
		
        $form = new ProdlistaForm($casalid,$produtos,$listas);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Lista = new Prodlista;
            $form->setInputFilter($Lista->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $Lista->setData($data);

                $saved = $this->getTable('Admin\Model\Prodlista')->save($Lista);
				echo "<script>alert('Produto Adicionado com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";

            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $profoto = $this->getTable('Admin\Model\Prodlista')->get($id);
            $form->bind($profoto);
            // $form->
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
		
		$produtos = $this->getTable('Admin\Model\Produto')->fetchAll(null,null,null,null,array('nome asc'))->toArray();				
		
        $lista = $this->getTable('Admin\Model\Lista');
        $sql = $lista->getSql();
        $select = $sql->select()
                ->where('noivo_id = ' . $casalid);

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));		

        $view = new ViewModel(array(
            'listas' => $paginator,
            'produtos' => $produtos,
            'form' => $form,
            'casalid' => $casalid,
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $casalid = (int) $this->params()->fromRoute('casalid', 0);
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Prodlista')->delete($id);
		echo "<script>alert('Produto Excluido com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
        // return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/prodlista/index/casalid/'.$casalid.'');
    }

}