<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Imobiliaria;
use Admin\Form\Imobiliaria as ImobiliariaForm;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Imobiliarias
 *
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ImobiliariaController extends ActionController {

    /**
     * Cria uma nova conta
     * @return void
     */
    public function registrarAction() {

        $session = new Container('userDados');

        $form = new ImobiliariaForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $imobiliaria = new Imobiliaria();
            $form->setInputFilter($imobiliaria->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('logomarca');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            //set data post and file ...    
            $form->setData($data);

            //echo "chegou aqui";

            if ($form->isValid()) {

                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {

                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    //$data['logomarca'] = $newName;
                    $data['usuario_id'] = $session->usuario->id;
                    //$data['produto_id'] = $proid;
                    $imobiliaria->setData($data);

                    $saved = $this->getTable('Admin\Model\Imobiliaria')->save($imobiliaria);

                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/cadastrar');
                } else {

                    $adapter->setDestination('./public_html/data/imobiliarias');
                    $destination = './public_html/data/imobiliarias';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['logomarca'] = $newName;
                    $data['usuario_id'] = $session->usuario->id;
                    //$data['produto_id'] = $proid;
                    $imobiliaria->setData($data);

                    $saved = $this->getTable('Admin\Model\Imobiliaria')->save($imobiliaria);

//                    $adapter->setDestination('./public_html/data/produtos');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];
//                        var_dump($target);

                        $imobiliaria->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/cadastrar');
                        //echo 'Profile Name ' . $prologomarca->nome . ' upload ' . $prologomarca->file;
                    }
                }
            }
        } else {
//            echo "Esse formulário não é valido";
        }

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('estados')
                ->order('nome asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $estados = $statement->execute();

        $view = new ViewModel(array(
            'form' => $form,
            'estados' => $estados
        ));
        return $view;
    }

    /**
     * Mostra as Imobiliarias cadastrados
     * @return void
     */
    public function atualizarAction() {

        $session = new Container('userDados');

        /* pesquisando qual o id do consultor para atualizar as informações */
        $imobiliaria1 = $this->getTable('Admin\Model\Imobiliaria');
        $sql1 = $imobiliaria1->getSql();
        $select1 = $sql1->select()
                ->where(array("usuario_id=" . $session->usuario->id . ""));

        $statement = $sql1->prepareStatementForSqlObject($select1);
        $imobiliariadados = $statement->execute();

        $imobiliaria_id = 0;

        foreach ($imobiliariadados as $con) {
            $imobiliaria_id = $con['id'];
        }



        $form = new ImobiliariaForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $imobiliaria = new Imobiliaria();
            $form->setInputFilter($imobiliaria->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('logomarca');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            //echo "chegou aqui";

            if ($form->isValid()) {




                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {

                    $data = $form->getData();

//                    echo $data['logomarca'];die();

                    unset($data['btnsalvar']);
                    unset($data['logomarca']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
//                    $data['logomarca'] = $data['logomarca'];
                    $data['usuario_id'] = $session->usuario->id;
                    $data['id'] = $imobiliaria_id;
                    //$data['produto_id'] = $proid;
                    $imobiliaria->setData($data);

                    try {
                        $saved = $this->getTable('Admin\Model\Imobiliaria')->save($imobiliaria);
                    } catch (\Exception $e) {
                        
                    }
                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario/perfil');
                } else {

                    $adapter->setDestination('./public_html/data/imobiliarias');
                    $destination = './public_html/data/imobiliarias';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();
                    unset($data['btnsalvar']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['logomarca'] = $newName;
                    $data['usuario_id'] = $session->usuario->id;
                    $data['id'] = $imobiliaria_id;
                    //$data['produto_id'] = $proid;
                    $imobiliaria->setData($data);

                    try {
                        $saved = $this->getTable('Admin\Model\Imobiliaria')->save($imobiliaria);
                    } catch (\Exception $e) {
                        
                    }
//                    $adapter->setDestination('./public_html/data/produtos');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];
//                        var_dump($target);

                        $imobiliaria->exchangeArray($form->getData());

//                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/cadastrar');
                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario/perfil');
                        //echo 'Profile Name ' . $prologomarca->nome . ' upload ' . $prologomarca->file;
                    }
                }
            }
        }
    }

}
