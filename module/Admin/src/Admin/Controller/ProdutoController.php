<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Produto;
use Admin\Model\Subcategoria;
use Admin\Model\Marca;
use Admin\Form\Produto as ProdutoForm;
use Zend\Db\Sql\Sql;

/**
 * Controlador que gerencia a produtos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ProdutoController extends ActionController {

    /**
     * Mostra as produtos cadastrados
     * @return void
     */
    public function indexAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        
        
        $q="";
        $request = $this->getRequest();
        if ($request->isPost()) {
            $q = " and nome LIKE '%" . $_POST['q'] . "%'";
        }        
        
        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('produtos')
                ->join('subcategorias', 'produtos.subcategoria_id = subcategorias.id', array('cnome'=>'nome'))
                ->where('produtos.id=produtos.id ' . $q)
                ->order('embalagem asc');
                // ->join('marcas', 'produtos.marca_id = marcas.id', array('mnome'=>'nome'))
                 

        //$statement = $sql->prepareStatementForSqlObject($select);
        //$results = $statement->execute();
        

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        
        $view = new ViewModel(array(
            'produtos' => $paginator,
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        /* $categoria = $this->getTable('Admin\Model\Produto');
          $sql = $categoria->getSql();
          $select_categoria = $sql->select(); */

        $marcas = $this->getTable('Admin\Model\Marca')->fetchAll()->toArray();
        
        $subcategorias = $this->getTable('Admin\Model\Subcategoria')->fetchAll()->toArray();

        $form = new ProdutoForm($marcas, $subcategorias);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $produto = new Produto;
            $form->setInputFilter($produto->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                $data['data'] = date('Y-m-d H:i:s');
                $data['preco'] = str_replace(",",".",$data['preco']);
                $data['preco_promocao'] = str_replace(",",".",$data['preco_promocao']);				
                $produto->setData($data);

                $saved = $this->getTable('Admin\Model\Produto')->save($produto);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/produto');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $produto = $this->getTable('Admin\Model\Produto')->get($id);
            $form->bind($produto);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
         $view = new ViewModel(array(
            'form' => $form
        ));

         return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Produto')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/produto');
    }

}