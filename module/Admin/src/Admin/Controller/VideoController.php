<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Video;
use Admin\Form\Video as VideoForm;
/**
 * Controlador que gerencia as videos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class VideoController extends ActionController {

    /**
     * Mostra as videos cadastrados
     * @return void
     */
    public function indexAction() {
        $video = $this->getTable('Admin\Model\Video');
        $sql = $video->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        
        $view = new ViewModel(array(
            'videos' => $paginator
        ));
        return $view;
    }
    
    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction()
    {
        $form = new VideoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $video = new Video;
            $form->setInputFilter($video->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $video->setData($data);
                
                $saved =  $this->getTable('Admin\Model\Video')->save($video);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/video');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {    
            $video = $this->getTable('Admin\Model\Video')->get($id);
            $form->bind($video);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
       
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }
     
    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $this->getTable('Admin\Model\Video')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/video');
    }

}