<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Post;
use Admin\Form\Post as PostForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Posts dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class PostController extends ActionController {

    /**
     * Mostra as Posts cadastrados
     * @return void
     */
    public function indexAction() {


        $Post = $this->getTable('Admin\Model\Post');
        $sql = $Post->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'posts' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
//        $translator = $this->getServiceLocator()->get('translator');
//        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new PostForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Post = new Post;
            $form->setInputFilter($Post->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($data);

            if ($form->isValid()) {

                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {

                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('file' => $error));
                } else {
                    
                    $adapter->setDestination('./public_html/data/posts');
                    $destination = './public_html/data/posts';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));
                    
                    $data = $form->getData();
                    $data['imagem'] = $newName;
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $Post->setData($data);

                    $saved = $this->getTable('Admin\Model\Post')->save($Post);
                    $adapter->setDestination('./public_html/data/posts');
                    if ($adapter->receive($File['name'])) {
                        
                        $file = $adapter->getFilter('File\Rename')->getFile();

                        $target = $file[0]['target'];
                        
                        $Post->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                }


                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/post');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Post = $this->getTable('Admin\Model\Post')->get($id);
            $form->bind($Post);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $Post = $this->getTable('Admin\Model\Post')->get($id);
        
         unlink('./public_html/data/posts/'.$Post->imagem);

        $this->getTable('Admin\Model\Post')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/post');
    }

}