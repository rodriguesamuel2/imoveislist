<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Versao;
use Admin\Form\Versao as VersaoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as versoes dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class VersaoController extends ActionController {

    /**
     * Mostra as versoes cadastrados
     * @return void
     */
    public function indexAction() {
        $motoid = (int) $this->params()->fromRoute('motoid', 0);
        if ($motoid > 0) {
            $versao = $this->getTable('Admin\Model\Versao');
            $sql = $versao->getSql();
            $select = $sql->select()
                    //->join('planos', 'versoes.plano_id = planos.id', array('plano'=>'descricao'))
                    ->where('moto_id = ' . $motoid);

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


            $view = new ViewModel(array(
                'versoes' => $paginator,
                'motoid' => $motoid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $motoid = (int) $this->params()->fromRoute('motoid', 0);
        //$form = new VersaoForm($motoid);
        $request = $this->getRequest();
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        //$planos = $this->getTable('Admin\Model\Plano')->fetchAll()->toArray();
        $form = new VersaoForm($motoid);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $versao = new Versao;
            $form->setInputFilter($versao->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $versao->setData($data);

                $saved = $this->getTable('Admin\Model\Versao')->save($versao);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/versao/index/motoid/'. $data['moto_id']);
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $versao = $this->getTable('Admin\Model\Versao')->get($id);
            $form->bind($versao);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $motoid = (int) $this->params()->fromRoute('motoid', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Versao')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/versao/index/motoid/' . $motoid);
    }

}