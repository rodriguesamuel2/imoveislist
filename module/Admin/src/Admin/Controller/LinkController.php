<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Link;
use Admin\Form\Link as LinkForm;

/**
 * Controlador que gerencia as links do site
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class LinkController extends ActionController {

    /**
     * Mostra as links cadastrados
     * @return void
     */
    public function indexAction() {
        $link = $this->getTable('Admin\Model\Link');
        $sql = $link->getSql();
        $select = $sql->select();

        $linktorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $linktor = new Paginator($linktorAdapter);
        $linktor->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'links' => $linktor
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $form = new LinkForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $link = new Link;
            $form->setInputFilter($link->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $link->setData($data);

                $saved = $this->getTable('Admin\Model\Link')->save($link);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/link');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $link = $this->getTable('Admin\Model\Link')->get($id);
            $form->bind($link);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Link')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/link');
    }

}