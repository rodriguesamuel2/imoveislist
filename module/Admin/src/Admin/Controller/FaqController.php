<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Faq;
use Admin\Form\Faq as FaqForm;

/**
 * Controlador que gerencia as faqs do site
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class FaqController extends ActionController {

    /**
     * Mostra as faqs cadastrados
     * @return void
     */
    public function indexAction() {
        $faq = $this->getTable('Admin\Model\Faq');
        $sql = $faq->getSql();
        $select = $sql->select();

        $faqtorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $faqtor = new Paginator($faqtorAdapter);
        $faqtor->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'faqs' => $faqtor
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $form = new FaqForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $faq = new Faq;
            $form->setInputFilter($faq->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $faq->setData($data);

                $saved = $this->getTable('Admin\Model\Faq')->save($faq);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/faq');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $faq = $this->getTable('Admin\Model\Faq')->get($id);
            $form->bind($faq);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Faq')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/faq');
    }

}