<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Agenfoto;
use Admin\Form\Agenfoto as AgenfotoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as profotos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class AgenfotoController extends ActionController {

    /**
     * Mostra as profotos cadastrados
     * @return void
     */
    public function indexAction() {
        $agenid = (int) $this->params()->fromRoute('agenid', 0);
        if ($agenid > 0) {
            $Agenfoto = $this->getTable('Admin\Model\Agenfoto');
            $sql = $Agenfoto->getSql();
            $select = $sql->select()
                    ->where('agenda_id = ' . $agenid);

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


            $view = new ViewModel(array(
                'agenfotos' => $paginator,
                'agenid' => $agenid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $agenid = (int) $this->params()->fromRoute('agenid', 0);
        $form = new AgenfotoForm($agenid);
        $request = $this->getRequest();
        if ($request->isPost()) {

            $Agenfoto = new Agenfoto();
            $form->setInputFilter($Agenfoto->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('imagem' => $error));
                } else {
                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['imagem'] = $File['name'];
                    //$data['produto_id'] = $proid;
                    $Agenfoto->setData($data);

                    $saved = $this->getTable('Admin\Model\Agenfoto')->save($Agenfoto);

                    $adapter->setDestination('./public_html/data/agenda');
                    if ($adapter->receive($File['name'])) {
                        $Agenfoto->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/agenfoto/index/agenid/' . $data['agenda_id']);
                        //echo 'Profile Name ' . $profoto->nome . ' upload ' . $profoto->file;
                    }
                }
            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Agenfoto = $this->getTable('Admin\Model\Agenfoto')->get($id);
            $form->bind($Agenfoto);
            //$form->
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $agenid = (int) $this->params()->fromRoute('agenid', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Agenfoto')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/agenfoto/index/agenid/' . $agenid);
    }

}
