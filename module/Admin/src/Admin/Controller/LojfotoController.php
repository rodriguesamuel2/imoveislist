<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Lojfoto;
use Admin\Form\Lojfoto as LojfotoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as lojfotos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class LojfotoController extends ActionController {

    /**
     * Mostra as lojfotos cadastrados
     * @return void
     */
    public function indexAction() {
        $lojid = (int) $this->params()->fromRoute('lojid', 0);
        if ($lojid > 0) {
            $lojfoto = $this->getTable('Admin\Model\Lojfoto');
            $sql = $lojfoto->getSql();
            $select = $sql->select()->where('loja_id = ' . $lojid);

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

            $view = new ViewModel(array(
                'lojfotos' => $paginator,
                'lojid' => $lojid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $lojid = (int) $this->params()->fromRoute('lojid', 0);
        $form = new LojfotoForm($lojid);
        $request = $this->getRequest();
        //if ($lojid > 0) {
        if ($request->isPost()) {

            $lojfoto = new Lojfoto();
            $form->setInputFilter($lojfoto->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('imagem' => $error));
                } else {
                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['imagem'] = $File['name'];
                    //$data['loja_id'] = $lojid;
                    $lojfoto->setData($data);

                    $saved = $this->getTable('Admin\Model\Lojfoto')->save($lojfoto);

                    $adapter->setDestination('./public_html/data/lojas');
                    if ($adapter->receive($File['name'])) {
                        $lojfoto->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/lojfoto/index/lojid/' . $data['loja_id']);
                        //echo 'Profile Name ' . $lojfoto->nome . ' upload ' . $lojfoto->file;
                    }
                }
            }
        }
        //}

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $lojfoto = $this->getTable('Admin\Model\Lojfoto')->get($id);
            $form->bind($lojfoto);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $lojid = (int) $this->params()->fromRoute('lojid', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Lojfoto')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/lojfoto/index/lojid/' . $lojid);
    }

}