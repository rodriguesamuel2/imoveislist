<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Locacao;
use Admin\Form\Locacao as LocacaoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Portifolios dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class LocacaoController extends ActionController {

    /**
     * Mostra as Portifolios cadastrados
     * @return void
     */
    public function indexAction() {

        $Locacao = $this->getTable('Admin\Model\Locacao');
        $sql = $Locacao->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $view = new ViewModel(array(
            'locacoes' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new LocacaoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

//            $registro_atual = $this->getTable('Admin\Model\Locacao')->get($_POST['id']);

            $Locacao = new Locacao;
            $form->setInputFilter($Locacao->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($data);
            
//            die("alert(".trim($_FILES["imagem"]["tmp_name"]).") - ".$registro_atual->imagem);
           
            if (isset($File['name'])&&$File['name']!="") {
                
                if ($form->isValid()) {

                    //HEY, FORM IS VALID...
                    $size = new Size(array('min' => 0)); //minimum bytes filesize

                    $adapter = new \Zend\File\Transfer\Adapter\Http();
                    //validator can be more than one...
                    //$adapter->setValidators(array($size), $File['name']);

                    if (!$adapter->isValid()) {

                        $dataError = $adapter->getMessages();
                        $error = array();
                        foreach ($dataError as $key => $row) {
                            $error[] = $row;
                        } //set formElementErrors
                        $form->setMessages(array('file' => $error));
                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/locacao/save/id/' . $_POST['id']);
                    } else {
                        $data = $form->getData();
                        $data['imagem'] = $File['name'];
                        unset($data['submit']);
                        //$data['post_date'] = date('Y-m-d H:i:s');
                        $Locacao->setData($data);

                        $saved = $this->getTable('Admin\Model\Locacao')->save($Locacao);
                        $adapter->setDestination('./public_html/data/locacoes');
                        if ($adapter->receive($File['name'])) {
                            $Locacao->exchangeArray($form->getData());

                            //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                        }
                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/locacao');
                    }
                }
            } else {
                $data = $request->getPost()->toArray();
//                $data = $form->getData();
//                $data['imagem'] = $File['name'];
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $Locacao->setData($data);
                $saved = $this->getTable('Admin\Model\Locacao')->save($Locacao);
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/locacao');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Locacao = $this->getTable('Admin\Model\Locacao')->get($id);
//            $Locacao->imagem = str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) ."/data/locacoes/".$Locacao->imagem;
            $form->bind($Locacao);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $Locacao = $this->getTable('Admin\Model\Locacao')->get($id);

        // unlink('./public_html/data/portifolios/'.$Portifolio->imagem);

        $this->getTable('Admin\Model\Locacao')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/locacao');
    }

}
