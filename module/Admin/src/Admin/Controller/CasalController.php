<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Casal;
use Admin\Form\Casal as CasalForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Clientes dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class CasalController extends ActionController {

    /**
     * Mostra as Clientes cadastrados
     * @return void
     */
    public function indexAction() {


        $Casal = $this->getTable('Admin\Model\Casal');
        $sql = $Casal->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'casais' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new CasalForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Casal = new Casal;
            $form->setInputFilter($Casal->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($data);

            if ($form->isValid()) {

                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {

                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('file' => $error));
                } else {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $Casal->setData($data);




                    $saved = $this->getTable('Admin\Model\Casal')->save($Casal);
                    $adapter->setDestination('./public_html/data/casais');
                    if ($adapter->receive($File['name'])) {
                        $Casal->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                }


                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/casal');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Casal = $this->getTable('Admin\Model\Casal')->get($id);
            $form->bind($Casal);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $Casal = $this->getTable('Admin\Model\Casal')->get($id);
        
        unlink('./public_html/data/clientes/'.$Casal->imagem);

        $this->getTable('Admin\Model\Casal')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/casal');
    }

}