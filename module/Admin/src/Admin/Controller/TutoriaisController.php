<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Disciplina;
use Admin\Model\Tutoriais;
use Admin\Form\Tutoriais as TutoriaisForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Portifolios dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class TutoriaisController extends ActionController {

    /**
     * Mostra as Portifolios cadastrados
     * @return void
     */
    public function indexAction() {

        $Tutoriais = $this->getTable('Admin\Model\Tutoriais');
        $sql = $Tutoriais->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $view = new ViewModel(array(
            'portifolios' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
		
	$cat = $this->getTable('Admin\Model\Disciplina')->fetchAll()->toArray();		
		
        $form = new TutoriaisForm($cat);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Tutoriais = new Tutoriais;
            $form->setInputFilter($Tutoriais->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($data);

            if ($form->isValid()) {

                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
					$data1 = $data['date'];
					$data['date'] = date('d/m/Y', strtotime($data1));
                    $Tutoriais->setData($data);




                    $saved = $this->getTable('Admin\Model\Tutoriais')->save($Tutoriais);
                    $adapter->setDestination('./public_html/data/tutoriais');
                    if ($adapter->receive($File['name'])) {
                        $Tutoriais->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                } else {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
					$data1 = $data['date'];
					$data['date'] = date('d/m/Y', strtotime($data1));
                    $Tutoriais->setData($data);




                    $saved = $this->getTable('Admin\Model\Tutoriais')->save($Tutoriais);
                    $adapter->setDestination('./public_html/data/tutoriais');
                    if ($adapter->receive($File['name'])) {
                        $Tutoriais->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                }


                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/tutoriais');
            }
        
		
		}
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Tutoriais = $this->getTable('Admin\Model\Tutoriais')->get($id);
            $form->bind($Tutoriais);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $Tutoriais = $this->getTable('Admin\Model\Tutoriais')->get($id);
        
        // unlink('./public_html/data/portifolios/'.$Portifolio->imagem);

        $this->getTable('Admin\Model\Tutoriais')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/tutoriais');
    }

}