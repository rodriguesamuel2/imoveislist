<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Marca;
use Admin\Model\Disciplina;
use Admin\Form\Disciplina as DisciplinaForm;

/**
 * Controlador que gerencia as categorias dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class DisciplinaController extends ActionController {

    /**
     * Mostra as categorias cadastrados
     * @return void
     */
    public function indexAction() {
        
    
        $Disciplina = $this->getTable('Admin\Model\Disciplina');
        $sql = $Disciplina->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

       
        $view = new ViewModel(array(
            'categorias' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
		
        $marcas = $this->getTable('Admin\Model\Marca')->fetchAll()->toArray();		
		
        $form = new DisciplinaForm($marcas);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Disciplina = new Disciplina;
            $form->setInputFilter($Disciplina->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $Disciplina->setData($data);

                $saved = $this->getTable('Admin\Model\Disciplina')->save($Disciplina);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/disciplina');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Disciplina = $this->getTable('Admin\Model\Disciplina')->get($id);
            $form->bind($Disciplina);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Disciplina')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/disciplina');
    }

}