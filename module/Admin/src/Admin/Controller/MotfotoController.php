<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Motfoto;
use Admin\Form\Motfoto as MotfotoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as motfotos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class MotfotoController extends ActionController {

    /**
     * Mostra as motfotos cadastrados
     * @return void
     */
    public function indexAction() {
        $motid = (int) $this->params()->fromRoute('motoid', 0);
        if ($motid > 0) {
            $motfoto = $this->getTable('Admin\Model\Motfoto');
            $sql = $motfoto->getSql();
            $select = $sql->select()->where('moto_id = ' . $motid);

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

            $view = new ViewModel(array(
                'motfotos' => $paginator,
                'motid' => $motid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $motid = (int) $this->params()->fromRoute('motoid', 0);
        $form = new MotfotoForm($motid);
        $request = $this->getRequest();
        //if ($motid > 0) {
        if ($request->isPost()) {

            $motfoto = new Motfoto();
            $form->setInputFilter($motfoto->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('imagem' => $error));
                } else {
                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['imagem'] = $File['name'];
                    //$data['moto_id'] = $motid;
                    $motfoto->setData($data);

                    $saved = $this->getTable('Admin\Model\Motfoto')->save($motfoto);

                    $adapter->setDestination('./public_html/data/motos');
                    if ($adapter->receive($File['name'])) {
                        $motfoto->exchangeArray($form->getData());

                        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/motfoto/index/motoid/' . $data['moto_id']);
                        //echo 'Profile Name ' . $motfoto->nome . ' upload ' . $motfoto->file;
                    }
                }
            }
        }
        //}

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $motfoto = $this->getTable('Admin\Model\Motfoto')->get($id);
            $form->bind($motfoto);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $motid = (int) $this->params()->fromRoute('motoid', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Motfoto')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/motfoto/index/motoid/' . $motid);
    }

}