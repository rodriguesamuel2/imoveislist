<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Pagfoto;
use Admin\Form\Pagfoto as PagfotoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as pagfotos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class PagfotoController extends ActionController {

    /**
     * Mostra as pagfotos cadastrados
     * @return void
     */
    public function indexAction() {
        $pagid = (int) $this->params()->fromRoute('pagid', 0);
        if ($pagid > 0) {
            $pagfoto = $this->getTable('Admin\Model\Pagfoto');
            $sql = $pagfoto->getSql();
            $select = $sql->select()
                    ->where('pagina_id = ' . $pagid);

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

            $view = new ViewModel(array(
                'pagfotos' => $paginator,
                'pagid' => $pagid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $pagid = (int) $this->params()->fromRoute('pagid', 0);
        $form = new PagfotoForm($pagid);
        $request = $this->getRequest();
        if ($request->isPost()) {

            $pagfoto = new Pagfoto();
            $form->setInputFilter($pagfoto->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('imagem' => $error));
                } else {

                    $adapter->setDestination('./public_html/data/paginas');
                    $destination = './public_html/data/paginas';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['imagem'] = $newName;
                    //$data['pagina_id'] = $pagid;
                    $pagfoto->setData($data);

                    $saved = $this->getTable('Admin\Model\Pagfoto')->save($pagfoto);

                    $adapter->setDestination('./public_html/data/paginas');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();

                        $target = $file[0]['target'];

//                        var_dump($target);

                        $pagfoto->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/pagfoto/index/pagid/' . $data['pagina_id']);
                        //echo 'Profile Name ' . $pagfoto->nome . ' upload ' . $pagfoto->file;
                    }
                }
            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $pagfoto = $this->getTable('Admin\Model\Pagfoto')->get($id);
            $form->bind($pagfoto);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $pagid = (int) $this->params()->fromRoute('pagid', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $Pagfoto = $this->getTable('Admin\Model\Pagfoto')->get($id);

        unlink('./public_html/data/paginas/' . $Pagfoto->imagem);

        $this->getTable('Admin\Model\Pagfoto')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/pagfoto/index/pagid/' . $pagid);
    }

}
