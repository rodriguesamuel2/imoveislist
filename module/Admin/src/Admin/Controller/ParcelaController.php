<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Parcela;
use Admin\Form\Parcela as ParcelaForm;
use Zend\Validator\File\Size;
use Zend\Db\Sql\Sql;

/**
 * Controlador que gerencia as parcelas dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ParcelaController extends ActionController {

    /**
     * Mostra as parcelas cadastrados
     * @return void
     */
    public function indexAction() {
        $motoid = (int) $this->params()->fromRoute('motoid', 0);
        if ($motoid > 0) {
            $parcela = $this->getTable('Admin\Model\Parcela');
            $sql = $parcela->getSql();
            $select = $sql->select()
                    ->join('planos', 'parcelas.plano_id = planos.id', array('plano' => 'descricao'))
                    ->join('versoes', 'parcelas.versao_id = versoes.id', array('versao' => 'nome'))
                    ->where('parcelas.moto_id = ' . $motoid)
                    ->order(array('versao_id', 'qtde'));

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


            $view = new ViewModel(array(
                'parcelas' => $paginator,
                'motoid' => $motoid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $motoid = (int) $this->params()->fromRoute('motoid', 0);
        $id = (int) $this->params()->fromRoute('id', 0);
        //$form = new ParcelaForm($motoid);
        $request = $this->getRequest();
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $planos = $this->getTable('Admin\Model\Plano')->fetchAll()->toArray();
        $versoes = $this->getTable('Admin\Model\Versao')->fetchAll(null,"versoes.moto_id=".$motoid)->toArray();        
        
        $form = new ParcelaForm($motoid, $planos, $versoes);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $parcela = new Parcela;
            $form->setInputFilter($parcela->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $parcela->setData($data);

                $saved = $this->getTable('Admin\Model\Parcela')->save($parcela);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/parcela/index/motoid/' . $data['moto_id']);
            }
        }

        if ($id > 0) {
            $parcela = $this->getTable('Admin\Model\Parcela')->get($id);
            $form->bind($parcela);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $motoid = (int) $this->params()->fromRoute('motoid', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Parcela')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/parcela/index/motoid/' . $motoid);
    }

}