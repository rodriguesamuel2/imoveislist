<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Moto;
use Admin\Form\Moto as MotoForm;

/**
 * Controlador que gerencia as motos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class MotoController extends ActionController {

    /**
     * Mostra as motos cadastrados
     * @return void
     */
    public function indexAction() {
            
        $moto = $this->getTable('Admin\Model\Moto');
        $sql = $moto->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
       
        $view = new ViewModel(array(
            'motos' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new MotoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $moto = new Moto;
            $form->setInputFilter($moto->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $moto->setData($data);

                $saved = $this->getTable('Admin\Model\Moto')->save($moto);
                return $this->redirect()->toUrl('/admin/moto');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $moto = $this->getTable('Admin\Model\Moto')->get($id);
            $form->bind($moto);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Moto')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/moto');
    }

}