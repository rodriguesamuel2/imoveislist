<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Loja;
//use Admin\Model\Subcategoria;
//use Admin\Model\Marca;
use Admin\Form\Loja as LojaForm;
use Zend\Db\Sql\Sql;

/**
 * Controlador que gerencia a lojas dos lojas
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class LojaController extends ActionController {

    /**
     * Mostra as lojas cadastrados
     * @return void
     */
    public function indexAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->columns(array('id', 'nome', 'rua', 'bairro', 'numero', 'cidade', 'estado'))
                ->from('lojas');

        //$statement = $sql->prepareStatementForSqlObject($select);
        //$results = $statement->execute();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'lojas' => $paginator,
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        /* $categoria = $this->getTable('Admin\Model\Loja');
          $sql = $categoria->getSql();
          $select_categoria = $sql->select(); */

        //$marcas = $this->getTable('Admin\Model\Marca')->fetchAll()->toArray();
        //$subcategorias = $this->getTable('Admin\Model\Subcategoria')->fetchAll()->toArray();

        $form = new LojaForm();
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $loja = new Loja;
            $form->setInputFilter($loja->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['data'] = date('Y-m-d H:i:s');
                $loja->setData($data);

                $saved = $this->getTable('Admin\Model\Loja')->save($loja);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/loja');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $loja = $this->getTable('Admin\Model\Loja')->get($id);
            $form->bind($loja);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));

        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Loja')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/loja');
    }

}