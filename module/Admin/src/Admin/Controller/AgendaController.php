<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Agenda;
use Admin\Form\Agenda as AgendaForm;
use Zend\Db\Sql\Sql;

/**
 * Controlador que gerencia a produtos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class AgendaController extends ActionController {

    /**
     * Mostra as produtos cadastrados
     * @return void
     */
    public function indexAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);      
        
        $select = $sql->select()
            ->from('agenda');

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        
        $view = new ViewModel(array(
            'eventos' => $paginator,
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        /* $categoria = $this->getTable('Admin\Model\Produto');
          $sql = $categoria->getSql();
          $select_categoria = $sql->select(); */

        $form = new AgendaForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Agenda = new Agenda;
            $form->setInputFilter($Agenda->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
//                $data1 = $data['data'];
//                $data['data'] = date('d/m/Y', strtotime($data1));
//                $data['dia'] = substr($data['data'],0,2);
//                $data['mes'] = substr($data['data'],3,2);
//                $data['ano'] = substr($data['data'],6,4);
                // $data['preco'] = str_replace(",",".",$data['preco']);
                // $data['preco_promocao'] = str_replace(",",".",$data['preco_promocao']);				
                $Agenda->setData($data);

                $saved = $this->getTable('Admin\Model\Agenda')->save($Agenda);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/agenda');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Agenda = $this->getTable('Admin\Model\Agenda')->get($id);
            $form->bind($Agenda);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
         $view = new ViewModel(array(
            'form' => $form
        ));

         return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Agenda')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/agenda');
    }

}