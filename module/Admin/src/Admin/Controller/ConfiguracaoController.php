<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Configuracao;
use Admin\Form\Configuracao as ConfiguracaoForm;
/**
 * Controlador que gerencia as configuracoes dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ConfiguracaoController extends ActionController {

    /**
     * Mostra as configuracoes cadastrados
     * @return void
     */
    public function indexAction() {
        $configuracao = $this->getTable('Admin\Model\Configuracao');
        $sql = $configuracao->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        
        $view = new ViewModel(array(
            'configuracoes' => $paginator
        ));
        return $view;
    }
    
    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction()
    {
        $form = new ConfiguracaoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $configuracao = new Configuracao;
            $form->setInputFilter($configuracao->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $configuracao->setData($data);
                
                $saved =  $this->getTable('Admin\Model\Configuracao')->save($configuracao);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/configuracao');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {    
            $configuracao = $this->getTable('Admin\Model\Configuracao')->get($id);
            $form->bind($configuracao);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
       
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }
     
    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $this->getTable('Admin\Model\Configuracao')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/configuracao');
    }

}