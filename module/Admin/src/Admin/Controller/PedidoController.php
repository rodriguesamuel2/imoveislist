<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Pedido;
use Admin\Model\Cliente;
use Admin\Form\Pedido as PedidoForm;
use Zend\Db\Sql\Sql;

/**
 * Controlador que gerencia a pedidos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class PedidoController extends ActionController {

    /**
     * Mostra as pedidos cadastrados
     * @return void
     */
    public function indexAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->columns(array('id', 'data', 'frete', 'valor_produtos', 'valor_frete', 'status', 'pago', 'clientes_id', 'casal_id'))
                ->from('pedidos')
                ->join('clientes', 'clientes.id = pedidos.clientes_id', array('cnome' => 'nome'));

        //$statement = $sql->prepareStatementForSqlObject($select);
        //$results = $statement->execute();
        //$clientes = $this->getTable('Admin\Model\Cliente')->fetchAll()->toArray();

		$casais = $this->getTable('Admin\Model\Casal')->fetchAll(null,null,null,null,array('nome asc'))->toArray();
		
        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'pedidos' => $paginator,
            'casais' => $casais,
                //'clientes' => $clientes
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        /* $categoria = $this->getTable('Admin\Model\Cliente');
          $sql = $categoria->getSql();
          $select_categoria = $sql->select(); */

        $clientes = $this->getTable('Admin\Model\Cliente')->fetchAll()->toArray();

        $form = new PedidoForm($clientes);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pedido = new Pedido;
            $form->setInputFilter($pedido->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $pedido->setData($data);

                $saved = $this->getTable('Admin\Model\Pedido')->save($pedido);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/pedido');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $pedido = $this->getTable('Admin\Model\Pedido')->get($id);
            $form->bind($pedido);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->columns(array('id', 'produto_id', 'pedido_id','quantidade'))
                ->from('pedido_produtos')
                ->join('produtos', 'pedido_produtos.produto_id = produtos.id', array('nome','preco','preco_promocao','promocao'))
                ->where(array('pedido_produtos.pedido_id='.$id));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $produtos = $statement->execute();

        $view = new ViewModel(array(
            'form' => $form,
            'produtos' => $produtos,
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Pedido')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/pedido');
    }

}