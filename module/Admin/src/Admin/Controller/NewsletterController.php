<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Newsletter;
use Admin\Form\Newsletter as NewsletterForm;
use Zend\Db\Sql\Sql;

/**
 * Controlador que gerencia as newslleters dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class NewsletterController extends ActionController {

    /**
     * Mostra as newslleters cadastrados
     * @return void
     */
    public function indexAction() {


        $newsletter = $this->getTable('Admin\Model\Newsletter');
        $sql = $newsletter->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'newsletters' => $paginator
        ));
        return $view;
    }
    public function listaAction() {        $newsletter = $this->getTable('Admin\Model\Newsletter');        $sql = $newsletter->getSql();        $select = $sql->select();        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);        $paginator = new Paginator($paginatorAdapter);        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));        $view = new ViewModel(array(            'newsletters' => $paginator        ));        return $view;    }
    /**
     * Mostra as newslleters cadastrados
     * @return void
     */
    public function sortearAction() {


//        $adapter = $this->getServiceLocator()->get('DbAdapter');
//        $sql = new Sql($adapter);
//        $select = $sql->select()
//                ->columns(array('id', 'nome', 'email', 'telefone'))
//                ->from('newsletter')
//                ->order("rand()")
//                ->limit(3);
//
//        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
//        $paginator = new Paginator($paginatorAdapter);
//        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $adapter1 = $this->getServiceLocator()->get('DbAdapter');
        $sql1 = new Sql($adapter1);

        
        $sql1 = "select * from newsletter order by rand();";
        $statement = $adapter1->query($sql1);
        $select = $statement->execute();


        $view = new ViewModel(array(
            'newsletters' => $select
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new NewsletterForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $newsletter = new Newsletter;
            $form->setInputFilter($newsletter->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $newsletter->setData($data);

                $saved = $this->getTable('Admin\Model\Newsletter')->save($newsletter);
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/newsletter');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $newsletter = $this->getTable('Admin\Model\Newsletter')->get($id);
            $form->bind($newsletter);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Newsletter')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/newsletter');
    }

}