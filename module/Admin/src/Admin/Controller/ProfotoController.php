<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Profoto;
use Admin\Form\Profoto as ProfotoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as profotos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ProfotoController extends ActionController {

    /**
     * Mostra as profotos cadastrados
     * @return void
     */
    public function indexAction() {
        $proid = (int) $this->params()->fromRoute('proid', 0);
        if ($proid > 0) {
            $profoto = $this->getTable('Admin\Model\Profoto');
            $sql = $profoto->getSql();
            $select = $sql->select()
                    ->where('produto_id = ' . $proid);

            $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
            $paginator = new Paginator($paginatorAdapter);
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


            $view = new ViewModel(array(
                'profotos' => $paginator,
                'proid' => $proid
            ));
            return $view;
        }
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $proid = (int) $this->params()->fromRoute('proid', 0);
        $form = new ProfotoForm($proid);
        $request = $this->getRequest();
        if ($request->isPost()) {

            $profoto = new Profoto();
            $form->setInputFilter($profoto->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('imagem' => $error));
                } else {

                    $adapter->setDestination('./public_html/data/produtos');
                    $destination = './public_html/data/produtos';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['imagem'] = $newName;
                    //$data['produto_id'] = $proid;
                    $profoto->setData($data);

                    $saved = $this->getTable('Admin\Model\Profoto')->save($profoto);

//                    $adapter->setDestination('./public_html/data/produtos');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];
//                        var_dump($target);

                        $profoto->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/profoto/index/proid/' . $data['produto_id']);
                        //echo 'Profile Name ' . $profoto->nome . ' upload ' . $profoto->file;
                    }
                }
            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $profoto = $this->getTable('Admin\Model\Profoto')->get($id);
            $form->bind($profoto);
            //$form->
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $proid = (int) $this->params()->fromRoute('proid', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $Profoto = $this->getTable('Admin\Model\Profoto')->get($id);

        unlink('./public_html/data/produtos/'.$Profoto->imagem);
        
        $this->getTable('Admin\Model\Profoto')->delete($id);
        
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/profoto/index/proid/' . $proid);
    }

}
