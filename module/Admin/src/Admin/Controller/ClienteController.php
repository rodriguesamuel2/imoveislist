<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Cliente;
use Admin\Form\Cliente as ClienteForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Clientes dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ClienteController extends ActionController {

    /**
     * Mostra as Clientes cadastrados
     * @return void
     */
    public function indexAction() {


        $Cliente = $this->getTable('Admin\Model\Cliente');
        $sql = $Cliente->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'clientes' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new ClienteForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Cliente = new Cliente;
            $form->setInputFilter($Cliente->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($data);

            if ($form->isValid()) {

                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {

                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('file' => $error));
                } else {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $Cliente->setData($data);




                    $saved = $this->getTable('Admin\Model\Cliente')->save($Cliente);
                    $adapter->setDestination('./public_html/data/clientes');
                    if ($adapter->receive($File['name'])) {
                        $Cliente->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                }


                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/cliente');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Cliente = $this->getTable('Admin\Model\Cliente')->get($id);
            $form->bind($Cliente);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $Cliente = $this->getTable('Admin\Model\Cliente')->get($id);
        
//        unlink('./public_html/data/clientes/'.$Cliente->imagem);

        $this->getTable('Admin\Model\Cliente')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/cliente');
    }

}