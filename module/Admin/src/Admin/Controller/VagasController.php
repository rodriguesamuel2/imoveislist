<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Catvagas;
use Admin\Model\Vagas;
use Admin\Form\Vagas as VagasForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Portifolios dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class VagasController extends ActionController {

    /**
     * Mostra as Portifolios cadastrados
     * @return void
     */
    public function indexAction() {

        $Vagas = $this->getTable('Admin\Model\Vagas');
        $sql = $Vagas->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $view = new ViewModel(array(
            'portifolios' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
		
	$cat = $this->getTable('Admin\Model\Catvagas')->fetchAll()->toArray();		
		
        $form = new VagasForm($cat);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Vagas = new Vagas;
            $form->setInputFilter($Vagas->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($data);

            if ($form->isValid()) {

                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $Vagas->setData($data);




                    $saved = $this->getTable('Admin\Model\Vagas')->save($Vagas);
                    $adapter->setDestination('./public_html/data/vagas');
                    if ($adapter->receive($File['name'])) {
                        $Vagas->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                } else {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $Vagas->setData($data);




                    $saved = $this->getTable('Admin\Model\Vagas')->save($Vagas);
                    $adapter->setDestination('./public_html/data/vagas');
                    if ($adapter->receive($File['name'])) {
                        $Vagas->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                }


                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/vagas');
            }
        
		
		}
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Vagas = $this->getTable('Admin\Model\Vagas')->get($id);
            $form->bind($Vagas);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $Vagas = $this->getTable('Admin\Model\Vagas')->get($id);
        
        // unlink('./public_html/data/portifolios/'.$Portifolio->imagem);

        $this->getTable('Admin\Model\Vagas')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/vagas');
    }

}