<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Consultor;
use Admin\Form\Consultor as ConsultorForm;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Consultors dos produtos
 *
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ConsultorController extends ActionController {

    /**
     * Cria uma nova conta
     * @return void
     */
    public function registrarAction() {

        $session = new Container('userDados');



        $form = new ConsultorForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $consultor = new Consultor();
            $form->setInputFilter($consultor->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('foto');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            //echo "chegou aqui";

            if ($form->isValid()) {




                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {



                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    //$data['foto'] = $newName;
                    $data['usuario_id'] = $session->usuario->id;
                    //$data['produto_id'] = $proid;
                    $consultor->setData($data);

                    $saved = $this->getTable('Admin\Model\Consultor')->save($consultor);

                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/cadastrar');
                } else {



                    $adapter->setDestination('./public_html/data/consultores');
                    $destination = './public_html/data/consultores';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['foto'] = $newName;
                    $data['usuario_id'] = $session->usuario->id;
                    //$data['produto_id'] = $proid;
                    $consultor->setData($data);

                    $saved = $this->getTable('Admin\Model\Consultor')->save($consultor);

//                    $adapter->setDestination('./public_html/data/produtos');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];
//                        var_dump($target);

                        $consultor->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/cadastrar');
                        //echo 'Profile Name ' . $profoto->nome . ' upload ' . $profoto->file;
                    }
                }
            }
        } else {
//            echo "Esse formulário não é valido";
        }

         $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('estados')
                ->order('nome asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $estados = $statement->execute();



        $view = new ViewModel(array(
            'form' => $form,
            'estados' => $estados
        ));
        return $view;
    }

    /**
     * Mostra as Consultors cadastrados
     * @return void
     */
    public function atualizarAction() {

        $session = new Container('userDados');

        /* pesquisando qual o id do consultor para atualizar as informações */
        $consultor1 = $this->getTable('Admin\Model\Consultor');
        $sql1 = $consultor1->getSql();
        $select1 = $sql1->select()
                ->where(array("usuario_id=" . $session->usuario->id . ""));

        $statement = $sql1->prepareStatementForSqlObject($select1);
        $consultordados = $statement->execute();

        $consultor_id = 0;

        foreach ($consultordados as $con) {
            $consultor_id = $con['id'];
        }



        $form = new ConsultorForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $consultor = new Consultor();
            $form->setInputFilter($consultor->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('foto');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            //echo "chegou aqui";

            if ($form->isValid()) {




                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {

                    $data = $form->getData();

//                    echo $data['foto'];die();

                    unset($data['btnsalvar']);
                    unset($data['foto']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
//                    $data['foto'] = $data['foto'];
                    $data['usuario_id'] = $session->usuario->id;
                    $data['id'] = $consultor_id;
                    //$data['produto_id'] = $proid;
                    $consultor->setData($data);

                    try {
                        $saved = $this->getTable('Admin\Model\Consultor')->save($consultor);
                    } catch (\Exception $e) {
                        
                    }

                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario/perfil');
                } else {



                    $adapter->setDestination('./public_html/data/consultores');
                    $destination = './public_html/data/consultores';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();
                    unset($data['btnsalvar']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['foto'] = $newName;
                    $data['usuario_id'] = $session->usuario->id;
                    $data['id'] = $consultor_id;
                    //$data['produto_id'] = $proid;
                    $consultor->setData($data);

                    $saved = $this->getTable('Admin\Model\Consultor')->save($consultor);

//                    $adapter->setDestination('./public_html/data/produtos');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];
//                        var_dump($target);

                        $consultor->exchangeArray($form->getData());


//                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/cadastrar');
                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario/perfil');
                        //echo 'Profile Name ' . $profoto->nome . ' upload ' . $profoto->file;
                    }
                }
            }
//            else{
//                echo "entro aqui";die();
//            }
        }
    }

    /**
     * Mostra as Consultors cadastrados
     * @return void
     */
    public function indexAction() {
        $consultor = $this->getTable('Admin\Model\Consultor');
        $sql = $consultor->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'Consultors' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new ConsultorForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $consultor = new Consultor;
            $form->setInputFilter($consultor->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                $data['senha'] = md5($data['senha']);
                $consultor->setData($data);

                $saved = $this->getTable('Admin\Model\Consultor')->save($consultor);
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/Consultor');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $consultor = $this->getTable('Admin\Model\Consultor')->get($id);
            $form->bind($consultor);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Consultor')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/Consultor');
    }

}
