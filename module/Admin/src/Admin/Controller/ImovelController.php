<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Imovel;
use Admin\Model\Imovelfoto;
use Admin\Model\Imoveldifatual;
use Admin\Form\Imovel as ImovelForm;
use Admin\Form\Imovelfoto as ImovelfotoForm;
use Admin\Form\Imoveldifatual as ImoveldifatualForm;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;
use Zend\Validator\File\Size;
use Zend\Db\Sql\Expression;
use \Exception;

/**
 * Controlador que gerencia as Imovels dos produtos
 *
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ImovelController extends ActionController {

    /**
     * Cria uma nova conta
     * @return void
     */
    public function cadastrarAction() {

        $session = new Container('userDados');

        $imovel_id = 0;

        $form = new ImovelForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            /* Salvando dados do imóvel */
            $Imovel = new Imovel;
            $form->setInputFilter($Imovel->getInputFilter());
            $teste = $request->getPost();

            $teste2['titulo'] = $teste['titulo'];
            $teste2['plano_id'] = $teste['plano_id'];
            $teste2['preco'] = $teste['preco'];
            $teste2['descricao'] = $teste['descricao'];
            $teste2['estado_id'] = $teste['estado_id'];
            $teste2['cidade_id'] = $teste['cidade_id'];
            $teste2['bairro'] = $teste['bairro'];
            $teste2['tipo_id'] = $teste['tipo_id'];
            $teste2['finalidade'] = $teste['finalidade'];
            $teste2['quartos'] = $teste['quartos'];
            $teste2['banheiros'] = $teste['banheiros'];
            $teste2['area_total'] = $teste['area_total'];
            $teste2['area_construida'] = $teste['area_construida'];
            $teste2['garagem'] = $teste['garagem'];
            $teste2['suites'] = $teste['suites'];
            $teste2['condominio'] = $teste['condominio'];

            $form->setData($teste2);

            if ($form->isValid()) {
                /* echo "chegou aqui"; */
                $data = $form->getData();
                unset($data['submit']);
                //unset($data['confirmarsenha']);
                $data['usuario_id'] = $session->usuario->id;
                $data['preco'] = str_replace(",", ".", $data['preco']);
                $data['area_total'] = str_replace(",", ".", $data['area_total']);
                $data['area_construida'] = str_replace(",", ".", $data['area_construida']);
                $data['condominio'] = str_replace(",", ".", $data['condominio']);
                $Imovel->setData($data);

                $saved = $this->getTable('Admin\Model\Imovel')->save($Imovel);
                /* return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/obrigado'); */
            } else {
                /* print_r($form->getMessages()); */
            }/* Fim do Salvando dados do imóvel */



            /* Início pegando o id do ultimo imovel cadastrado pelo usuario */

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select = $sql->select()
                    ->columns(array('id' => new Expression('max(id)')))
                    ->from('imoveis')
                    ->where(array("usuario_id=" . $session->usuario->id . ""));
            $statement = $sql->prepareStatementForSqlObject($select);
            $ultimoimovel = $statement->execute();

            foreach ($ultimoimovel as $uimovel) {
                $imovel_id = $uimovel['id'];
            }
            /* Final pegando o id do ultimo imovel cadastrado pelo usuario */

            /* Início salvando as fotos do imóvel */
            $form2 = new ImovelfotoForm($imovel_id);
            $request = $this->getRequest();
            $cont = 1;
            //$contador = 1;
            # Declarar a variável "i", que será nosso controle, para que não haja
            # loop infinito.
            $i = 0;

            # Faz um loop conforme o número de arquivos
            foreach ($_FILES["fotos"]["error"] as $key => $error) {

                # Definir a pasta que os arquivos serão "upados".
                // $pasta = "fotos/_" . $_FILES["uploads"]["name"][$i];
                # Aqui, você faz o upload do arquivo, utilizando a classe que você
                # tem aí.
                //$ftp->upload($_FILES["uploads"]["tmp_name"][$i], $pasta);
                # Agora o arquivo já foi upado, pode fazer alguns scripts adicionais, como por exemplo
                # adicionar o nome dele no banco de dados, ou talvez alertar o nome de cada arquivo.
                # Incrementar algum um valor a mais na variável "i" para que não ocorra loop infinito.
                /* for ($cont = 1; $cont <= 8; $cont++) { */
                $imovelfoto = new Imovelfoto();
                $form2->setInputFilter($imovelfoto->getInputFilter());

                //print_r($_FILES["uploads"]["name"]);die;

                $nonFile = $request->getPost()->toArray();
                $File = $this->params()->fromFiles($_FILES["fotos"]["name"][$i]);
                $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
                );



                /* $dataAtual['id'] = $data['id' . $cont]; */
                /* $imovelid = $data['imovel_id' . $cont]; */
                $dataAtual['imagem'] = $_FILES["fotos"]["name"][$i];
                $dataAtual['imovel_id'] = $imovel_id;

                //set data post and file ...    
                $form2->setData($dataAtual);


                if ($form2->isValid()) {


                    //HEY, FORM IS VALID...
                    $size = new Size(array('min' => 200000, 'max' => 5e+6)); //minimum bytes filesize

                    $adapter = new \Zend\File\Transfer\Adapter\Http();


                    $adapter->setDestination('./public_html/data/imoveis');

                    $destination = './public_html/data/imoveis';

                    $ext = pathinfo($_FILES["fotos"]["name"][$i], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $_FILES["fotos"]["name"][$i]) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form2->getData();


                    $data['imagem'] = $newName;

                    $imovelfoto->setData($data);

                    $saved = $this->getTable('Admin\Model\Imovelfoto')->save($imovelfoto);


                    if ($adapter->receive($_FILES["fotos"]["name"][$i])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];


                        $imovelfoto->exchangeArray($form->getData());
                    }
                }
                /* }
                 */ $i++;
                if ($i == 8) {
                    break;
                }
            }/* Final salvando as fotos do imóvel */

            /* Início salvando os diferenciais do imóvel */
            $form3 = new ImoveldifatualForm();
            $request = $this->getRequest();
            $Imoveldifatual = new Imoveldifatual;
            $form3->setInputFilter($Imoveldifatual->getInputFilter());
            $aux = $request->getPost();

            foreach ($aux['diferencial'] as $diferencial) {



                $aux2['imovel_id'] = $imovel_id;
                $aux2['diferencial_id'] = $diferencial;


                $form3->setData($aux2);

                if ($form3->isValid()) {
                    /* echo "chegou aqui"; */
                    $data = $form3->getData();

                    $Imoveldifatual->setData($data);

                    $saved = $this->getTable('Admin\Model\Imoveldifatual')->save($Imoveldifatual);
                    /* return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/obrigado'); */
                } else {
                    /* print_r($form->getMessages()); */
                }
            }/* Fim salvando os diferenciais do imóvel */
            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/obrigado');
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Imovel = $this->getTable('Admin\Model\Imovel')->get($id);
            $form->bind($Imovel);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }



        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('imoveis_tipos')
                ->order('tipo asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $tipos = $statement->execute();


        /* $select = $sql->select()
          // ->columns(array('id','nome','preco'))
          ->from('cidades')
          ->order('desc_cidade asc');
          $statement = $sql->prepareStatementForSqlObject($select);
          $cidades = $statement->execute(); */

        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('estados')
                ->order('nome asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $estados = $statement->execute();

        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('imoveis_diferenciais')
                ->order('nome asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $diferenciais = $statement->execute();


        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('planos')
                ->where('ativo=1')
                ->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $planos = $statement->execute();

        $view = new ViewModel(array(
            'form' => $form,
            'tipos' => $tipos,
            'estados' => $estados,
            'diferenciais' => $diferenciais,
            'planos' => $planos,
            'title' => "| Adicionar Imóvel"
        ));
        return $view;
    }

    /**
     * Cria uma nova conta
     * @return void
     */
    public function editarAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);

        $session = new Container('userDados');

        $imovel_id = (int) $this->params()->fromRoute('id', 0);



        $form = new ImovelForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            /* Salvando dados do imóvel */
            $Imovel = new Imovel;
            $form->setInputFilter($Imovel->getInputFilter());
            $teste = $request->getPost();

            $teste2['id'] = $imovel_id;
            $teste2['titulo'] = $teste['titulo'];
            $teste2['plano_id'] = $teste['plano_id'];
            $teste2['preco'] = $teste['preco'];
            $teste2['descricao'] = $teste['descricao'];

            $teste2['estado_id'] = $teste['estado_id'];
            $teste2['cidade_id'] = $teste['cidade_id'];
            $teste2['bairro'] = $teste['bairro'];
            $teste2['tipo_id'] = $teste['tipo_id'];
            $teste2['finalidade'] = $teste['finalidade'];
            $teste2['quartos'] = $teste['quartos'];
            $teste2['banheiros'] = $teste['banheiros'];
            $teste2['area_total'] = $teste['area_total'];
            $teste2['area_construida'] = $teste['area_construida'];
            $teste2['garagem'] = $teste['garagem'];
            $teste2['suites'] = $teste['suites'];
            $teste2['condominio'] = $teste['condominio'];

            $form->setData($teste2);

            if ($form->isValid()) {
                /* echo "chegou aqui"; */
                $data = $form->getData();
                unset($data['submit']);
                //unset($data['confirmarsenha']);
                $data['usuario_id'] = $session->usuario->id;
                $data['preco'] = str_replace(",", ".", $data['preco']);
                $data['area_total'] = str_replace(",", ".", $data['area_total']);
                $data['area_construida'] = str_replace(",", ".", $data['area_construida']);
                $data['condominio'] = str_replace(",", ".", $data['condominio']);
                $Imovel->setData($data);

                try {
                    $saved = $this->getTable('Admin\Model\Imovel')->save($Imovel);
                } catch (\Exception $e) {
                    
                }
                /* return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/obrigado'); */
            } else {
                /* print_r($form->getMessages()); */
            }/* Fim do Salvando dados do imóvel */





            /* Início salvando as fotos do imóvel */
            $form2 = new ImovelfotoForm($imovel_id);
            $request = $this->getRequest();
            $cont = 1;
            //$contador = 1;
            # Declarar a variável "i", que será nosso controle, para que não haja
            # loop infinito.
            $i = 0;

            # Faz um loop conforme o número de arquivos
            foreach ($_FILES["fotos"]["error"] as $key => $error) {

                # Definir a pasta que os arquivos serão "upados".
                // $pasta = "fotos/_" . $_FILES["uploads"]["name"][$i];
                # Aqui, você faz o upload do arquivo, utilizando a classe que você
                # tem aí.
                //$ftp->upload($_FILES["uploads"]["tmp_name"][$i], $pasta);
                # Agora o arquivo já foi upado, pode fazer alguns scripts adicionais, como por exemplo
                # adicionar o nome dele no banco de dados, ou talvez alertar o nome de cada arquivo.
                # Incrementar algum um valor a mais na variável "i" para que não ocorra loop infinito.
                /* for ($cont = 1; $cont <= 8; $cont++) { */
                $imovelfoto = new Imovelfoto();
                $form2->setInputFilter($imovelfoto->getInputFilter());

                //print_r($_FILES["uploads"]["name"]);die;

                $nonFile = $request->getPost()->toArray();
                $File = $this->params()->fromFiles($_FILES["fotos"]["name"][$i]);
                $data = array_merge_recursive(
                        $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
                );



                /* $dataAtual['id'] = $data['id' . $cont]; */
                /* $imovelid = $data['imovel_id' . $cont]; */
                $dataAtual['imagem'] = $_FILES["fotos"]["name"][$i];
                $dataAtual['imovel_id'] = $imovel_id;

                //set data post and file ...    
                $form2->setData($dataAtual);


                if ($form2->isValid()) {


                    //HEY, FORM IS VALID...
                    $size = new Size(array('min' => 200000, 'max' => 5e+6)); //minimum bytes filesize

                    $adapter = new \Zend\File\Transfer\Adapter\Http();


                    $adapter->setDestination('./public_html/data/imoveis');

                    $destination = './public_html/data/imoveis';

                    $ext = pathinfo($_FILES["fotos"]["name"][$i], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $_FILES["fotos"]["name"][$i]) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form2->getData();


                    $data['imagem'] = $newName;

                    $imovelfoto->setData($data);

                    try {
                        $saved = $this->getTable('Admin\Model\Imovelfoto')->save($imovelfoto);
                    } catch (\Exception $e) {
                        
                    }

                    if ($adapter->receive($_FILES["fotos"]["name"][$i])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];


                        $imovelfoto->exchangeArray($form->getData());
                    }
                }
                /* }
                 */ $i++;
                if ($i == 8) {
                    break;
                }
            }/* Final salvando as fotos do imóvel */

            /* deletar os diferenciais antes de salvar */
            $select = $sql->delete()
                    ->from('imoveis_dif_atual')
                    ->where("imovel_id=" . $imovel_id);
            $statement = $sql->prepareStatementForSqlObject($select);
            $statement->execute();
            /* fim deletar os diferenciais antes de salvar */

            /* Início salvando os diferenciais do imóvel */
            $form3 = new ImoveldifatualForm();
            $request = $this->getRequest();
            $Imoveldifatual = new Imoveldifatual;
            $form3->setInputFilter($Imoveldifatual->getInputFilter());
            $aux = $request->getPost();
            if (isset($aux['diferencial'])) {
                foreach ($aux['diferencial'] as $diferencial) {

                    $aux2['imovel_id'] = $imovel_id;
                    $aux2['diferencial_id'] = $diferencial;

                    $form3->setData($aux2);

                    if ($form3->isValid()) {
                        /* echo "chegou aqui"; */
                        $data = $form3->getData();

                        $Imoveldifatual->setData($data);

                        $saved = $this->getTable('Admin\Model\Imoveldifatual')->save($Imoveldifatual);
                        /* return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/obrigado'); */
                    } else {
                        /* print_r($form->getMessages()); */
                    }
                }
            }/* Fim salvando os diferenciais do imóvel */
//            return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/editar/' . $imovel_id);
        }


        $select = $sql->select()
                ->from('imoveis_tipos')
                ->order('tipo asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $tipos = $statement->execute();

        $select = $sql->select()
                ->from('imoveis_fotos')
                ->where("imovel_id=" . $imovel_id);
        $statement = $sql->prepareStatementForSqlObject($select);
        $fotos = $statement->execute();

        $select = $sql->select()
                ->from('imoveis')
                ->join('cidades', 'imoveis.cidade_id = cidades.cidade_id', array('cidade' => 'desc_cidade', 'estado' => 'flg_estado'))
                ->where("id=" . $imovel_id . " and usuario_id=" . $session->usuario->id);
        $statement = $sql->prepareStatementForSqlObject($select);
        $imovel = $statement->execute();
        $imovel->buffer();

        $estado_id = "";
        $cidade_id = "";
        foreach ($imovel as $imo) {
            $estado_id = $imo['estado_id'];
            $cidade_id = $imo['cidade_id'];
        }


//        foreach($imovel as $imo){
//            echo $imo['descricao'];die();
//        }

        $select = $sql->select()
                ->from('imoveis_dif_atual')
                ->where("imovel_id=" . $imovel_id);
        $statement = $sql->prepareStatementForSqlObject($select);
        $imoveis_dif_atual = $statement->execute();



        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('estados')
                ->order('nome asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $estados = $statement->execute();

        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('cidades')
                ->where("flg_estado='" . $estado_id . "'")
                ->order('desc_cidade asc');
//        echo $sql->getSqlstringForSqlObject($select);
//        die();
        $statement = $sql->prepareStatementForSqlObject($select);
        $cidades = $statement->execute();

        $select = $sql->select()
                ->quantifier('DISTINCT')
                ->columns(array('bairro'))
                ->from(array('i' => 'imoveis'))
                ->where("cidade_id=" . $cidade_id . "")
                ->order('bairro asc');

        $statement = $sql->prepareStatementForSqlObject($select);
        $bairros = $statement->execute();

        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('imoveis_diferenciais')
                ->order('nome asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $diferenciais = $statement->execute();


        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('planos')
                ->where('ativo=1')
                ->order('id asc');
        $statement = $sql->prepareStatementForSqlObject($select);
        $planos = $statement->execute();

        $view = new ViewModel(array(
            'form' => $form,
            'tipos' => $tipos,
            'estados' => $estados,
            'cidades' => $cidades,
            'bairros' => $bairros,
            'diferenciais' => $diferenciais,
            'planos' => $planos,
            'diferenciaisatual' => $imoveis_dif_atual,
            'fotos' => $fotos,
            'imovel' => $imovel,
            'title' => "| Adicionar Imóvel"
        ));
        return $view;
    }

    /**
     * Exclui uma foto do imóvel
     * @return void
     */
    public function deletarfotoAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        $imovelid = (int) $this->params()->fromRoute('imovelid', 0);

        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $imovelfoto = $this->getTable('Admin\Model\Imovelfoto')->get($id);

        unlink('./public_html/data/imoveis/' . $imovelfoto->imagem);

        $this->getTable('Admin\Model\Imovelfoto')->delete($id);

        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/editar/id/' . $imovelid . '#gallery');
    }

    /**
     * Exclui uma foto do imóvel
     * @return void
     */
    public function deletarimovelAction() {

        $id = (int) $this->params()->fromRoute('id', 0);

        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $session = new Container('userDados');

        $Imovel = $this->getTable('Admin\Model\Imovel');
        $sql = $Imovel->getSql();
        $select = $sql->select()
                ->columns(array('id', 'usuario_id'))
                ->where(array("usuario_id=" . $session->usuario->id . "", "id=" . $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $imovel = $statement->execute();

        foreach ($imovel as $imo) {

            $this->getTable('Admin\Model\Imovel')->delete($imo['id']);
        }

        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/imovel/meusimoveis');
    }

    public function carregarcidadesAction() {



        $estado = $this->params()->fromQuery('estado', '');

        $this->layout('layout/ajax-layout');

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                // ->columns(array('id','nome','preco'))
                ->from('cidades')
                ->where("flg_estado='" . $estado . "'")
                ->order('desc_cidade asc');

        $statement = $sql->prepareStatementForSqlObject($select);
        $dados = $statement->execute();

        $view = new ViewModel(array(
            'dados' => $dados
        ));
        return $view;
    }

    public function carregarbairrosAction() {

        $cidade_id = $this->params()->fromQuery('cidade', 0);

        $this->layout('layout/ajax-layout');

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select = $sql->select()
                ->quantifier('DISTINCT')
                ->columns(array('bairro'))
                ->from(array('i' => 'imoveis'))
                ->where("cidade_id=" . $cidade_id . "")
                ->order('bairro asc');

        $statement = $sql->prepareStatementForSqlObject($select);
        $dados = $statement->execute();

        $view = new ViewModel(array(
            'dados' => $dados
        ));
        return $view;
    }

    /**
     * Mostra as Imovels cadastrados
     * @return void
     */
    public function obrigadoAction() {
        /* $Imovel = $this->getTable('Admin\Model\Imovel');
          $sql = $Imovel->getSql();
          $select = $sql->select();

          $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
          $paginator = new Paginator($paginatorAdapter);
          $paginator->setCurrentPageNumber($this->params()->fromRoute('page')); */


        $view = new ViewModel(
                array(
            'title' => "| Adicionar Imóvel"
                )
        );
        return $view;
    }

    /**
     * Mostra as Imovels cadastrados
     * @return void
     */
    public function meusimoveisAction() {
        $session = new Container('userDados');

        $Imovel = $this->getTable('Admin\Model\Imovel');
        $sql = $Imovel->getSql();
        $select = $sql->select()
                ->join('cidades', 'imoveis.cidade_id = cidades.cidade_id', array('cidade' => 'desc_cidade', 'estado' => 'flg_estado'))
                ->join('imoveis_fotos', 'imoveis.id = imoveis_fotos.imovel_id', array('imagem'))
                ->where(array("usuario_id=" . $session->usuario->id . ""))
                ->group("imoveis.id");

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'imoveis' => $paginator,
            'title' => "| Meus Imóveis"
        ));
        return $view;
    }

    /**
     * Mostra as Imovels cadastrados
     * @return void
     */
    public function indexAction() {
        $Imovel = $this->getTable('Admin\Model\Imovel');
        $sql = $Imovel->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'Imovels' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new ImovelForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Imovel = new Imovel;
            $form->setInputFilter($Imovel->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                $data['senha'] = md5($data['senha']);
                $Imovel->setData($data);

                $saved = $this->getTable('Admin\Model\Imovel')->save($Imovel);
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/Imovel');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Imovel = $this->getTable('Admin\Model\Imovel')->get($id);
            $form->bind($Imovel);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Imovel')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/Imovel');
    }

}
