<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Portifolio;
use Admin\Form\Portifolio as PortifolioForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as Portifolios dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class PortifolioController extends ActionController {

    /**
     * Mostra as Portifolios cadastrados
     * @return void
     */
    public function indexAction() {

        $Portifolio = $this->getTable('Admin\Model\Portifolio');
        $sql = $Portifolio->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $view = new ViewModel(array(
            'portifolios' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new PortifolioForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Portifolio = new Portifolio;
            $form->setInputFilter($Portifolio->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($data);

            if ($form->isValid()) {

                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $Portifolio->setData($data);




                    $saved = $this->getTable('Admin\Model\Portifolio')->save($Portifolio);
                    $adapter->setDestination('./public_html/data/portifolios');
                    if ($adapter->receive($File['name'])) {
                        $Portifolio->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                } else {
                    $data = $form->getData();
                    $data['imagem'] = $File['name'];
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $Portifolio->setData($data);




                    $saved = $this->getTable('Admin\Model\Portifolio')->save($Portifolio);
                    $adapter->setDestination('./public_html/data/portifolios');
                    if ($adapter->receive($File['name'])) {
                        $Portifolio->exchangeArray($form->getData());

                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                }


                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/portifolio');
            }
        
		
		}
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $Portifolio = $this->getTable('Admin\Model\Portifolio')->get($id);
            $form->bind($Portifolio);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $Portifolio = $this->getTable('Admin\Model\Portifolio')->get($id);
        
        // unlink('./public_html/data/portifolios/'.$Portifolio->imagem);

        $this->getTable('Admin\Model\Portifolio')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/portifolio');
    }

}