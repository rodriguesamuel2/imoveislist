<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Marca;
use Admin\Form\Marca as MarcaForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as marcas dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class MarcaController extends ActionController {

    /**
     * Mostra as marcas cadastrados
     * @return void
     */
    public function indexAction() {
        $marca = $this->getTable('Admin\Model\Marca');
        $sql = $marca->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'marcas' => $paginator
        ));
        return $view;
    }

    /**
     * Retorna os produtos da marca
     * @return Zend\Http\Response 
     */
    public function produtosAction() {
        /* exemplo para usar com ajax */
        /* $id = (int) $this->params()->fromRoute('id', 0);
          $where = array('marca_id' => $id);
          $produtos = $this->getTable('Application\Model\Produto')
          ->fetchAll(null, $where)
          ->toArray();

          $response = $this->getResponse();
          $response->setStatusCode(200);
          $response->setContent(json_encode($produtos));
          $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
          return $response; */
        $id = (int) $this->params()->fromRoute('id', 0);
        $where = array('marca_id' => $id);
        $produtos = $this->getTable('Admin\Model\Produto')
                ->fetchAll(null, $where)
                ->toArray();
        $result = new ViewModel(array(
            'produtos' => $produtos
                )
        );
        $result->setTerminal(true);
        return $result;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {

        $form = new MarcaForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $marca = new Marca();
            $form->setInputFilter($marca->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('file');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('imagem' => $error));
                } else {
                    
                    $adapter->setDestination('./public_html/data/marcas');
                    $destination = './public_html/data/marcas';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));
                    
                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['file'] = $newName;
                    $marca->setData($data);

                    $saved = $this->getTable('Admin\Model\Marca')->save($marca);

                    $adapter->setDestination('./public_html/data/marcas');
                    if ($adapter->receive($File['name'])) {
                        
                        $file = $adapter->getFilter('File\Rename')->getFile();

                        $target = $file[0]['target'];
                        
                        $marca->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/marca');
                        //echo 'Profile Name ' . $marca->nome . ' upload ' . $marca->file;
                    }
                }
            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $marca = $this->getTable('Admin\Model\Marca')->get($id);
            $form->bind($marca);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }
        
        $Marca = $this->getTable('Admin\Model\Marca')->get($id);

        unlink('./public_html/data/marcas/' . $Marca->file);

        $this->getTable('Admin\Model\Marca')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/marca');
    }

}