<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Curriculo;
use Admin\Form\Curriculo as CurriculoForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as curriculos da página inicial
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class CurriculoController extends ActionController {

    /**
     * Mostra as curriculos cadastrados
     * @return void
     */
    public function indexAction() {

        $curriculo = $this->getTable('Admin\Model\Curriculo');
        $sql = $curriculo->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'curriculos' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {


        $form = new CurriculoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $curriculo = new Curriculo();
            $form->setInputFilter($curriculo->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('arquivo');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('arquivo' => $error));
                } else {
                    

                    $adapter->setDestination('./public_html/data/curriculos');
                    $destination = './public_html/data/curriculos';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));
                    
                    $data = $form->getData();

                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['arquivo'] = $newName;
                    //$data['produto_id'] = $proid;
                    $curriculo->setData($data);

                    $saved = $this->getTable('Admin\Model\Curriculo')->save($curriculo);
                    
                    $adapter->setDestination('./public_html/data/curriculos');
                    if ($adapter->receive($File['name'])) {
                        
                        $file = $adapter->getFilter('File\Rename')->getFile();
                        $target = $file[0]['target'];
//                        var_dump($target);
                        
                        $curriculo->exchangeArray($form->getData());

                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/curriculo/index');
                        //echo 'Profile Name ' . $curriculo->nome . ' upload ' . $curriculo->file;
                    }
                }
            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $curriculo = $this->getTable('Admin\Model\Curriculo')->get($id);
            $form->bind($curriculo);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);

        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $Curriculo = $this->getTable('Admin\Model\Curriculo')->get($id);

        unlink('./public_html/data/curriculos/'.$Curriculo->arquivo);

        $this->getTable('Admin\Model\Curriculo')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/curriculo/index');
    }

}
