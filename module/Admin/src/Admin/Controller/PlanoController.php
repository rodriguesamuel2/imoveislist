<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Plano;
use Admin\Form\Plano as PlanoForm;

/**
 * Controlador que gerencia as planos dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class PlanoController extends ActionController {

    /**
     * Mostra as planos cadastrados
     * @return void
     */
    public function indexAction() {
        
    
        $plano = $this->getTable('Admin\Model\Plano');
        $sql = $plano->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

       
        $view = new ViewModel(array(
            'planos' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new PlanoForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $plano = new Plano;
            $form->setInputFilter($plano->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $plano->setData($data);

                $saved = $this->getTable('Admin\Model\Plano')->save($plano);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/plano');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $plano = $this->getTable('Admin\Model\Plano')->get($id);
            $form->bind($plano);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Plano')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/plano');
    }

}