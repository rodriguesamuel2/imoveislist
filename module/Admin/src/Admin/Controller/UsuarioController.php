<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Usuario;
use Admin\Form\Usuario as UsuarioForm;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;

/**
 * Controlador que gerencia as usuarios dos produtos
 *
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class UsuarioController extends ActionController {

    /**
     * Cria uma nova conta
     * @return void
     */
    public function registrarAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select1 = $sql->select()
                //->columns(array('id', 'legenda', 'texto', 'imagem'))
                ->from('imobiliarias')
                //->where(array('banners.ativo=1'))
                ->order(array("nome asc"));
        $statement1 = $sql->prepareStatementForSqlObject($select1);
        $imobiliarias = $statement1->execute();

//        $translator = $this->getServiceLocator()->get('translator');
//        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new UsuarioForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $usuario = new Usuario;
            $form->setInputFilter($usuario->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                unset($data['confirmarsenha']);
                $data['role'] = 'usuario';
                $data['valid'] = 1;
                $data['senha'] = md5($data['senha']);
                $usuario->setData($data);

                $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/auth/index');
            }
        }
        //$id = (int) $this->params()->fromRoute('id', 0);
        //if ($id > 0) {
        //    $usuario = $this->getTable('Admin\Model\Usuario')->get($id);
        //    $form->bind($usuario);
        //    $form->get('submit')->setAttribute('value', 'Salvar');
        //}

        $view = new ViewModel(array(
            'form' => $form,
            'imobiliarias' => $imobiliarias
        ));
        return $view;
    }

    /**
     * Mostra os dados do perfil
     * @return void
     */
    public function perfilAction() {
        $session = new Container('userDados');

        $usuario = $this->getTable('Admin\Model\Usuario');
        $sql = $usuario->getSql();
        $select = $sql->select()
                ->where(array("id=" . $session->usuario->id . ""));

        $statement = $sql->prepareStatementForSqlObject($select);
        $usuario = $statement->execute();

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
        $select1 = $sql->select()
                ->from('estados')
                ->order(array("nome asc"));
        $statement1 = $sql->prepareStatementForSqlObject($select1);
        $estados = $statement1->execute();

        $consultor = array();
        $imobiliaria = array();
        $cidades = array();

        /* se for um consultor buscar informações do consultor */
        if ($session->usuario->tipo_id == 2) {

            $consultor1 = $this->getTable('Admin\Model\Consultor');
            $sql = $consultor1->getSql();
            $select = $sql->select()
                    ->where(array("usuario_id=" . $session->usuario->id . ""));

            $statement = $sql->prepareStatementForSqlObject($select);
            $consultor = $statement->execute();
            
             $estado_id = '';

            foreach ($consultor as $imob) {
                $estado_id = $imob['estado_id'];
            }

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select1 = $sql->select()
                    ->from('cidades')
                    ->where(array("flg_estado='" . $estado_id . "'"))
                    ->order(array("desc_cidade asc"));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $cidades = $statement1->execute();
        }

        /* se for um consultor buscar informações da imobiliaria */
        if ($session->usuario->tipo_id == 3) {
            $imobiliaria1 = $this->getTable('Admin\Model\Imobiliaria');
            $sql = $imobiliaria1->getSql();
            $select = $sql->select()
                    ->where(array("usuario_id=" . $session->usuario->id . ""));

            $statement = $sql->prepareStatementForSqlObject($select);
            $imobiliaria = $statement->execute();

            $estado_id = '';

            foreach ($imobiliaria as $imob) {
                $estado_id = $imob['estado_id'];
            }

            $adapter = $this->getServiceLocator()->get('DbAdapter');
            $sql = new Sql($adapter);
            $select1 = $sql->select()
                    ->from('cidades')
                    ->where(array("flg_estado='" . $estado_id . "'"))
                    ->order(array("desc_cidade asc"));
            $statement1 = $sql->prepareStatementForSqlObject($select1);
            $cidades = $statement1->execute();
        }


        $view = new ViewModel(array(
            'usuario' => $usuario,
            'consultor' => $consultor,
            'imobiliaria' => $imobiliaria,
            'estados' => $estados,
            'cidades' => $cidades,
            'title' => "| Perfil"
        ));
        return $view;
    }

    /**
     * Mostra as usuarios cadastrados
     * @return void
     */
    public function indexAction() {
        $usuario = $this->getTable('Admin\Model\Usuario');
        $sql = $usuario->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'usuarios' => $paginator
        ));
        return $view;
    }

    /**
     * Atualizar dados do usuario
     * @return void
     */
    public function atualizarAction() {
        $session = new Container('userDados');
        /* $translator = $this->getServiceLocator()->get('translator');
          \Zend\Validator\AbstractValidator::setDefaultTranslator($translator); */
        $form = new UsuarioForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $usuario = new Usuario;
            $form->setInputFilter($usuario->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                if ($session->usuario->senha = md5($data['senha'])) {

                    /* echo $data['confirmarsenha'];die();        */

                    $data['id'] = $session->usuario->id;
                    $data['role'] = $session->usuario->role;
                    $data['valid'] = $session->usuario->valid;
                    if (isset($data['senhanova']) && $data['senhanova'] != "") {

                        $data['senha'] = md5($data['senhanova']);
                    } else {
                        $data['senha'] = $session->usuario->senha;
                    }
                    unset($data['btnsubmit']);
                    unset($data['confirmarsenha']);
                    unset($data['senhanova']);

                    $usuario->setData($data);

                    try {
                        $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
                    } catch (\Exception $e) {
                        
                    }

                    $_SESSION['mensagem'] = "Dados alterados com sucesso!";
                } else {
                    $_SESSION['mensagem'] = "Não foi possível alterar os dados as senha está incorreta!";
                }
            } else {
                print_r($form->getMessages());
            }
        }
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario/perfil');
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new UsuarioForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $usuario = new Usuario;
            $form->setInputFilter($usuario->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                $data['senha'] = md5($data['senha']);
                $usuario->setData($data);

                $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $usuario = $this->getTable('Admin\Model\Usuario')->get($id);
            $form->bind($usuario);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Usuario')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario');
    }

}
