<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Produto;
use Admin\Model\Casal;
use Admin\Model\Lista;
use Admin\Form\Lista as ListaForm;

/**
 * Controlador que gerencia as Listas dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class ListaController extends ActionController {

    /**
     * Mostra as Listas cadastrados
     * @return void
     */
    public function indexAction() {
        
    
        $Lista = $this->getTable('Admin\Model\Lista');
        $sql = $Lista->getSql();
		$select = $sql->select()
            ->group(array('noivo_id'));

		$casais = $this->getTable('Admin\Model\Casal')->fetchAll(null,null,null,null,array('nome asc'))->toArray();
		$produtos = $this->getTable('Admin\Model\Produto')->fetchAll(null,null,null,null,array('nome asc'))->toArray();		
		
        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

       
        $view = new ViewModel(array(
            'listas' => $paginator,
            'casais' => $casais,
            'produtos' => $produtos
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
		
        $casal = $this->getTable('Admin\Model\Casal')->fetchAll()->toArray();		
        $produtos = $this->getTable('Admin\Model\Produto')->fetchAll()->toArray();		
		
        $form = new ListaForm($casal,$produtos);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $Lista = new Lista;
            $form->setInputFilter($Lista->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $Lista->setData($data);

                // $saved = $this->getTable('Admin\Model\Lista')->save($Lista);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/prodlista/index/casalid/'.$data['noivo_id'].'');
            }
        }
        // $id = (int) $this->params()->fromRoute('id', 0);
        // if ($id > 0) {
            // $Lista = $this->getTable('Admin\Model\Lista')->get($id);
            // $form->bind($Lista);
            // $form->get('submit')->setAttribute('value', 'Salvar');
        // }
        
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Lista')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/lista');
    }

}