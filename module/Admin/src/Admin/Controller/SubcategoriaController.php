<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Subcategoria;
use Admin\Model\Categoria;
use Admin\Form\Subcategoria as SubcategoriaForm;

/**
 * Controlador que gerencia a subcategorias dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class SubcategoriaController extends ActionController {

    /**
     * Mostra as subcategorias cadastrados
     * @return void
     */
    public function indexAction() {

        $subcategoria = $this->getTable('Admin\Model\Subcategoria');
        $sql = $subcategoria->getSql();
        $select = $sql->select()
                ->order("nome asc");

        $categorias = $this->getTable('Admin\Model\Categoria')->fetchAll(null,null,null,null,array('nome asc'))->toArray();


        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'subcategorias' => $paginator,
            'categorias' => $categorias
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        /* $categoria = $this->getTable('Admin\Model\Categoria');
          $sql = $categoria->getSql();
          $select_categoria = $sql->select(); */

        $categorias = $this->getTable('Admin\Model\Categoria')->fetchAll()->toArray();

        $form = new SubcategoriaForm($categorias);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $subcategoria = new Subcategoria;
            $form->setInputFilter($subcategoria->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                //$data['post_date'] = date('Y-m-d H:i:s');
                $subcategoria->setData($data);

                $saved = $this->getTable('Admin\Model\Subcategoria')->save($subcategoria);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/subcategoria');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $subcategoria = $this->getTable('Admin\Model\Subcategoria')->get($id);
            $form->bind($subcategoria);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Subcategoria')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/subcategoria');
    }

}