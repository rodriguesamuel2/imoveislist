<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
/**
 * Controlador que gerencia a index do administrador
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class IndexController extends ActionController
{
    /**
     * Mostra as categorias cadastradas
     * @return void
     */
    public function indexAction()
    {
        
        $view = new ViewModel(array(
            'categorias' => $this->getTable('Admin\Model\Categoria')->fetchAll()->toArray()
        ));
        return $view;
    }
}