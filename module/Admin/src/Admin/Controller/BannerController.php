<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Banner;
use Admin\Form\Banner as BannerForm;
use Zend\Validator\File\Size;

/**
 * Controlador que gerencia as banners da página inicial
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class BannerController extends ActionController {

    /**
     * Mostra as banners cadastrados
     * @return void
     */
    public function indexAction() {

        $banner = $this->getTable('Admin\Model\Banner');
        $sql = $banner->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'banners' => $paginator
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {


        $form = new BannerForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $banner = new Banner();
            $form->setInputFilter($banner->getInputFilter());

            $nonFile = $request->getPost()->toArray();
            $File = $this->params()->fromFiles('imagem');
            $data = array_merge_recursive(
                    $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray()
            );


            //set data post and file ...    
            $form->setData($data);

            if ($form->isValid()) {


                //HEY, FORM IS VALID...
                $size = new Size(array('min' => 200000)); //minimum bytes filesize

                $adapter = new \Zend\File\Transfer\Adapter\Http();
                //validator can be more than one...
                //$adapter->setValidators(array($size), $File['name']);

                if (!$adapter->isValid()) {
                    $dataError = $adapter->getMessages();
                    $error = array();
                    foreach ($dataError as $key => $row) {
                        $error[] = $row;
                    } //set formElementErrors
                    $form->setMessages(array('imagem' => $error));
                } else {

                    $adapter->setDestination('./public_html/data/banners');
                    $destination = './public_html/data/banners';

                    $ext = pathinfo($File['name'], PATHINFO_EXTENSION);

                    $newName = md5(rand() . $File['name']) . '.' . $ext;

                    $adapter->addFilter('File\Rename', array(
                        'target' => $destination . '/' . $newName,
                    ));

                    $data = $form->getData();
                    unset($data['submit']);
                    //$data['post_date'] = date('Y-m-d H:i:s');
                    $data['imagem'] = $newName;
                    //$data['produto_id'] = $proid;
                    $banner->setData($data);

                    $saved = $this->getTable('Admin\Model\Banner')->save($banner);

                    $adapter->setDestination('./public_html/data/banners');
                    if ($adapter->receive($File['name'])) {

                        $file = $adapter->getFilter('File\Rename')->getFile();

                        $target = $file[0]['target'];

//                        var_dump($target);

                        $banner->exchangeArray($form->getData());


                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/banner/index');
                        //echo 'Profile Name ' . $banner->nome . ' upload ' . $banner->file;
                    }
                }
            }
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $banner = $this->getTable('Admin\Model\Banner')->get($id);
            $form->bind($banner);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {

        $id = (int) $this->params()->fromRoute('id', 0);

        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $Banner = $this->getTable('Admin\Model\Banner')->get($id);

        unlink('./public_html/data/banners/' . $Banner->imagem);

        $this->getTable('Admin\Model\Banner')->delete($id);

        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/banner/index');
    }

}
