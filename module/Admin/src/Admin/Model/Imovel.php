<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade Consultor
 * 
 * @category Admin
 * @package Model
 */
class Imovel extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName ='imoveis';
 
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $plano_id;
 
    /**
     * @var string
     */
    protected $titulo;

    /**
     * @var int
     */
    protected $preco;
 
    /**
     * @var string
     */
    protected $descricao;    
 
    /**
     * @var string
     */
    protected $estado_id;

    /**
     * @var int
     */
    protected $cidade_id;

    /**
     * @var int
     */
    protected $bairro_id;

    /**
     * @var int
     */
    protected $tipo_id;

    /**
     * @var string
     */
    protected $finalidade;

    /**
     * @var int
     */
    protected $quartos;    

    /**
     * @var int
     */
    protected $banheiros;

    /**
     * @var int
     */
    protected $area_total;

    /**
     * @var int
     */
    protected $area_construida;

    /**
     * @var int
     */
    protected $garagem;

    /**
     * @var int
     */
    protected $suites;

    /**
     * @var int
     */
    protected $condominio;

    /**
     * @var int
     */
    protected $usuario_id;

    /**
     * @var string
     */
    protected $criacao;

    /**
     * @var string
     */
    protected $validade; 

    /**
     * @var string
     */
    protected $status;

   

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

             $inputFilter->add($factory->createInput(array(
                'name'     => 'plano_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'titulo',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                 'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 90,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'preco',
                'required' => true,               
            )));


            $inputFilter->add($factory->createInput(array(
                'name'     => 'descricao',
                'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'estado_id',
                'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'cidade_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'bairro',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                 'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 90,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'tipo_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'finalidade',
                'required' => true,
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 1,
                        ),
                    ),
                ),
            )));

         

            $inputFilter->add($factory->createInput(array(
                'name'     => 'banheiros',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'area_total',
                'required' => true,                
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'area_construida',
                'required' => true,                
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'garagem',
                'required' => true,  
                'filters'  => array(
                    array('name' => 'Int'),
                ),              
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'suites',
                'required' => true,  
                'filters'  => array(
                    array('name' => 'Int'),
                ),              
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'condominio',
                'required' => false,                           
            )));


            $inputFilter->add($factory->createInput(array(
                'name'     => 'usuario_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'criacao',
                'required' => false,
               
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'validade',
                'required' => false,               
            )));

              $inputFilter->add($factory->createInput(array(
                'name'     => 'status',
                'required' => false,               
            )));
          

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}