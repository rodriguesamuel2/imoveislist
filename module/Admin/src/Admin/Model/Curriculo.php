<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Curriculo
 * 
 * @category Admin
 * @package Model
 */
class Curriculo extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'curriculos';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $objetivo;
    
    /**
     * @var string
     */
    protected $nome;
    
    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $perfil;
    
    /**
     * @var id
     */
    protected $cliente_id;

    /**
     * @var string
     */
    protected $arquivo;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'cliente_id',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'Int'),
//                        ),
//            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'objetivo',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 250,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'nome',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 150,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 150,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'arquivo',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'perfil',
                        'required' => false,
                       
            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
