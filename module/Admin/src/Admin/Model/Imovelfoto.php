<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Profoto
 * 
 * @category Admin
 * @package Model
 */
class Imovelfoto extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'imoveis_fotos';

    /**
     * @var int
     */
    protected $id;       

    /**
     * @var string
     */
    protected $imagem;   



    /**
     * @var int
     */
    protected $imovel_id;

      /**
     * @var int
     */
    protected $ordem;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'imagem',
                        'required' => true,
//                        'filters' => array(
//                            //array('name' => 'StripTags'),
//                            //array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 200,
//                                ),
//                            ),
//                        ),
            )));

            
            

            $inputFilter->add($factory->createInput(array(
                        'name' => 'imovel_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

           
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ordem',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

           

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}