<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Produto
 * 
 * @category Admin
 * @package Model
 */
class Produto extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'produtos';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $nome;
    /**
     * @var string
     */
    protected $endereco;

    /**
     * @var int
     */
    protected $preco;

    /**
     * @var int
     */
    protected $preco_promocao;

    /**
     * @var string
     */
    protected $especificacoes;

    /**
     * @var int
     */
    protected $oferta;

    /**
     * @var int
     */
    protected $promocao;

    /**
     * @var int
     */
    protected $novo;

    /**
     * @var string
     */
    protected $descricao;

    /**
     * @var int
     */
    protected $subcategoria_id;

    /**
     * @var int
     */
    protected $marca_id;

    /**
     * @var int
     */
    protected $ativo;

    /**
     * @var int
     */
    protected $data;

    /**
     * @var int
     */
    protected $estoque;
    /**
     * @var int
     */
    protected $peso;
    /**
     * @var int
     */
    protected $altura;
    /**
     * @var int
     */
    protected $largura;
    /**
     * @var int
     */
    protected $comprimento;
    

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'nome',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'endereco',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 200,
//                                ),
//                            ),
//                        ),
//            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'preco',
                        'required' => false,
                        /*'validators' => array(
                            array(
                                'name' => 'Decimal',
                            ),
                        ),*/
                            //'filters'  => array(
                            //    array('name' => 'Int'),
                            //),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'preco_promocao',
                        'required' => false,
                       
                            //'filters'  => array(
                            //    array('name' => 'Number'),
                            //),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'especificacoes',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'promocao',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'oferta',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'novo',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'descricao',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'subcategoria_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'marca_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'ativo',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'data',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'estoque',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'peso',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'altura',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'largura',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'comprimento',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}