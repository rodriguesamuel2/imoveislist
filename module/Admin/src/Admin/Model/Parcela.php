<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade Parcela
 * 
 * @category Admin
 * @package Model
 */
class Parcela extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName ='parcelas';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var int
     */
    protected $moto_id;
    
    /**
     * @var int
     */
    protected $versao_id;
    /**
     * @var int
     */
    protected $plano_id;
    /**
     * @var int
     */
    protected $qtde;
    /**
     * @var int
     */
    protected $valor_parcela;
    /**
     * @var int
     */
    protected $valor_credito;
    
   
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));         
          
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'moto_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'plano_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'qtde',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'valor_parcela',
                'required' => true,
                
            )));
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'valor_credito',
                'required' => false,
                
            ))); 
 
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}