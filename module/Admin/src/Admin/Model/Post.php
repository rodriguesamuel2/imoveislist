<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Post
 * 
 * @category Admin
 * @package Model
 */
class Post extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'posts';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $titulo;

    /**
     * @var string
     */
    protected $texto;

    /**
     * @var date
     */
    protected $data;

    /**
     * @var string
     */
    protected $imagem;

    /**
     * @var string
     */
    protected $autor;

    /**
     * @var string
     */
    protected $video;

    /**
     * @var string
     */
    protected $status;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'titulo',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 250,
                                ),
                            ),
                        ),
            )));



            $inputFilter->add($factory->createInput(array(
                        'name' => 'texto',
                        'required' => false,
                        'filters' => array(
//                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'data',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'autor',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 0,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'video',
                        'required' => false,
            )));



            $inputFilter->add($factory->createInput(array(
                        'name' => 'imagem',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));




            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}