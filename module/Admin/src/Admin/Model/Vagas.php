<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Portifolio
 * 
 * @category Admin
 * @package Model
 */
class Vagas extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'vagas';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $titulo;

    /**
     * @var string
     */
    protected $texto;

    /**
     * @var date
     */
    protected $data;

    /**
     * @var string
     */
    protected $imagem;

    /**
     * @var int
     */
    protected $cliente_id;

    /**
     * @var string
     */
    protected $ativo;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'titulo',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 250,
                                ),
                            ),
                        ),
            )));



            $inputFilter->add($factory->createInput(array(
                        'name' => 'texto',
                        'required' => false,
                        'filters' => array(
//                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'data',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'imagem',
                        'required' => false,
            )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'cliente_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'ativo',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 1,
                                ),
                            ),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
