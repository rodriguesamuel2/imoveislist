<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Parceiro
 * 
 * @category Admin
 * @package Model
 */
class Parceiro extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'parceiros';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $nome;


    /**
     * @var string
     */
    protected $imagem;
    
      /**
     * @var string
     */
    protected $site;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'nome',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 200,
                                ),
                            ),
                        ),
            )));
            
            
            
           
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'imagem',
                        'required' => false,
            )));
            
           
            
             $inputFilter->add($factory->createInput(array(
                        'name' => 'site',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 0,
                                    'max' => 150,
                                ),
                            ),
                        ),
            )));
           

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}