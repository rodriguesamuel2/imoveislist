<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Post
 * 
 * @category Admin
 * @package Model
 */
class Pagina extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'paginas';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $titulo;

    /**
     * @var string
     */
    protected $texto;

    /**
     * @var string
     */
    protected $estado;

    /**
     * @var string
     */
    protected $cidade;

    /**
     * @var string
     */
    protected $facul;

    /**
     * @var string
     */
    protected $curso;

    /**
     * @var string
     */
    protected $ano;

    /**
     * @var int
     */
    protected $cliente_id;

    /**
     * @var string
     */
    protected $ativo;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'titulo',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 150,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'texto',
                        'required' => true,
            )));

//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'estado',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 10,
//                                ),
//                            ),
//                        ),
//            )));

//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'cidade',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 50,
//                                ),
//                            ),
//                        ),
//            )));

//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'facul',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 500,
//                                ),
//                            ),
//                        ),
//            )));

//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'curso',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 500,
//                                ),
//                            ),
//                        ),
//            )));

//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'ano',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 50,
//                                ),
//                            ),
//                        ),
//            )));

//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'cliente_id',
//                        'required' => false,
//                        'filters' => array(
//                            array('name' => 'Int'),
//                        ),
//            )));

//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'ativo',
//                        'required' => false,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 1,
//                                ),
//                            ),
//                        ),
//            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
