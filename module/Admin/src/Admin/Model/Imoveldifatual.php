<?php

namespace Admin\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

/**
 * Entidade Profoto
 * 
 * @category Admin
 * @package Model
 */
class Imoveldifatual extends Entity {

    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'imoveis_dif_atual';

    /**
     * @var int
     */
    protected $id;       

    /**
     * @var int
     */
    protected $imovel_id;

      /**
     * @var int
     */
    protected $diferencial_id;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));          
            

            $inputFilter->add($factory->createInput(array(
                        'name' => 'imovel_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

           
            $inputFilter->add($factory->createInput(array(
                        'name' => 'diferencial_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

           

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}