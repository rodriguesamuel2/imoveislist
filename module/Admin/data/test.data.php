<?php

//queries used by tests
return array(
    'categorias' => array(
        'create' => 'CREATE TABLE categorias (
                    id int(11) NOT NULL AUTO_INCREMENT,
                    nome varchar(100) NOT NULL,
                     PRIMARY KEY (id)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8',
        'drop' => "DROP TABLE categorias;"
    ),
    'marcas' => array(
        'create' => 'CREATE TABLE marcas (
        id int(11) NOT NULL AUTO_INCREMENT,
        nome varchar(100) NOT NULL,
        imagem varchar(50) DEFAULT NULL,
        PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ',
        'drop' => "DROP TABLE marcas;"
    ),
    'configuracoes' => array(
        'create' => 'CREATE TABLE configuracoes (
        id int(11) NOT NULL AUTO_INCREMENT,
        nome varchar(45) NOT NULL,
        descricao text NOT NULL,
        parametro varchar(150) NOT NULL,
        PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8

        ',
        'drop' => "DROP TABLE configuracoes;"
    ),
    'subcategorias' => array(
        'create' => 'CREATE TABLE subcategorias (
            id int(11) NOT NULL AUTO_INCREMENT,
            nome varchar(100) NOT NULL,
            categoria_id int(11) NOT NULL,
            PRIMARY KEY (id),
            KEY fk_subcategorias_categorias1_idx (categoria_id),
            CONSTRAINT fk_subcategorias_categorias1 FOREIGN KEY (categoria_id) REFERENCES categorias (id) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ',
        'drop' => "DROP TABLE subcategorias;"
    ),
    'produtos' => array(
        'create' => 'CREATE TABLE produtos (
        id int(11) NOT NULL AUTO_INCREMENT,
        nome varchar(150) NOT NULL,
        preco float NOT NULL,
        preco_promocao float DEFAULT NULL,
        especificacoes text,
        promocao int(11) DEFAULT NULL,
        oferta int(11) DEFAULT NULL,
        novo int(11) DEFAULT NULL,
        descricao text NOT NULL,
        subcategoria_id int(11) NOT NULL,
        marca_id int(11) NOT NULL,
        ativo int(11) DEFAULT NULL,
        data timestamp NULL DEFAULT NULL,
        estoque float DEFAULT NULL,
        PRIMARY KEY (id),
        KEY fk_produtos_subcategorias1_idx (subcategoria_id),
        KEY fk_produtos_marcas1_idx (marca_id),
        CONSTRAINT fk_produtos_subcategorias1 FOREIGN KEY (subcategoria_id) REFERENCES subcategorias (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT fk_produtos_marcas1 FOREIGN KEY (marca_id) REFERENCES marcas (id) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ',
        'drop' => "DROP TABLE produtos;"
    ),
    'profotos' => array(
        'create' => "CREATE TABLE profotos (
            id int(11) NOT NULL AUTO_INCREMENT,
            imagem varchar(200) NOT NULL,
            produto_id int(11) NOT NULL,
            principal int(11) DEFAULT '0',
            legenda varchar(150) DEFAULT NULL,
            PRIMARY KEY (id),
            KEY fk_profotos_produtos_idx (produto_id),
            CONSTRAINT fk_profotos_produtos FOREIGN KEY (produto_id) REFERENCES produtos (id) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ",
        'drop' => "DROP TABLE profotos;"
    ),
    'paginas' => array(
        'create' => "CREATE TABLE paginas (
            id int(11) NOT NULL AUTO_INCREMENT,
             titulo varchar(150) NOT NULL,
            texto text NOT NULL,
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ",
        'drop' => "DROP TABLE paginas;"
    ),
    'pagfotos' => array(
        'create' => "
        CREATE TABLE pagfotos (
        id int(11) NOT NULL AUTO_INCREMENT,
        legenda varchar(150) DEFAULT NULL,
        principal int(11) DEFAULT '0',
        pagina_id int(11) NOT NULL,
        PRIMARY KEY (id),
        KEY fk_pagfotos_paginas1_idx (pagina_id),
        CONSTRAINT fk_pagfotos_paginas1 FOREIGN KEY (pagina_id) REFERENCES paginas (id) ON DELETE NO ACTION ON UPDATE NO ACTION
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ",
        'drop' => "DROP TABLE pagfotos;"
    ),
    'clientes' => array(
        'create' => "
        CREATE TABLE clientes (
    id int(11) NOT NULL AUTO_INCREMENT,
    nome varchar(200) DEFAULT NULL,
    email varchar(200) DEFAULT NULL,
    telefone varchar(45) DEFAULT NULL,
    celular varchar(45) DEFAULT NULL,
    newsletter int(11) DEFAULT NULL,
    ativo int(11) DEFAULT NULL,
    data timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    senha varchar(250) DEFAULT NULL,
    valid tinyint(4) DEFAULT NULL,
    role varchar(20) DEFAULT NULL,
    login varchar(50) DEFAULT NULL,
    PRIMARY KEY (id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ",
        'drop' => "DROP TABLE clientes;"
    ),
    'usuarios' => array(
        'create' => "
        CREATE TABLE usuarios (
        id int(11) NOT NULL AUTO_INCREMENT,
        nomeusuario varchar(200) NOT NULL,
        senha varchar(250) NOT NULL,
        nome varchar(200) NOT NULL,
        valid tinyint(4) NOT NULL,
        role varchar(20) NOT NULL,
        PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ",
        'drop' => "DROP TABLE usuarios;"
    ),
);