<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Pagina;
use Admin\Model\Pagfoto;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class PagfotoTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $pagfoto = new Pagfoto();
        $if = $pagfoto->getInputFilter();
 
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        $this->assertEquals(4, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('legenda'));
        $this->assertTrue($if->has('principal'));
        $this->assertTrue($if->has('pagina_id'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Input inválido: legenda = 
     */
    public function testInputFilterInvalido()
    {
        $pagfoto = new Pagfoto();
        //legenda não deve ter mais de 150
        $pagfoto->legenda = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }
 
    /**
     * Teste de inserção de um comment válido
     */
    public function testInsert()
    {
        $pagfoto = $this->addPagfoto();
        $saved = $this->getTable('Admin\Model\Pagfoto')->save($pagfoto);
        $this->assertEquals('Empresa', $saved->legenda);
        $this->assertEquals(1, $saved->id);
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     */
    public function testInsertInvalido()
    {
        $pagfoto = new Pagfoto();
        $pagfoto->legenda = 'teste';
        $pagfoto->pagina_id = 0;
 
        $saved = $this->getTable('Admin\Model\Pagfoto')->save($pagfoto);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Pagfoto');
        $pagfoto = $this->addPagfoto();
 
        $saved = $tableGateway->save($pagfoto);
        $id = $saved->id;
 
        $this->assertEquals(1, $id);
 
        $pagfoto = $tableGateway->get($id);
        $this->assertEquals('Empresa', $pagfoto->legenda);
 
        $pagfoto->legenda = 'Notebooks';
        $updated = $tableGateway->save($pagfoto);
 
        $pagfoto = $tableGateway->get($id);
        $this->assertEquals('Notebooks', $pagfoto->legenda);
 
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     * @expectedExceptionMessage Statement could not be executed
     */
    public function testUpdateInvalido()
    {
        $tableGateway = $this->getTable('Admin\Model\Pagfoto');
        $pagfoto = $this->addPagfoto();
 
        $saved = $tableGateway->save($pagfoto);
        $id = $saved->id;
 
        $pagfoto = $tableGateway->get($id);
        $pagfoto->pagina_id = 20;
        $updated = $tableGateway->save($pagfoto);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Pagfoto');
        $pagfoto = $this->addPagfoto();
 
        $saved = $tableGateway->save($pagfoto);
        $id = $saved->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $pagfoto = $tableGateway->get($id);
    }
 
   
    
    private function addPagina()
    {
        $pagina = new Pagina();
        $pagina->titulo = 'A Apple compra a Coderockr';
        $pagina->texto = 'texto da pagina';
        
        $saved = $this->getTable('Admin\Model\Pagina')->save($pagina);
         
        return $saved;
    }
 
    private function addPagfoto() 
    {
        $pagina = $this->addPagina();
        $pagfoto = new Pagfoto();
        $pagfoto->pagina_id = $pagina->id;
        $pagfoto->legenda = 'Empresa';
        $pagfoto->principal = 1;
        
 
        return $pagfoto;
    }
}