<?php

namespace Admin\Model;

use Core\Test\ModelTestCase;
use Admin\Model\Configuracao;
use Zend\InputFilter\InputFilterInterface;

/**
 * @group Model
 */
class ConfiguracaoTest extends ModelTestCase {

    public function testGetInputFilter() {
        $categoria = new Configuracao();
        $if = $categoria->getInputFilter();
        //testa se existem filtros
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }

    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if) {
        //testa os filtros
        $this->assertEquals(4, $if->count());

        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('nome'));
        $this->assertTrue($if->has('descricao'));
        $this->assertTrue($if->has('parametro'));
    }

    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalido() {
        //testa se os filtros estão funcionando
        $categoria = new Configuracao();
        //nome só pode ter 100 caracteres
        $categoria->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }

    /**
     * Teste de inserção de uma categoria válida
     */
    public function testInsert() {
        $categoria = $this->addConfiguracao();

        $saved = $this->getTable('Admin\Model\Configuracao')->save($categoria);

        //testa o filtro de tags e espaços
        $this->assertEquals('Email Contato', $saved->nome);
        $this->assertEquals('Email para onde serão enviadas as mensagens do site', $saved->descricao);
        $this->assertEquals('samuel@agenciaw3.com.br', $saved->parametro);

        //testa o auto increment da chave primária
        $this->assertEquals(1, $saved->id);
    }

    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInsertInvalido() {
        $categoria = new Configuracao();
        $categoria->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';


        $saved = $this->getTable('Admin\Model\Configuracao')->save($categoria);
    }

    public function testUpdate() {
        $tableGateway = $this->getTable('Admin\Model\Configuracao');
        $categoria = $this->addConfiguracao();

        $saved = $tableGateway->save($categoria);
        $id = $saved->id;

        $this->assertEquals(1, $id);

        $categoria = $tableGateway->get($id);
        $this->assertEquals('Email Contato', $saved->nome);
        $this->assertEquals('Email para onde serão enviadas as mensagens do site', $saved->descricao);
        $this->assertEquals('samuel@agenciaw3.com.br', $saved->parametro);

        $categoria->nome = 'Email pedidos';
        $categoria->descricao = 'Email para onde serão enviadas os pedidos do site';
        $categoria->parametro = 'alessandro@agenciaw3.com.br';
        $updated = $tableGateway->save($categoria);

        $categoria = $tableGateway->get($id);
        $this->assertEquals('Email pedidos', $updated->nome);
        $this->assertEquals('Email para onde serão enviadas os pedidos do site', $updated->descricao);
        $this->assertEquals('alessandro@agenciaw3.com.br', $updated->parametro);
    }

    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete() {
        $tableGateway = $this->getTable('Admin\Model\Configuracao');
        $categoria = $this->addConfiguracao();

        $saved = $tableGateway->save($categoria);
        $id = $saved->id;

        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas

        $categoria = $tableGateway->get($id);
    }

    private function addConfiguracao() {
        $categoria = new Configuracao();
        $categoria->nome = 'Email Contato';
        $categoria->descricao = 'Email para onde serão enviadas as mensagens do site';
        $categoria->parametro = 'samuel@agenciaw3.com.br';

        return $categoria;
    }

}