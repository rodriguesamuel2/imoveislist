<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Categoria;
use Admin\Model\Subcategoria;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class SubcategoriaTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $subcategoria = new Subcategoria();
        $if = $subcategoria->getInputFilter();
 
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        $this->assertEquals(3, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('nome'));
        $this->assertTrue($if->has('categoria_id'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Input inválido: nome = 
     */
    public function testInputFilterInvalido()
    {
        $subcategoria = new Subcategoria();
        //nome não deve ter mais de 100
        $subcategoria->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }
 
    /**
     * Teste de inserção de um comment válido
     */
    public function testInsert()
    {
        $subcategoria = $this->addSubcategoria();
        $saved = $this->getTable('Admin\Model\Subcategoria')->save($subcategoria);
        $this->assertEquals('Impressoras', $saved->nome);
        $this->assertEquals(1, $saved->id);
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     */
    public function testInsertInvalido()
    {
        $subcategoria = new Subcategoria();
        $subcategoria->nome = 'teste';
        $subcategoria->categoria_id = 0;
 
        $saved = $this->getTable('Admin\Model\Subcategoria')->save($subcategoria);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Subcategoria');
        $subcategoria = $this->addSubcategoria();
 
        $saved = $tableGateway->save($subcategoria);
        $id = $saved->id;
 
        $this->assertEquals(1, $id);
 
        $subcategoria = $tableGateway->get($id);
        $this->assertEquals('Impressoras', $subcategoria->nome);
 
        $subcategoria->nome = 'Notebooks';
        $updated = $tableGateway->save($subcategoria);
 
        $subcategoria = $tableGateway->get($id);
        $this->assertEquals('Notebooks', $subcategoria->nome);
 
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     * @expectedExceptionMessage Statement could not be executed
     */
    public function testUpdateInvalido()
    {
        $tableGateway = $this->getTable('Admin\Model\Subcategoria');
        $subcategoria = $this->addSubcategoria();
 
        $saved = $tableGateway->save($subcategoria);
        $id = $saved->id;
 
        $subcategoria = $tableGateway->get($id);
        $subcategoria->categoria_id = 20;
        $updated = $tableGateway->save($subcategoria);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Subcategoria');
        $subcategoria = $this->addSubcategoria();
 
        $saved = $tableGateway->save($subcategoria);
        $id = $saved->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $subcategoria = $tableGateway->get($id);
    }
 
    private function addCategoria()
    {
        $categoria = new Categoria();
        $categoria->nome = 'Informática';
      
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
 
        return $saved;
    }
 
    private function addSubcategoria() 
    {
        $categoria = $this->addCategoria();
        $subcategoria = new Subcategoria();
        $subcategoria->categoria_id = $categoria->id;
        $subcategoria->nome = 'Impressoras';
        
 
        return $subcategoria;
    }
}