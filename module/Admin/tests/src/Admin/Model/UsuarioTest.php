<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Usuario;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class UsuarioTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $usuario = new Usuario();
        $if = $usuario->getInputFilter();
        //testa se existem filtros
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        //testa os filtros
        $this->assertEquals(6, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('nomeusuario'));
        $this->assertTrue($if->has('senha'));
        $this->assertTrue($if->has('nome'));
        $this->assertTrue($if->has('valid'));
        $this->assertTrue($if->has('role'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalidoNomeusuario()
    {
        //testa se os filtros estão funcionando
        $usuario = new Usuario();
        //nomeusuario só pode ter 50 caracteres
        $usuario->nomeusuario = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }        
 
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalidoRole()
    {
        //testa se os filtros estão funcionando
        $usuario = new Usuario();
        //role só pode ter 20 caracteres
        $usuario->role = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }        
 
    /**
     * Teste de inserção de um usuario válido
     */
    public function testInsert()
    {
        $usuario = $this->addUsuario();
 
        //testa o filtro de tags e espaços
        $this->assertEquals('Steve Jobs', $usuario->nome);
        //testa o auto increment da chave primária
        $this->assertEquals(1, $usuario->id);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Input inválido: nomeusuario =
     */
    public function testInsertInvalido()
    {
        $usuario = new usuario();
        $usuario->nome = 'teste';
        $usuario->nomeusuario = '';
 
        $saved = $this->getTable('Admin\Model\usuario')->save($usuario);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Usuario');
        $usuario = $this->addUsuario();
 
        $id = $usuario->id;
 
        $this->assertEquals(1, $id);
 
        $usuario = $tableGateway->get($id);
        $this->assertEquals('Steve Jobs', $usuario->nome);
 
        $usuario->nome = 'Bill <br>Gates';
        $updated = $tableGateway->save($usuario);
 
        $usuario = $tableGateway->get($id);
        $this->assertEquals('Bill Gates', $usuario->nome);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Usuario');
        $usuario = $this->addUsuario();
 
        $id = $usuario->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $usuario = $tableGateway->get($id);
    }
 
    private function addUsuario()
    {
        $usuario = new Usuario();
        $usuario->nomeusuario = 'steve';
        $usuario->senha = md5('apple');
        $usuario->nome = 'Steve <b>Jobs</b>';
        $usuario->valid = 1;
        $usuario->role = 'admin';
 
        $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
        return $saved;
    }
 
}