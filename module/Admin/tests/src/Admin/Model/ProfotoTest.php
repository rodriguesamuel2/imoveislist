<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Categoria;
use Admin\Model\Subcategoria;
use Admin\Model\Marca;
use Admin\Model\Produto;
use Admin\Model\Profoto;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class ProfotoTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $profoto = new Profoto();
        $if = $profoto->getInputFilter();
 
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        $this->assertEquals(5, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('imagem'));
        $this->assertTrue($if->has('produto_id'));
        $this->assertTrue($if->has('principal'));
        $this->assertTrue($if->has('legenda'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Input inválido: imagem = 
     */
    public function testInputFilterInvalido()
    {
        $profoto = new Profoto();
        //nome não deve ter mais de 100
        $profoto->imagem = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }
 
    /**
     * Teste de inserção de um comment válido
     */
    public function testInsert()
    {
        $profoto = $this->addProfoto();
        $saved = $this->getTable('Admin\Model\Profoto')->save($profoto);
        $this->assertEquals('foto1.jpg', $saved->imagem);
        $this->assertEquals(1, $saved->id);
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     */
    public function testInsertInvalido()
    {
        $profoto = new Profoto();
        $profoto->imagem = 'teste';
        $profoto->produto_id = 0;
 
        $saved = $this->getTable('Admin\Model\Profoto')->save($profoto);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Profoto');
        $profoto = $this->addProfoto();
 
        $saved = $tableGateway->save($profoto);
        $id = $saved->id;
 
        $this->assertEquals(1, $id);
 
        $profoto = $tableGateway->get($id);
        $this->assertEquals('foto1.jpg', $profoto->imagem);
 
        $profoto->imagem = 'foto2.png';
        $updated = $tableGateway->save($profoto);
 
        $profoto = $tableGateway->get($id);
        $this->assertEquals('foto2.png', $profoto->imagem);
 
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     * @expectedExceptionMessage Statement could not be executed
     */
    public function testUpdateInvalido()
    {
        $tableGateway = $this->getTable('Admin\Model\Profoto');
        $profoto = $this->addProfoto();
 
        $saved = $tableGateway->save($profoto);
        $id = $saved->id;
 
        $profoto = $tableGateway->get($id);
        $profoto->produto_id = 20;
        $updated = $tableGateway->save($profoto);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Profoto');
        $profoto = $this->addProfoto();
 
        $saved = $tableGateway->save($profoto);
        $id = $saved->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $profoto = $tableGateway->get($id);
    }
 
    private function addCategoria()
    {
        $categoria = new Categoria();
        $categoria->nome = 'Informática';
      
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
 
        return $saved;
    }
 
    private function addSubcategoria() 
    {
        $categoria = $this->addCategoria();
        $subcategoria = new Subcategoria();
        $subcategoria->categoria_id = $categoria->id;
        $subcategoria->nome = 'Impressoras';
        
        $saved = $this->getTable('Admin\Model\Subcategoria')->save($subcategoria);
        
        return $saved;
    }
    
     private function addMarca()
    {
        $marca = new Marca();
        $marca->nome = 'HP';
        $marca->imagem = 'arquivo.jpg';
         
        $saved = $this->getTable('Admin\Model\Marca')->save($marca);
        
        return $saved;
    }
    
    private function addProduto() 
    {
        $subcategoria = $this->addSubcategoria();
        $marca = $this->addMarca();
        $produto = new Produto();
        $produto->subcategoria_id = $subcategoria->id;
        $produto->nome = 'hp';
        $produto->preco = '189,26';
        $produto->preco_promocao = '179,99';
        $produto->especificacoes = 'hp multifuncional';
        $produto->oferta = 0;
        $produto->novo = 1;
        $produto->promocao = 0;
        $produto->descricao = 'impressora de alta qualidade';
        $produto->marca_id = $marca->id;
        $produto->ativo = 1;
        $produto->estoque = 100;
        $produto->data = date('Y-m-d H:i:s');
        
        $saved = $this->getTable('Admin\Model\Produto')->save($produto);
 
        return $saved;
    }
 
    private function addProfoto() 
    {
        $produto = $this->addProduto();
        $profoto = new Profoto();
        $profoto->produto_id = $produto->id;
        $profoto->imagem = 'foto1.jpg';
        $profoto->legenda = '';
        $profoto->principal = 1;
         
        return $profoto;
    }
}