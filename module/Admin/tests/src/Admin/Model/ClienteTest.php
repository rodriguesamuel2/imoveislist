<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Cliente;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class ClienteTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $cliente = new Cliente();
        $if = $cliente->getInputFilter();
        //testa se existem filtros
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        //testa os filtros
        $this->assertEquals(12, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('nome'));
        $this->assertTrue($if->has('login'));
        $this->assertTrue($if->has('email'));
        $this->assertTrue($if->has('telefone'));
        $this->assertTrue($if->has('celular'));
        $this->assertTrue($if->has('newsletter'));
        $this->assertTrue($if->has('ativo'));
        $this->assertTrue($if->has('data'));
        $this->assertTrue($if->has('senha'));
        $this->assertTrue($if->has('valid'));
        $this->assertTrue($if->has('role'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalidoClientelogin()
    {
        //testa se os filtros estão funcionando
        $cliente = new Cliente();
        //login só pode ter 50 caracteres
        $cliente->login = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }        
 
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalidoRole()
    {
        //testa se os filtros estão funcionando
        $cliente = new Cliente();
        //role só pode ter 20 caracteres
        $cliente->role = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }        
 
    /**
     * Teste de inserção de um Cliente válido
     */
    public function testInsert()
    {
        $cliente = $this->addCliente();
 
        //testa o filtro de tags e espaços
        $this->assertEquals('Steve Jobs', $cliente->nome);
        //testa o auto increment da chave primária
        $this->assertEquals(1, $cliente->id);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Input inválido: login =
     */
    public function testInsertInvalido()
    {
        $cliente = new Cliente();
               
        $cliente->login = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
 
        $saved = $this->getTable('Admin\Model\Cliente')->save($cliente);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Cliente');
        $cliente = $this->addCliente();
 
        $id = $cliente->id;
 
        $this->assertEquals(1, $id);
 
        $cliente = $tableGateway->get($id);
        $this->assertEquals('Steve Jobs', $cliente->nome);
 
        $cliente->nome = 'Bill <br>Gates';
        $updated = $tableGateway->save($cliente);
 
        $cliente = $tableGateway->get($id);
        $this->assertEquals('Bill Gates', $cliente->nome);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Cliente');
        $cliente = $this->addCliente();
 
        $id = $cliente->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $cliente = $tableGateway->get($id);
    }
 
    private function addCliente()
    {
        $cliente = new Cliente();
        $cliente->login = 'steve';
        $cliente->telefone = '(38)3213-0033';
        $cliente->celular = '(38)3213-0033';
        $cliente->email = 'steve@steve.com.br';
        $cliente->senha = md5('apple');
        $cliente->nome = 'Steve <b>Jobs</b>';
        $cliente->valid = 1;
        $cliente->role = 'admin';
 
        $saved = $this->getTable('Admin\Model\Cliente')->save($cliente);
        return $saved;
    }
 
}