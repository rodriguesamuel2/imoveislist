<?php
 
use Core\Test\ControllerTestCase;
use Admin\Controller\CategoriaController;
use Admin\Model\Categoria;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
 
 
/**
 * @group Controller
 */
class CategoriaControllerTest extends ControllerTestCase
{
    /**
     * Namespace completa do Controller
     * @var string
     */
    protected $controllerFQDN = 'Admin\Controller\CategoriaController';
 
    /**
     * Nome da rota. Geralmente o nome do módulo
     * @var string
     */
    protected $controllerRoute = 'admin';
 
     /**
     * Testa o acesso a uma action que não existe
     */
    public function test404() {
        $this->routeMatch->setParam('action', 'action_nao_existentertrtr');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Testa a página inicial, que deve mostrar os categorias
     */
    public function testIndexAction() {
        // Cria categorias para testar
        $postA = $this->addCategoria();
        $postB = $this->addCategoria();

        // Invoca a rota index
        $this->routeMatch->setParam('action', 'index');
        $result = $this->controller->dispatch($this->request, $this->response);

        // Verifica o response
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        // Testa se um ViewModel foi retornado
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);

        // Testa os dados da view
        $variables = $result->getVariables();

        $this->assertArrayHasKey('categorias', $variables);

        // Faz a comparação dos dados
        $controllerData = $variables["categorias"]->getCurrentItems()->toArray();
        $this->assertEquals($postA->nome, $controllerData[0]['nome']);
        $this->assertEquals($postB->nome, $controllerData[1]['nome']);
    }

    /**
     * Testa a página inicial, que deve mostrar os categorias com paginador
     */
    public function testIndexActionPaginator() {
        // Cria categorias para testar
        $post = array();
        for ($i = 0; $i < 25; $i++) {
            $post[] = $this->addCategoria();
        }

        // Invoca a rota index
        $this->routeMatch->setParam('action', 'index');
        $result = $this->controller->dispatch($this->request, $this->response);

        // Verifica o response
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        // Testa se um ViewModel foi retornado
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);

        // Testa os dados da view
        $variables = $result->getVariables();

        $this->assertArrayHasKey('categorias', $variables);

        //testa o paginator
        $paginator = $variables["categorias"];
        $this->assertEquals('Zend\Paginator\Paginator', get_class($paginator));
        $categorias = $paginator->getCurrentItems()->toArray();
        $this->assertEquals(10, count($categorias));
        $this->assertEquals($post[0]->id, $categorias[0]['id']);
        $this->assertEquals($post[1]->id, $categorias[1]['id']);

        //testa a terceira página da paginação
        $this->routeMatch->setParam('action', 'index');
        $this->routeMatch->setParam('page', 3);
        $result = $this->controller->dispatch($this->request, $this->response);
        $variables = $result->getVariables();
        $controllerData = $variables["categorias"]->getCurrentItems()->toArray();
        $this->assertEquals(5, count($controllerData));
    }
 
    /**
     * Testa a tela de inclusão de um novo registro
     * @return void
     */
    public function testSaveActionNewRequest()
    {
 
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a resposta
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        
        // Testa se recebeu um ViewModel
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
 
        //verifica se existe um form
        $variables = $result->getVariables();
        $this->assertInstanceOf('Zend\Form\Form', $variables['form']);
        $form = $variables['form'];
        //testa os ítens do formulário
        $id = $form->get('id');
        $this->assertEquals('id', $id->getName());
        $this->assertEquals('hidden', $id->getAttribute('type'));
    }
 
    /**
     * Testa a alteração de um post
     */
    public function testSaveActionUpdateFormRequest()
    {
        $categoriaA = $this->addCategoria();
 
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        $this->routeMatch->setParam('id', $categoriaA->id);
        $result = $this->controller->dispatch($this->request, $this->response);
 
        // Verifica a resposta
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
 
        // Testa se recebeu um ViewModel
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
 
        $variables = $result->getVariables();
 
        //verifica se existe um form
        $variables = $result->getVariables();
        $this->assertInstanceOf('Zend\Form\Form', $variables['form']);
        $form = $variables['form'];
 
        //testa os ítens do formulário
        $id = $form->get('id');
        $nome = $form->get('nome');
        $this->assertEquals('id', $id->getName());
        $this->assertEquals($categoriaA->id, $id->getValue());
        $this->assertEquals($categoriaA->nome, $nome->getValue());
    }
 
    /**
     * Testa a inclusão de um novo post
     */
    public function testSaveActionCategoriaRequest()
    {
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        
        $this->request->setMethod('post');
        $this->request->getPost()->set('nome', 'Apple compra a Coderockr');
        
        
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a resposta
        $response = $this->controller->getResponse();
        //a página redireciona, então o status = 302
        $this->assertEquals(302, $response->getStatusCode());
 
        //verifica se salvou
        $categorias = $this->getTable('Admin\Model\Categoria')->fetchAll()->toArray();
        $this->assertEquals(1, count($categorias));
        $this->assertEquals('Apple compra a Coderockr', $categorias[0]['nome']);
    }
 
    /**
     * Tenta salvar com dados inválidos
     *
     */
    public function testSaveActionInvalidCategoriaRequest()
    {
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        
        $this->request->setMethod('post');
        $this->request->getPost()->set('nome', '');
        
        $result = $this->controller->dispatch($this->request, $this->response);
 
        //verifica se existe um form
        $variables = $result->getVariables();
        $this->assertInstanceOf('Zend\Form\Form', $variables['form']);
        $form = $variables['form'];
 
        //testa os errors do formulário
        $nome = $form->get('nome');
        $nomeErrors = $nome->getMessages();
        $this->assertEquals("Value is required and can't be empty", $nomeErrors['isEmpty']);
 
        //$description = $form->get('description');
        //$descriptionErrors = $description->getMessages();
        //$this->assertEquals("Value is required and can't be empty", $descriptionErrors['isEmpty']);
        
    }
 
    /**
     * Testa a exclusão sem passar o id do post
     * @expectedException Exception
     * @expectedExceptionMessage Código obrigatório
     */
    public function testInvalidDeleteAction()
    {
        $categoria = $this->addCategoria();
        // Dispara a ação
        $this->routeMatch->setParam('action', 'delete');
 
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a resposta
        $response = $this->controller->getResponse();
 
    }
 
    /**
     * Testa a exclusão do post
     */
    public function testDeleteAction()
    {
        $categoria = $this->addCategoria();
        // Dispara a ação
        $this->routeMatch->setParam('action', 'delete');
        $this->routeMatch->setParam('id', $categoria->id);
 
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a resposta
        $response = $this->controller->getResponse();
        //a página redireciona, então o status = 302
        $this->assertEquals(302, $response->getStatusCode());
 
        //verifica se excluiu
        $categorias = $this->getTable('Admin\Model\Categoria')->fetchAll()->toArray();
        $this->assertEquals(0, count($categorias));
    }
 
    /**
     * Adiciona um post para os testes
     */
    private function addCategoria()
    {
        $categoria = new Categoria();
        $categoria->nome = 'Apple compra a Coderockr';
        //$categoria->description = 'A Apple compra a <b>Coderockr</b><br> ';
        //$categoria->post_date = date('Y-m-d H:i:s');
 
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
 
        return $saved;
    }
}