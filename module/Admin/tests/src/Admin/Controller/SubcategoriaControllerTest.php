<?php
 
use Core\Test\ControllerTestCase;
use Admin\Controller\SubcategoriaController;
use Admin\Model\Subcategoria;
use Admin\Model\Categoria;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
 
 
/**
 * @group Controller
 */
class SubcategoriaControllerTest extends ControllerTestCase
{
    /**
     * Namespace completa do Controller
     * @var string
     */
    protected $controllerFQDN = 'Admin\Controller\SubcategoriaController';
 
    /**
     * Nome da rota. Geralmente o nome do módulo
     * @var string
     */
    protected $controllerRoute = 'admin';
 
     /**
     * Testa o acesso a uma action que não existe
     */
    public function test404() {
        $this->routeMatch->setParam('action', 'action_nao_existentertrtr');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Testa a página inicial, que deve mostrar os subcategorias
     */
    public function testIndexAction() {
        // Cria subcategorias para testar
        $subcategoriaA = $this->addSubcategoria();
        $subcategoriaB = $this->addSubcategoria();

        // Invoca a rota index
        $this->routeMatch->setParam('action', 'index');
        $result = $this->controller->dispatch($this->request, $this->response);

        // Verifica o response
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        // Testa se um ViewModel foi retornado
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);

        // Testa os dados da view
        $variables = $result->getVariables();

        $this->assertArrayHasKey('subcategorias', $variables);

        // Faz a comparação dos dados
        $controllerData = $variables["subcategorias"]->getCurrentItems()->toArray();
        $this->assertEquals($subcategoriaA->nome, $controllerData[0]['nome']);
        $this->assertEquals($subcategoriaB->nome, $controllerData[1]['nome']);
    }

    /**
     * Testa a página inicial, que deve mostrar os subcategorias com paginador
     */
    public function testIndexActionPaginator() {
        // Cria subcategorias para testar
        $subcategoria = array();
        for ($i = 0; $i < 25; $i++) {
            $subcategoria[] = $this->addSubcategoria();
        }

        // Invoca a rota index
        $this->routeMatch->setParam('action', 'index');
        $result = $this->controller->dispatch($this->request, $this->response);

        // Verifica o response
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        // Testa se um ViewModel foi retornado
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);

        // Testa os dados da view
        $variables = $result->getVariables();

        $this->assertArrayHasKey('subcategorias', $variables);

        //testa o paginator
        $paginator = $variables["subcategorias"];
        $this->assertEquals('Zend\Paginator\Paginator', get_class($paginator));
        $subcategorias = $paginator->getCurrentItems()->toArray();
        $this->assertEquals(10, count($subcategorias));
        $this->assertEquals($subcategoria[0]->id, $subcategorias[0]['id']);
        $this->assertEquals($subcategoria[1]->id, $subcategorias[1]['id']);

        //testa a terceira página da paginação
        $this->routeMatch->setParam('action', 'index');
        $this->routeMatch->setParam('page', 3);
        $result = $this->controller->dispatch($this->request, $this->response);
        $variables = $result->getVariables();
        $controllerData = $variables["subcategorias"]->getCurrentItems()->toArray();
        $this->assertEquals(5, count($controllerData));
    }
 
    /**
     * Testa a tela de inclusão de um novo registro
     * @return void
     */
    public function testSaveActionNewRequest()
    {
 
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a ressubcategoriaa
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        
        // Testa se recebeu um ViewModel
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
 
        //verifica se existe um form
        $variables = $result->getVariables();
        $this->assertInstanceOf('Zend\Form\Form', $variables['form']);
        $form = $variables['form'];
        //testa os ítens do formulário
        $id = $form->get('id');
        $this->assertEquals('id', $id->getName());
        $this->assertEquals('hidden', $id->getAttribute('type'));
    }
 
    /**
     * Testa a alteração de um subcategoria
     */
    public function testSaveActionUpdateFormRequest()
    {
        $subcategoriaA = $this->addSubcategoria();
 
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        $this->routeMatch->setParam('id', $subcategoriaA->id);
        $result = $this->controller->dispatch($this->request, $this->response);
 
        // Verifica a ressubcategoriaa
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
 
        // Testa se recebeu um ViewModel
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
 
        $variables = $result->getVariables();
 
        //verifica se existe um form
        $variables = $result->getVariables();
        $this->assertInstanceOf('Zend\Form\Form', $variables['form']);
        $form = $variables['form'];
 
        //testa os ítens do formulário
        $id = $form->get('id');
        $nome = $form->get('nome');
        $subcategoria_id = $form->get('categoria_id');
        $this->assertEquals('id', $id->getName());
        $this->assertEquals($subcategoriaA->id, $id->getValue());
        $this->assertEquals($subcategoriaA->nome, $nome->getValue());
        $this->assertEquals($subcategoriaA->categoria_id, $subcategoria_id->getValue());
    }
 
    /**
     * Testa a inclusão de um novo subcategoria
     */
    public function testSaveActionSubcategoriaRequest()
    {
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        
        $this->request->setMethod('post');
        $this->request->getPost()->set('nome', 'Apple compra a Coderockr');
        $this->request->getPost()->set('categoria_id', '1');
        
        
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a ressubcategoriaa
        $response = $this->controller->getResponse();
        //a página redireciona, então o status = 302
        $this->assertEquals(302, $response->getStatusCode());
 
        //verifica se salvou
        $subcategorias = $this->getTable('Admin\Model\Subcategoria')->fetchAll()->toArray();
        $this->assertEquals(1, count($subcategorias));
        $this->assertEquals('Apple compra a Coderockr', $subcategorias[0]['nome']);
        $this->assertEquals('1', $subcategorias[0]['categoria_id']);
    }
 
    /**
     * Tenta salvar com dados inválidos
     *
     */
    public function testSaveActionInvalidSubcategoriaRequest()
    {
        // Dispara a ação
        $this->routeMatch->setParam('action', 'save');
        
        $this->request->setMethod('post');
        $this->request->getPost()->set('nome', '');
        $this->request->getPost()->set('categoria_id', '');
        
        $result = $this->controller->dispatch($this->request, $this->response);
 
        //verifica se existe um form
        $variables = $result->getVariables();
        $this->assertInstanceOf('Zend\Form\Form', $variables['form']);
        $form = $variables['form'];
 
        //testa os errors do formulário
        $nome = $form->get('nome');
        $nomeErrors = $nome->getMessages();
        $this->assertEquals("Value is required and can't be empty", $nomeErrors['isEmpty']);
        
        $subcategoria_id = $form->get('categoria_id');
        $subcategoria_idErrors = $subcategoria_id->getMessages();
        $this->assertEquals("Value is required and can't be empty", $subcategoria_idErrors['isEmpty']);
 
        //$description = $form->get('description');
        //$descriptionErrors = $description->getMessages();
        //$this->assertEquals("Value is required and can't be empty", $descriptionErrors['isEmpty']);
        
    }
 
    /**
     * Testa a exclusão sem passar o id do subcategoria
     * @expectedException Exception
     * @expectedExceptionMessage Código obrigatório
     */
    public function testInvalidDeleteAction()
    {
        $subcategoria = $this->addSubcategoria();
        // Dispara a ação
        $this->routeMatch->setParam('action', 'delete');
 
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a ressubcategoriaa
        $response = $this->controller->getResponse();
 
    }
 
    /**
     * Testa a exclusão do subcategoria
     */
    public function testDeleteAction()
    {
        $subcategoria = $this->addSubcategoria();
        // Dispara a ação
        $this->routeMatch->setParam('action', 'delete');
        $this->routeMatch->setParam('id', $subcategoria->id);
 
        $result = $this->controller->dispatch($this->request, $this->response);
        // Verifica a ressubcategoriaa
        $response = $this->controller->getResponse();
        //a página redireciona, então o status = 302
        $this->assertEquals(302, $response->getStatusCode());
 
        //verifica se excluiu
        $subcategorias = $this->getTable('Admin\Model\Subcategoria')->fetchAll()->toArray();
        $this->assertEquals(0, count($subcategorias));
    }
     /**
     * Adiciona um categooria para os testes
     */
    private function addCategoria()
    {
        $categoria = new Categoria();
        $categoria->nome = 'Informatica';
        //$categoria->description = 'A Apple compra a <b>Coderockr</b><br> ';
        //$categoria->post_date = date('Y-m-d H:i:s');
 
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
 
        return $saved;
    }
 
    /**
     * Adiciona um subcategoria para os testes
     */
    private function addSubcategoria()
    {
        $categoria = $this->addCategoria();
        $subcategoria = new Subcategoria();
        $subcategoria->nome = 'Apple compra a Coderockr';
        $subcategoria->categoria_id = $categoria->id;
 
        $saved = $this->getTable('Admin\Model\Subcategoria')->save($subcategoria);
 
        return $saved;
    }
}