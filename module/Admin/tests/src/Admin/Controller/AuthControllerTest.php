<?php
 
use Core\Test\ControllerTestCase;
use Admin\Controller\AuthController;
use Admin\Model\Usuario;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
 
 
/**
 * @group Controller
 */
class AuthControllerTest extends ControllerTestCase
{
    /**
     * Namespace completa do Controller
     * @var string
     */
    protected $controllerFQDN = 'Admin\Controller\AuthController';
 
    /**
     * Nome da rota. Geralmente o nome do módulo
     * @var string
     */
    protected $controllerRoute = 'admin';
 
    
    public function test404()
    {
        $this->routeMatch->setParam('action', 'action_nao_existente');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }
 
    public function testIndexActionLoginForm()
    {
        // Invoca a rota index
        $this->routeMatch->setParam('action', 'index');
        $result = $this->controller->dispatch($this->request, $this->response);
 
        // Verifica o response
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
 
        // Testa se um ViewModel foi retornado
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
 
        // Testa os dados da view
        $variables = $result->getVariables();
 
        $this->assertArrayHasKey('form', $variables);
 
        // Faz a comparação dos dados
        $this->assertInstanceOf('Zend\Form\Form', $variables['form']);
        $form = $variables['form'];
        //testa os ítens do formulário
        $nomeusuario = $form->get('nomeusuario');
        $this->assertEquals('nomeusuario', $nomeusuario->getName());
        $this->assertEquals('text', $nomeusuario->getAttribute('type'));
 
        $senha = $form->get('senha');
        $this->assertEquals('senha', $senha->getName());
        $this->assertEquals('password', $senha->getAttribute('type'));
    } 
 
    /**
     * @expectedException Exception
     * @expectedExceptionMessage Acesso inválido
     */
    public function testLoginInvalidMethod()
    {
        $usuario = $this->addUsuario();
 
        // Invoca a rota index
        $this->routeMatch->setParam('action', 'login');
        $result = $this->controller->dispatch($this->request, $this->response);
 
    }
    public function testLogin()
    {
        $usuario = $this->addUsuario();
 
        // Invoca a rota index
        $this->request->setMethod('post');
        $this->request->getPost()->set('nomeusuario', $usuario->nomeusuario);
        $this->request->getPost()->set('senha', 'apple');
 
        $this->routeMatch->setParam('action', 'login');
        $result = $this->controller->dispatch($this->request, $this->response);
 
        // Verifica o response
        $response = $this->controller->getResponse();
        //deve ter redirecionado
        $this->assertEquals(302, $response->getStatusCode());
        $headers = $response->getHeaders();
        $this->assertEquals('Location: /', $headers->get('Location'));
    }     
 
    public function testLogout()
    {
        $usuario = $this->addUsuario();
 
        $this->routeMatch->setParam('action', 'logout');
        $result = $this->controller->dispatch($this->request, $this->response);
 
        // Verifica o response
        $response = $this->controller->getResponse();
        //deve ter redirecionado
        $this->assertEquals(302, $response->getStatusCode());
        $headers = $response->getHeaders();
        $this->assertEquals('Location: /', $headers->get('Location'));
    }
 
    private function addUsuario()
    {
        $usuario = new Usuario();
        $usuario->nomeusuario = 'steve';
        $usuario->senha = md5('apple');
        $usuario->nome = 'Steve <b>Jobs</b>';
        $usuario->valid = 1;
        $usuario->role = 'admin';
 
        $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
        return $saved;
    }
 
}