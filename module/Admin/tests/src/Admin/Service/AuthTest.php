<?php
namespace Admin\Service;
 
use DateTime;
use Core\Test\ServiceTestCase;
use Admin\Model\Usuario;
use Core\Model\EntityException;
use Zend\Authentication\AuthenticationService;
 
/**
 * Testes do serviço Auth
 * @category Admin
 * @package Service
 * @author  Samuel Rodrigues<samuel@agenciaw3.com.br>
 */
 
/**
 * @group Service
 */
class AuthTest extends ServiceTestCase
{
 
    /**
     * Authenticação sem parâmetros
     * @expectedException \Exception
     * @return void
     */
    public function testAuthenticateWithoutParams()
    {
        $authService = $this->serviceManager->get('Admin\Service\Auth');
 
        $authService->authenticate();
     }
 
    /**
     * Authenticação sem parâmetros
     * @expectedException \Exception
     * @expectedExceptionMessage Parâmetros inválidos
     * @return void
     */
    public function testAuthenticateEmptyParams()
    {
        $authService = $this->serviceManager->get('Admin\Service\Auth');
 
        $authService->authenticate(array());
     }
 
    /**
     * Teste da autenticação inválida
     * @expectedException \Exception
     * @expectedExceptionMessage Login ou senha inválidos
     * @return void
     */
    public function testAuthenticateInvalidParameters()
    {
        $authService = $this->serviceManager->get('Admin\Service\Auth');
 
        $authService->authenticate(array('nomeusuario' => 'invalid', 'senha' => 'invalid'));
    }
 
    /**
     * Teste da autenticação Inválida
     * @expectedException \Exception
     * @expectedExceptionMessage Login ou senha inválidos
     * @return void
     */
    public function testAuthenticateInvalidPassord()
    {
        $authService = $this->serviceManager->get('Admin\Service\Auth');
        $usuario = $this->addUsuario();
 
        $authService->authenticate(array('nomeusuario' => $usuario->nomeusuario, 'senha' => 'invalida'));
    }
 
    /**
     * Teste da autenticação Válida
     * @return void
     */
    public function testAuthenticateValidParams()
    {
        $authService = $this->serviceManager->get('Admin\Service\Auth');
        $usuario = $this->addUsuario();
        
        $result = $authService->authenticate(
            array('nomeusuario' => $usuario->nomeusuario, 'senha' => 'apple')
        );
        $this->assertTrue($result);
 
        //testar a se a authenticação foi criada
        $auth = new AuthenticationService();
        $this->assertEquals($auth->getIdentity(), $usuario->nomeusuario);
 
        //verica se o usuário foi salvo na sessão
        $session = $this->serviceManager->get('Session');
        $savedUsuario = $session->offsetGet('usuario');
        $this->assertEquals($usuario->id, $savedUsuario->id);
 
    }
 
    /**
     * Limpa a autenticação depois de cada teste
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        $auth = new AuthenticationService();
        $auth->clearIdentity();
    }
 
    /**
     * Teste do logout
     * @return void
     */
    public function testLogout()
    {
        $authService = $this->serviceManager->get('Admin\Service\Auth');
        $usuario = $this->addUsuario();
        
        $result = $authService->authenticate(
            array('nomeusuario' => $usuario->nomeusuario, 'senha' => 'apple')
        );
        $this->assertTrue($result);
 
        $result = $authService->logout();
        $this->assertTrue($result);
        
        //verifica se removeu a identidade da autenticação
        $auth = new AuthenticationService();
        $this->assertNull($auth->getIdentity());
 
        //verifica se o usuário foi removido da sessão
        $session = $this->serviceManager->get('Session');
        $savedUsuario = $session->offsetGet('usuario');
        $this->assertNull($savedUsuario);
    }
 
  
    private function addUsuario()
    {
        $usuario = new Usuario();
        $usuario->nomeusuario = 'steve';
        $usuario->senha = md5('apple');
        $usuario->nome = 'Steve <b>Jobs</b>';
        $usuario->valid = 1;
        $usuario->role = 'admin';
 
        $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
        return $saved;
    }
 
}